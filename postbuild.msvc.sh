#!/bin/bash

# exit on failure
set -e

BUILD_DIR=$1
REPO=`pwd`

if [ -z $BUILD_DIR ]; then
    echo "specify a build dir:"
    echo "    bash postbuild.msvc.sh Trodes/build_d"
fi

cp $REPO/Trodes/Libraries/Windows/FTDI/amd64/ftd2xx64.dll $BUILD_DIR/ftd2xx.dll
cp $REPO/extern/zmq/msvc64/czmq.dll $BUILD_DIR/czmq.dll
cp $REPO/extern/zmq/msvc64/libmlm.dll $BUILD_DIR/libmlm.dll
cp $REPO/extern/zmq/msvc64/libsodium.dll $BUILD_DIR/libsodium.dll
cp $REPO/extern/zmq/msvc64/ZeroMQ/bin/libzmq-v140-mt-4_3_1.dll $BUILD_DIR/libzmq-v140-mt-4_3_1.dll
