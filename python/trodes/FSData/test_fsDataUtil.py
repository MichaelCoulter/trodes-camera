from unittest import TestCase
from trodes.FSData.fsDataUtil import BinaryRecordsFileWriter
from trodes.FSData.fsDataUtil import BinaryRecordsFileReader
from trodes.FSData.fsDataUtil import BinaryRecordsError


class TestBinaryRecordsFileWriter(TestCase):
    def test_write_rec(self):
        bin_rec = BinaryRecordsFileWriter(label='unittest', file_id=1, save_dir='/tmp',
                                          file_prefix='binary_record_unit_test', file_postfix='bin_rec',
                                          rec_label_dict={1: 'label1', 2: 'label2'},
                                          rec_format_dict={1: 'II', 2: 'Iccc'})

        bin_rec.write_rec(1, 2, 3)
        bin_rec.write_rec(2, 4, b'a', b'b', b'c')

        self.assertRaises(BinaryRecordsError, bin_rec.write_rec, 1, 2)
        self.assertRaises(BinaryRecordsError, bin_rec.write_rec, 2, 4, 2, 2, 2)


class TestBinaryRecordsFileReader(TestCase):
    def test_read_write(self):
        writer = BinaryRecordsFileWriter(label='unittest', file_id=1, save_dir='/tmp',
                                          file_prefix='binary_record_unit_test', file_postfix='bin_rec',
                                          rec_label_dict={1: 'label1', 2: 'label2'},
                                          rec_format_dict={1: 'II', 2: 'Iccc'})

        writer.write_rec(1, 2, 3)
        writer.write_rec(2, 4, b'a', b'b', b'c')
        
        del writer

        reader = BinaryRecordsFileReader(file_id=1, save_dir='/tmp', file_prefix='binary_record_unit_test',
                                         file_postfix='bin_rec')

        self.assertTupleEqual(reader._read_record(), (0, 1, (2, 3)))
        self.assertTupleEqual(reader._read_record(), (1, 2, (4, b'a', b'b', b'c')))
        self.assertFalse(reader._read_record())

