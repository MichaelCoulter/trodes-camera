
"""
fsdata.py: Program for getting data and processing data from trodes and delivering
real-time feedback

Copyright 2015 Loren M. Frank

This program is part of the trodes data acquisition package.
trodes is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

trodes is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""

import trodes.FSData.fsFilters as fsFilters
import trodes.FSData.fsSockets as fsSockets
import trodes.FSData.fsDataUtil as fsUtils

import sys
import logging
import os.path

import selectors
import struct

import numpy as np

import xmltodict

from enum import Enum

sys.path.append('../Modules/FSGui')
import fsShared

# import trodes.FSData.fsShared as fsShared


class FSDataState(Enum):
    """ States that FSData can be in.

    init - Waiting for initialization messages necessary to setup persistent objects and sockets.
    waiting - Paused state where no data packets are being received from Trodes.
    processing - Data packets are being received but stim is not enabled, so parameters and settings can be updated.
    stimulating - Data packets are bnig received and stim is enabled, so parameter updating is locked.
    terminate - Cleanup state, sockets should be closed and memory cleared.
    """
    init = 0
    waiting = 1
    processing = 2
    stimulating = 3
    terminate = 4


class FSDataInitType(Enum):
    """ Maps TRODESMESSAGE needed to initialize FSData to an Enum class.

    Used as a convenience to check when initialization is done
    """
    ecu_hardware = fsShared.TRODESMESSAGE_ECUHARDWAREINFO
    mcu_hardware = fsShared.TRODESMESSAGE_MCUHARDWAREINFO
    num_ntrodes = fsShared.TRODESMESSAGE_NUMNTRODES
    num_pointsperspike = fsShared.TRODESMESSAGE_NUMPOINTSPERSPIKE
    num_chanperntrode = fsShared.TRODESMESSAGE_NUMCHANPERNTRODE
    dig_ports = fsShared.TRODESMESSAGE_NDIGITALPORTS


class FSDataSupervisor:
    def __init__(self, fsgui_host_name, fsgui_port):
        self.class_log = logging.getLogger(name='{}.{}'.format(self.__class__.__module__, self.__class__.__name__))
        self.state = FSDataState.init
        self.init_step = []

        self.class_log.info('Getting TCPIP socket to FSGui server {} port {}'.format(fsgui_host_name, fsgui_port))

        self.fsgui_handler = fsSockets.FSGuiSocketHandler(
            fsgui_socket=fsSockets.FSTCPSocket(hostname=fsgui_host_name, port=fsgui_port))

        self.class_log.info('FSData: got TCPIP socket to FSGui server')

        self.input_selector = selectors.DefaultSelector()
        #self.input_selector = selectors.EpollSelector()
        self.input_selector.register(fileobj=self.fsgui_handler, events=selectors.EVENT_READ)

        self.data_socket_handlers = []
        self.rtf = None
        self.validfunction = []
        self.mcusocket = None
        self.ecusocket = None
        self.record_manager = None
        self.config = []

    def state_change(self, new_state):
        self.class_log.info('Old state ({}), new state ({})'.format(self.state, new_state))
        self.state = new_state

    def check_init_done(self):
        check = True
        # check to make sure all init messages have been received
        for init_type in FSDataInitType:
            check = check and init_type in self.init_step

        return check

    def send_status_update(self, message_type_byte, message_bytearray):
        s = bytearray(1)
        s[0] = ord(message_type_byte)
        s.extend(message_bytearray)
        self.fsgui_handler.send_fsgui_message(fsShared.FS_RT_STATUS, s)

    def send_custom_ripple_baseline(self, baseline_mean, baseline_std):
        mean_packstring = 'd' * len(baseline_mean)
        mean_data = struct.pack(mean_packstring, *baseline_mean)
        self.fsgui_handler.send_fsgui_message(fsShared.FS_SET_CUSTOM_RIPPLE_BASELINE_MEAN, mean_data)

        std_packstring = 'd' * len(baseline_std)
        std_data = struct.pack(std_packstring, *baseline_std)
        self.fsgui_handler.send_fsgui_message(fsShared.FS_SET_CUSTOM_RIPPLE_BASELINE_STD, std_data)

    def start_stimulation(self):
        self.class_log.info('Start stimulation')
        for i in self.validfunction:
            self.ecusocket.send_shortcut_message(i)
        # trigger settle as soon as stim is executed
        self.send_settle_command()

    def stop_stimulation(self):
        for i in self.validfunction:
            # the stop function is the valid function number + 1
            self.ecusocket.send_shortcut_message(i+1)

    def send_settle_command(self):
        settlecommand = struct.pack('=H', 0x6601)
        self.mcusocket.senddata(settlecommand)

    def send_shortcut_message(self, function_num):
        self.ecusocket.send_shortcut_message(function_num)

    def process_query_messages(self, message):

        if message.msg_type == fsShared.FS_QUERY_RT_FEEDBACK_STATUS:
            self.send_status_update(fsShared.FS_STIM_STATUS, self.rtf.get_status_text_bytes())
            self.send_status_update(fsShared.FS_RIPPLE_STATUS, self.rtf.ripple_filter.get_status_text_bytes())
            self.send_status_update(fsShared.FS_SPATIAL_STATUS, self.rtf.spatial_filter.get_status_text_bytes())
            self.send_status_update(fsShared.FS_LATENCY_STATUS, self.rtf.latency_filter.get_status_text_bytes())

            if self.rtf.ripple_filter.param.updateCustomBaseline:
                self.rtf.ripple_filter.update_custom_ripple_baseline()
                self.send_custom_ripple_baseline(baseline_mean=self.rtf.ripple_filter.get_custom_ripple_mean(),
                                                 baseline_std=self.rtf.ripple_filter.get_custom_ripple_std())
            return True

        elif message.msg_type == fsShared.FS_START_LATENCY_TEST:
            self.rtf.latency_test_enabled = True
            self.class_log.info('Received Latency Test START command\n')
            return True
        elif message.msg_type == fsShared.FS_STOP_LATENCY_TEST:
            self.rtf.latency_test_enabled = False
            self.class_log.info('Received Latency Test STOP command\n')
            return True

    def process_param_messages(self, message):
        if message.msg_type == fsShared.TRODESMESSAGE_ENABLECONTDATASOCKET:
            ntrodeindex, enable = struct.unpack('=hB', message.msg_data)
            self.class_log.info('ENABLECONTDATASOCKET {} {}'.format(ntrodeindex, enable))
            # find the data client for this ntrode and enable it
            for i in range(len(self.data_socket_handlers)):
                if (self.data_socket_handlers[i].data_client_info.dataType == fsShared.TRODESDATATYPE_CONTINUOUS) and \
                        (self.data_socket_handlers[i].data_client_info.nTrodeIndex == ntrodeindex):
                    self.data_socket_handlers[i].enabled = enable
                    # set the id of this ntrode in the chanRippleFilter
                    # structure so we can print its status easily
                    self.rtf.ripple_filter.chan_ripple_filter[ntrodeindex].ntrode_id = \
                        self.data_socket_handlers[i].data_client_info.nTrodeId
                    self.rtf.ripple_filter.chan_ripple_filter[ntrodeindex].enabled = enable
            self.rtf.cont_ntrode_enabled[ntrodeindex] = enable
            self.rtf.ripple_filter.chan_ripple_filter[ntrodeindex].enable = enable
            return True
        elif message.msg_type == fsShared.TRODESMESSAGE_ENABLESPIKEDATASOCKET:
            ntrodeindex, enable = struct.unpack('hB', message.msg_data)
            self.class_log.info('ENABLESPIKEDATASOCKET {} {}'.format(ntrodeindex, enable))
            # the ntrode index is a qint16, enable is a byte
            for i in range(len(self.data_socket_handlers)):
                if (self.data_socket_handlers[i].data_client_info.dataType == fsShared.TRODESDATATYPE_SPIKES) and \
                        (self.data_socket_handlers[i].data_client_info.nTrodeIndex == ntrodeindex):
                    self.data_socket_handlers[i].enabled = enable
            return True
        elif message.msg_type == fsShared.FS_CONFIG_FILE:
            # the message contains an entire fsgui configuration file, so we parse it and save the results
            self.config = xmltodict.parse(message.msg_data)
            print('Got and parsed config file')
        elif message.msg_type == fsShared.FS_SET_RIPPLE_STIM_PARAMS:
            self.class_log.info('updating ripple filter parameters, len {}'.format(len(message.msg_data)))
            # parse the ripple parameters structure
            self.rtf.ripple_filter.update_ripple_params(message.msg_data)
            return True
        elif message.msg_type == fsShared.FS_SET_CUSTOM_RIPPLE_BASELINE_MEAN:
            self.class_log.info('update custom baseline mean')
            self.rtf.ripple_filter.set_custom_ripple_mean(message.msg_data)
            return True
        elif message.msg_type == fsShared.FS_SET_CUSTOM_RIPPLE_BASELINE_STD:
            self.class_log.info('update custom baseline std')
            self.rtf.ripple_filter.set_custom_ripple_std(message.msg_data)
            return True
        elif message.msg_type == fsShared.FS_SET_SPATIAL_STIM_PARAMS:
            self.class_log.info('updating spatial parameters')
            self.rtf.spatial_filter.update_spatial_params(message.msg_data)
            return True
        elif message.msg_type == fsShared.FS_SET_LATENCY_TEST_PARAMS:
            self.class_log.info('updating latency parameters')
            self.rtf.latency_filter.update_latency_params(message.msg_data)
            return True
        elif message.msg_type == fsShared.TRODESMESSAGE_SETSCRIPTFUNCTIONVALID:
            # get the function number
            fnum, valid = struct.unpack('=hB', message.msg_data)
            # add or remove this function depending on valid
            if valid:
                if fnum not in self.validfunction:
                    self.validfunction.append(fnum)
            else:
                if fnum in self.validfunction:
                    self.validfunction.pop(self.validfunction.index(fnum))
            return True

    def process_file_messages(self, message):
        if message.msg_type == fsShared.FS_CREATE_SAVE_FILE:
            full_filepath = message.msg_data.decode('utf-8')
            split_filepath = os.path.split(full_filepath)
            split_filename = split_filepath[1].rsplit(sep='.', maxsplit=1)
            if split_filename[1] != 'rec':
                self.class_log.warning('File name received was not a rec type')

            self.rtf.create_record_manager(label='fsdata', save_dir=split_filepath[0],
                                           file_prefix=split_filename[0], file_postfix='fsdatarec')
            return True

        elif message.msg_type == fsShared.FS_START_RECORDING:
            self.rtf.start_recording()
            return True

        elif message.msg_type == fsShared.FS_STOP_RECORDING:
            self.rtf.stop_recording()
            return True

        elif message.msg_type == fsShared.FS_CLOSE_SAVE_FILE:
            self.rtf.close_record_manager()
            return True

    def process_state_messages(self, message):
        if message.msg_type == fsShared.TRODESMESSAGE_TURNONDATASTREAM:
            self.class_log.info('TURNONDATASTREAM')
            # go through the sockets and turn on the ones that are enabled
            data = bytes()
            for tmp_s in self.data_socket_handlers:
                if tmp_s.enabled:
                    self.class_log.info('Enable data {} {} {}'.format(tmp_s.data_client_info.dataType,
                                        tmp_s.data_client_info.nTrodeId, tmp_s.enabled))
                    tmp_s.send_trodes_message(fsShared.TRODESMESSAGE_TURNONDATASTREAM, data)

            self.state_change(FSDataState.processing)
            return True

        elif message.msg_type == fsShared.TRODESMESSAGE_TURNOFFDATASTREAM:
            # go through the sockets and turn on the ones that are enabled
            data = bytes()
            for tmp_s in self.data_socket_handlers:
                if tmp_s.enabled:
                    tmp_s.send_trodes_message(fsShared.TRODESMESSAGE_TURNOFFDATASTREAM, data)

            self.state_change(FSDataState.waiting)
            return True

        elif message.msg_type == fsShared.TRODESMESSAGE_QUIT:
            self.state_change(FSDataState.terminate)
            return True

        elif message.msg_type == fsShared.FS_RESET_RT_FEEDBACK:
            self.rtf.reset_realtime_processing()
            self.class_log.info('Reseting realtime filters')
            return True

        elif message.msg_type == fsShared.FS_START_RT_FEEDBACK:
            self.rtf.stim_enabled = True
            self.class_log.info('Received realtime START command')
            self.state_change(FSDataState.stimulating)
            return True

        elif message.msg_type == fsShared.FS_STOP_RT_FEEDBACK:
            # stop any ongoing stimulation
            self.stop_stimulation()
            self.rtf.stim_enabled = False
            self.class_log.info('Received realtime STOP command')
            self.state_change(FSDataState.processing)
            return True

        return None

    def process_init_messages(self, message):
        if message.msg_type == fsShared.TRODESMESSAGE_ECUHARDWAREINFO:
            # set the hardware address and open a UDP socket to it
            hni = fsShared.HardwareNetworkInfo()
            hni.port, tmpaddress = struct.unpack('H80s', message.msg_data)
            hni.address = tmpaddress.partition(b'\0')[0].decode('utf-8')
            self.class_log.info('trying to get hardware socket to {} port {}'.format(hni.address, hni.port))
            self.ecusocket = fsSockets.ECUSocket(hostname=hni.address, port=hni.port)
            return FSDataInitType.ecu_hardware

        elif message.msg_type == fsShared.TRODESMESSAGE_MCUHARDWAREINFO:
            # set the hardware address and open a UDP socket to it
            hni = fsShared.HardwareNetworkInfo()
            hni.port, tmpaddress = struct.unpack('H80s', message.msg_data)
            hni.address = tmpaddress.partition(b'\0')[0].decode('utf-8')
            self.class_log.info('trying to get hardware socket to {} port {}'.format(hni.address, hni.port))
            self.mcusocket = fsSockets.MCUSocket(hostname=hni.address, port=hni.port)
            return FSDataInitType.mcu_hardware

        elif message.msg_type == fsShared.TRODESMESSAGE_NUMNTRODES:
            # the message contains the number of nTrodes
            self.numntrodes = struct.unpack('i', message.msg_data)[0]
            #initilalize the ntrode channel list
            # create the filter object
            self.rtf = fsFilters.RTFilterManager(self, self.numntrodes)
            return FSDataInitType.num_ntrodes

        elif message.msg_type == fsShared.TRODESMESSAGE_NUMPOINTSPERSPIKE:
            # the message contains the number of points per channel for the spike waveform.
            self.numpointsperspike = struct.unpack('i', message.msg_data)
            return FSDataInitType.num_pointsperspike

        elif message.msg_type == fsShared.TRODESMESSAGE_NUMCHANPERNTRODE:
            # the message contains a ntrode number and the number of channels for that ntrode
            self.numchanperntrode = struct.unpack_from('=' + str(self.numntrodes) + 'i', message.msg_data)
            return FSDataInitType.num_chanperntrode

        elif message.msg_type == fsShared.TRODESMESSAGE_NDIGITALPORTS:
            inports, outports = struct.unpack('=ii', message.msg_data) # two ints
            self.rtf.din_state = np.zeros(inports, dtype=np.bool_)
            self.rtf.dout_state = np.zeros(outports, dtype=np.bool_)
            return FSDataInitType.dig_ports

    def process_dataclientmessage(self, message):
        if message.msg_type == fsShared.TRODESMESSAGE_STARTDATACLIENT:
            # the message contains a DataClientInfo structure
            # int  socketType;
            # uint8_t dataType;
            # uint16_t nTrodeId;
            # int nTrodeIndex;
            # uint16_t decimation;
            # unsigned short port;
            # char hostName[80];
            # print('datasize', len(message.msg_data), 'format size', struct.calcsize('BHiHH80s'))
            tmpdc = struct.unpack('iBHiHH80s', message.msg_data)
            # move elements into a DataClient class for clarity
            dc = fsShared.DataClientInfo()
            dc.socketType = int(tmpdc[0])
            dc.dataType = int(tmpdc[1])
            dc.nTrodeId = int(tmpdc[2])
            dc.nTrodeIndex = int(tmpdc[3])
            dc.decimation = int(tmpdc[4])
            dc.port = int(tmpdc[5])
            dc.hostName = tmpdc[6].partition(b'\0')[0].decode('utf-8')  # partition splits at the /0 char

            # start the socket
            if dc.socketType == fsShared.TRODESSOCKETTYPE_TCPIP:
                self.class_log.info('FSTCPSockets')
                tmpdatasocket = fsSockets.FSTCPSocket(dc.hostName, dc.port)
            elif dc.socketType == fsShared.TRODESSOCKETTYPE_UDP:
                self.class_log.info('FSUDPSockets')
                tmpdatasocket = fsSockets.FSUDPSocket(dc.hostName, dc.port)
            else:
                self.class_log.info('Error: unknown socket type {} requested'.format(dc.socketType))
                sys.exit(-1)

            self.class_log.info('FSDATA: STARTDATACLIENT - socketType {}, dataType {}, nTrodeId {}, '
                                'nTrodeIndex {}, decimation {}, dst_port {}, hostName {}, src_port {}'
                                .format(dc.socketType,
                                        dc.dataType,
                                        dc.nTrodeId,
                                        dc.nTrodeIndex,
                                        dc.decimation,
                                        dc.port,
                                        dc.hostName,
                                        tmpdatasocket.get_local_port()))
            if tmpdatasocket.sock == -1:
                self.class_log.error('Failed connecting to trodes data socket on host {} port {}'.format(dc.hostName,
                                                                                                         dc.port))
            else:
                # set this to the appropriate datatype by sending a mssage with the datatype
                # and the ntrode index if relevant
                tmp_data_socket_handler = fsSockets.TrodesDataSocketHandler(data_client_info=dc,
                                                                            trodes_data_socket=tmpdatasocket)

                self.data_socket_handlers.append(tmp_data_socket_handler)
                # also add the socket to the list of sockets for the select call
                # self.input_socket_handlers.append(tmp_data_socket_handler)
                self.input_selector.register(fileobj=tmp_data_socket_handler, events=selectors.EVENT_READ)

                if dc.dataType in [fsShared.TRODESDATATYPE_CONTINUOUS, fsShared.TRODESDATATYPE_SPIKES]:
                    c = struct.pack('=BH', dc.dataType, dc.nTrodeIndex)
                    self.data_socket_handlers[-1].enabled = False
                else:
                    # set the nTrodeIndex element to 0
                    c = struct.pack('=BH', dc.dataType, 0)
                    # enabled by default
                    self.data_socket_handlers[-1].enabled = True
                # send the datatype message to trodes
                if dc.socketType == fsShared.TRODESSOCKETTYPE_TCPIP:
                    # we only need to set the datatype for TCPIP sockets,
                    # as there a server could be for multiple datatypes
                    self.data_socket_handlers[-1].send_trodes_message(fsShared.TRODESMESSAGE_SETDATATYPE, c)
                # now send the decimation
                c = struct.pack('=H', dc.decimation)

                if (dc.dataType == fsShared.TRODESDATATYPE_CONTINUOUS):
                    self.data_socket_handlers[-1].send_trodes_message(fsShared.TRODESMESSAGE_SETDECIMATION, c)
            return True
        return False

    def main_loop(self):
        while self.state is not FSDataState.terminate:
            #readable, writeable, exception = select.select(self.input_socket_handlers, [], [])
            events = self.input_selector.select()

            for sel_key, sel_mask in events:
                s = sel_key.fileobj

                if s is not self.fsgui_handler:
                    # read and process the data from this datasocket
                    dataset = s.get_data_message().data
                    data_client_info = s.data_client_info
                    try:
                        for data in dataset:
                            self.rtf.processdata(data_client_info=data_client_info, data=data)
                    except TypeError:
                        # dataset is not iterable, try to use it as data
                        self.rtf.processdata(data_client_info=data_client_info, data=dataset)
                else:
                    # this is a message from fsgui
                    message = s.get_fsgui_message()

                    message_type = None

                    if self.state is FSDataState.init:
                        message_type = message_type or self.process_param_messages(message)
                        message_type = message_type or self.process_dataclientmessage(message)
                        init_type = self.process_init_messages(message)

                        if (message_type or init_type) is None:
                            self.class_log.warning('unexpected message (fsShared ID: {})'
                                                   ' received before init finished.'.format(message.msg_type))

                        if init_type is not None:
                            self.init_step.append(init_type)

                        if self.check_init_done():
                            self.state_change(FSDataState.waiting)

                    elif self.state is FSDataState.waiting:
                        message_type = message_type or self.process_param_messages(message)
                        message_type = message_type or self.process_state_messages(message)
                        message_type = message_type or self.process_query_messages(message)
                        message_type = message_type or self.process_file_messages(message)
                        message_type = message_type or self.process_dataclientmessage(message)

                        if message_type is None:
                            self.class_log.warning('unexpected message(fsShared ID: {})'
                                                   ' received during waiting state.'.format(message.msg_type))

                    elif self.state is FSDataState.processing:
                        message_type = message_type or self.process_param_messages(message)
                        message_type = message_type or self.process_query_messages(message)
                        message_type = message_type or self.process_state_messages(message)
                        message_type = message_type or self.process_file_messages(message)

                        if message_type is None:
                            self.class_log.warning('unexpected message(fsShared ID: {})'
                                                   ' received during processing state.'.format(message.msg_type))

                    elif self.state is FSDataState.stimulating:
                        message_type = message_type or self.process_state_messages(message)
                        message_type = message_type or self.process_query_messages(message)
                        message_type = message_type or self.process_file_messages(message)

                        if message_type is None:
                            self.class_log.warning('unexpected message(fsShared ID: {})'
                                                   ' received during stimulating state.'.format(message.msg_type))

                    elif self.state is FSDataState.terminate:
                        # cleanup
                        pass
        self.class_log.info('exiting')


