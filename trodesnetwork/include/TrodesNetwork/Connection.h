#pragma once

#include <TrodesNetwork/Generated/Handshake.h>
#include <TrodesNetwork/Util.h>

#include <thread>
#include <zmq.hpp>

namespace trodes {
namespace network {

class Connection {
public:
    Connection(std::string address, int port);
    ~Connection();

    std::string get_service_endpoint() const;
    std::string get_logger_endpoint() const;

    void add_endpoint(std::string name, std::string endpoint);
    std::string get_endpoint(std::string name);

private:
    std::string service_endpoint;
    std::string logger_endpoint;
};


}
}