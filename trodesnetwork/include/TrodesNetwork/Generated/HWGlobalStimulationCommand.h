#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor HWGlobalStimulationCommand
struct HWGlobalStimulationCommand {
    // field impl resetSequencerCmd
    bool resetSequencerCmd;
    // field impl killStimulationCmd
    bool killStimulationCmd;
    // field impl clearDSPOffsetRemovalCmd
    bool clearDSPOffsetRemovalCmd;
    // field impl enableStimulation
    bool enableStimulation;
    MSGPACK_DEFINE_MAP(resetSequencerCmd, killStimulationCmd, clearDSPOffsetRemovalCmd, enableStimulation);
};


}  // network
}  // trodes