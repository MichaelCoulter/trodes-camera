#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor TrodesTimestampData
struct TrodesTimestampData {
    // field impl localTimestamp
    uint32_t localTimestamp;
    // field impl systemTimestamp
    int64_t systemTimestamp;
    MSGPACK_DEFINE_MAP(localTimestamp, systemTimestamp);
};


}  // network
}  // trodes