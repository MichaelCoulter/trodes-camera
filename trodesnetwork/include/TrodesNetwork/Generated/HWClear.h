#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor HWClear
struct HWClear {
    // field impl number
    uint16_t number;
    MSGPACK_DEFINE_MAP(number);
};


}  // network
}  // trodes