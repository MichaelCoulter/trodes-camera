#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor HWStartStop
struct HWStartStop {
    // field impl startstop
    std::string startstop;
    // field impl slotgroup
    std::string slotgroup;
    // field impl number
    uint16_t number;
    MSGPACK_DEFINE_MAP(startstop, slotgroup, number);
};


}  // network
}  // trodes