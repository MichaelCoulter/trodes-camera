#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor Handshake
struct Handshake {
    // field impl replyEndpoint
    std::string replyEndpoint;
    // field impl mailEndpoint
    std::string mailEndpoint;
    // field impl message
    std::string message;
    MSGPACK_DEFINE_MAP(replyEndpoint, mailEndpoint, message);
};


}  // network
}  // trodes