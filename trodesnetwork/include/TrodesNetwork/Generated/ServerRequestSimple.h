#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor ServerRequestSimple
struct ServerRequestSimple {
    // field impl tag
    std::string tag;
    // field impl name
    std::string name;
    // field impl endpoint
    std::string endpoint;
    MSGPACK_DEFINE_MAP(tag, name, endpoint);
};


}  // network
}  // trodes