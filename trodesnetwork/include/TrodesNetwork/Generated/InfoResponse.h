#pragma once

#include <string>
#include <msgpack.hpp>

#include <variant>
#include "ITime.h"
#include "ITimerate.h"

namespace trodes {
namespace network {

// sum types are still a work in progress
struct IRTime{
// hah
// field impl a
ITime a;
};


struct IRTimerate{
// hah
// field impl a
ITimerate a;
};


struct IRTrodesConfig{
// error, was expecting only one unnamed variable
};


using InfoResponse = std::variant<IRTime, IRTimerate, IRTrodesConfig>;

}  // network
}  // trodes
namespace msgpack {
MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
namespace adaptor {
// Place class template specialization here
template<>
struct convert<trodes::network::InfoResponse> {
    msgpack::object const& operator()(msgpack::object const& o, trodes::network::InfoResponse& v) const {
        if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
        std::string tag_id = o.via.array.ptr[0].as<std::string>();
        if (tag_id != "tag") throw msgpack::type_error();
        std::string tag = o.via.array.ptr[1].as<std::string>();

// here is some code
if (tag == "IRTime") {
// handle this case
trodes::network::ITime a = o.via.array.ptr[2].as<trodes::network::ITime>();
v = trodes::network::IRTime{a};
} else if (tag == "IRTimerate") {
// handle this case
trodes::network::ITimerate a = o.via.array.ptr[2].as<trodes::network::ITimerate>();
v = trodes::network::IRTimerate{a};
} else if (tag == "IRTrodesConfig") {
// handle this case
v = trodes::network::IRTrodesConfig{};
} else {
    throw msgpack::type_error();
}

        return o;
    }
};
template<>
struct pack<trodes::network::InfoResponse> {
    template <typename Stream>
    packer<Stream>& operator()(msgpack::packer<Stream>& o, trodes::network::InfoResponse const& v) const {
        // packing member variables as an array.
auto elems = std::visit([](auto&& arg) {
    using T = std::decay_t<decltype(arg)>;
if constexpr (std::is_same_v<T, trodes::network::IRTime>) {
    return 3;
} else if constexpr (std::is_same_v<T, trodes::network::IRTimerate>) {
    return 3;
} else if constexpr (std::is_same_v<T, trodes::network::IRTrodesConfig>) {
    return 2;
} else {
    throw msgpack::type_error();
}
    },
    v
);
        o.pack_array(elems);
        o.pack("tag");
std::string tag = std::visit([](auto&& arg) {
    using T = std::decay_t<decltype(arg)>;
if constexpr (std::is_same_v<T, trodes::network::IRTime>) {
    return "IRTime";
} else if constexpr (std::is_same_v<T, trodes::network::IRTimerate>) {
    return "IRTimerate";
} else if constexpr (std::is_same_v<T, trodes::network::IRTrodesConfig>) {
    return "IRTrodesConfig";
} else {
    throw msgpack::type_error();
}
    },
    v
);
        o.pack(tag);
// this is sad
if (tag == "IRTime") {
// pack
o.pack(std::get<trodes::network::IRTime>(v).a);
} else if (tag == "IRTimerate") {
// pack
o.pack(std::get<trodes::network::IRTimerate>(v).a);
} else if (tag == "IRTrodesConfig") {
// pack
} else {
    throw msgpack::type_error();
}
        return o;
    }
};

} // namespace adaptor
} // MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS)
} // namespace msgpack