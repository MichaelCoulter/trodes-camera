#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor TrodesLFPData
struct TrodesLFPData {
    // field impl localTimestamp
    uint32_t localTimestamp;
    // field impl lfpData
    std::vector< int16_t > lfpData;
    // field impl systemTimestamp
    int64_t systemTimestamp;
    MSGPACK_DEFINE_MAP(localTimestamp, lfpData, systemTimestamp);
};


}  // network
}  // trodes