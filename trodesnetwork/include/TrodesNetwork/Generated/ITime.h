#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor ITime
struct ITime {
    // field impl time
    uint32_t time;
    MSGPACK_DEFINE_MAP(time);
};


}  // network
}  // trodes