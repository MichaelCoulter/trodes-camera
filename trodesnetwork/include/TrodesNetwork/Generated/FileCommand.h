#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor FileCommand
struct FileCommand {
    // field impl command
    std::string command;
    // field impl filename
    std::string filename;
    MSGPACK_DEFINE_MAP(command, filename);
};


}  // network
}  // trodes