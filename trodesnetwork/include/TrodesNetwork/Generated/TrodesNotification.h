#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor TrodesNotification
struct TrodesNotification {
    // field impl tag
    std::string tag;
    // field impl who
    std::string who;
    MSGPACK_DEFINE_MAP(tag, who);
};


}  // network
}  // trodes