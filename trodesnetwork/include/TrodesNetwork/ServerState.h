#pragma once

#include <optional>
#include <map>
#include <mutex>
#include <string>

namespace trodes {
namespace network {

class ServerState {
public:
    ServerState();
    ~ServerState();

    // delete copy and assignment constructor so only one state
    ServerState(const ServerState&) = delete;
    ServerState& operator=(ServerState const&);

    void add_endpoint(std::string name, std::string endpoint);
    std::optional<std::string> get_endpoint(std::string name);

private:
    mutable std::mutex mutex_;
    std::map<std::string, std::string> mappy;
};


}
}