#pragma once

#include <TrodesNetwork/Connection.h>
#include <TrodesNetwork/Resources/RawSourcePublisher.h>
#include <TrodesNetwork/Util.h>

#include <string>

namespace trodes {
namespace network {

template<typename T>
class SourcePublisher {
public:
    SourcePublisher(std::string address, int port, std::string name)
        : name(name),
          pub(util::get_wildcard_string(address))
    {
        Connection c(address, port);
        c.add_endpoint(name, pub.last_endpoint());
    }

    ~SourcePublisher() {
    }

    void publish(T data) {
        return pub.publish(data);
    }

private:
    std::string name;
    RawSourcePublisher<T> pub;
};


}
}