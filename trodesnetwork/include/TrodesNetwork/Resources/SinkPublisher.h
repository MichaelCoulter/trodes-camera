#pragma once

#include <TrodesNetwork/Connection.h>
#include <TrodesNetwork/Resources/RawSinkPublisher.h>
#include <TrodesNetwork/Util.h>

#include <string>

namespace trodes {
namespace network {

template<typename T>
class SinkPublisher {
public:
    SinkPublisher(std::string address, int port, std::string name)
        : name(name),
        endpoint(util::get_endpoint_retry_forever(address, port, name)),
        pub(endpoint)
    {
    }

    ~SinkPublisher() {
    }

    void publish(T data) {
        return pub.publish(data);
    }

private:
    std::string name;
    std::string endpoint;
    RawSinkPublisher<T> pub;
};


}
}