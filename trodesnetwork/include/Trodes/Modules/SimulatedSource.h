#pragma once

#include <Trodes/Modules/SpikeGenerator.h>
#include <Trodes/Modules/SimulatedSourceConfig.h>
#include <Trodes/Modules/TimestampedSample.h>
#include <TrodesNetwork/Generated/SpikePacket.h>
#include <TrodesNetwork/Resources/SourcePublisher.h>
#include <Trodes/TimestampUtil.h>

#include <chrono>
#include <cstdint>
#include <string>
#include <thread>

namespace trodes {

class SimulatedSource {
public:
    SimulatedSource(SimulatedSourceConfig config, SpikeGenerator generator)
        : config(config),
          generator(generator),
          perSample_ns(1'000'000'000/config.sampleRate)
    {

    }

    ~SimulatedSource() {

    }

    void start() {
        start_time_ns = util::system_timestamp_ns();
        // the first deadline is one sample away
        deadline_elapsed_ns = perSample_ns;
    }

    TimestampedSample getSample() {
        const uint64_t current_time_ns = util::system_timestamp_ns();
        const uint64_t deadline_time_ns = start_time_ns + (uint64_t)deadline_elapsed_ns;

        const int16_t sample = generator.getSample(deadline_time_ns);

        // are we ahead of schedule
        // if it's too short, don't bother waiting
        const int64_t delta_early_ns = deadline_time_ns - current_time_ns;
        if (delta_early_ns > 100) {
            std::this_thread::sleep_for(std::chrono::nanoseconds(delta_early_ns));
        }

        // set our new deadline
        deadline_elapsed_ns += perSample_ns;
        return TimestampedSample{deadline_time_ns, sample};
    }

private:
    SimulatedSourceConfig config;
    SpikeGenerator generator;
    const double perSample_ns;
    uint64_t start_time_ns;
    // use a floating then cast
    // so adding perSample_ns accumulates properly
    double deadline_elapsed_ns;
};

}