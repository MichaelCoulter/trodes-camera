#pragma once

#include <chrono>

namespace trodes {
namespace util {

    // nanoseconds since UNIX epoch
    uint64_t system_timestamp_ns();

}
}
