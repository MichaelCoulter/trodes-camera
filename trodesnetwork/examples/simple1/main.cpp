#include <TrodesNetwork/Connection.h>
#include <TrodesNetwork/Generated/Handshake.h>
#include <TrodesNetwork/Generated/SpikePacket.h>
#include <TrodesNetwork/Server.h>
#include <TrodesNetwork/Resources/RawSourceSubscriber.h>
#include <TrodesNetwork/Resources/ServiceConsumer.h>
#include <TrodesNetwork/Resources/ServiceProvider.h>
#include <TrodesNetwork/Resources/SinkPublisher.h>
#include <TrodesNetwork/Resources/SinkSubscriber.h>
#include <TrodesNetwork/Resources/SourcePublisher.h>
#include <TrodesNetwork/Resources/SourceSubscriber.h>

#include <Trodes/Modules/SimulateSpikes.h>
#include <Trodes/Modules/SimulatedSource.h>

#include <chrono>
#include <iostream>
#include <thread>

void testSourcePubSub(std::string logger_endpoint) {
    using trodes::network::RawSinkPublisher;

    trodes::network::RawSourceSubscriber<std::string> subby{logger_endpoint};

    trodes::network::SinkPublisher<std::string> forwarding{"logs"};

    while (true) {
        auto msg = subby.receive();
        forwarding.publish(msg);
        // std::cout << "Log: " << msg << std::endl;
    }
}

int main(int argc, char** argv) {
    if (argc) {}
    if (argv) {}

    trodes::network::Server server;
    trodes::network::Connection c;

    std::thread logger(testSourcePubSub, c.get_logger_endpoint());
    logger.detach();

    std::thread combinedLogger([]() {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        trodes::network::SinkSubscriber<std::string> logs("logs");

        while (true) {
            auto msg = logs.receive();
            std::cout << "[combined log]: " + msg << std::endl;
        }
    });
    combinedLogger.detach();


    std::thread publisher([]() {
        trodes::network::SourcePublisher<std::string> tetrodes("tetrodes");

        trodes::network::SinkPublisher<std::string> logs{"logs"};
        logs.publish("made tetrodes");
    });

    std::thread servicer([]() {
        trodes::network::ServiceProvider<std::string, std::string> hardware("hardware.service");

        trodes::network::SinkPublisher<std::string> logs{"logs"};
        logs.publish("made servicer");
    });

    publisher.join();
    servicer.join();

    std::thread simSample([]() {
        trodes::SimulateSpikes<trodes::SimulatedSource> simulator(
            "spikes",
            trodes::SimulatedSource(
                trodes::SimulatedSourceConfig{1000},
                trodes::SpikeGenerator(trodes::SpikeGeneratorConfig{30, 50, 5000})
                ));

        simulator.run();
    });
    simSample.detach();

    std::thread sampleForwarder([]() {
        trodes::network::SourceSubscriber<trodes::network::SpikePacket> spikesSub{"spikes"};
        trodes::network::SinkPublisher<std::string> logs{"logs"};

        while (true) {
            auto recvd = spikesSub.receive();
            logs.publish("spike[" + std::to_string(recvd.timestamp) + "]: " + std::to_string(recvd.x));
        }

    });
    sampleForwarder.detach();

    trodes::network::SinkPublisher<std::string> logs{"logs"};

    for (int i = 0; i < 10; i++) {
        logs.publish("Sending messages");
    }

    trodes::network::SourceSubscriber<std::string> trodesSub{"tetrodes"};
    trodes::network::ServiceConsumer<std::string, std::string> hardwareConsumer{"hardware.service"};

    std::cout << "Hello." << std::endl;
    return 0;
}