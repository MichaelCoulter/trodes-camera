cmake_minimum_required(VERSION 3.10)

project(trodesnetwork)

add_subdirectory(src)

# Enable by running `cmake -DBUILD_EXAMPLES=on`
if (BUILD_EXAMPLES)
    add_subdirectory(examples)
endif()

# Enable by running `cmake -DBUILD_TESTS=on`
if (BUILD_TESTS)
    add_subdirectory(test)
endif()