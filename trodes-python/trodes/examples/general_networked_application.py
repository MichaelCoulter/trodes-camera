import PyQt6.QtCore as QtCore
import PyQt6.QtWidgets as QtWidgets
import sys

from trodes.examples.network_select_dialog import NetworkSelectDialog

class GeneralNetworkedApplication(QtCore.QObject):
    def __init__(self, constructor, parent=None):
        super().__init__(parent=parent)

        self.parent = parent

        self.constructor = constructor
        self.window = None

        self.network_select = NetworkSelectDialog()
        self.network_select.finished.connect(self.has_connection)
        self.network_select.show()

    def has_connection(self, network_address):
        self.network_select.close()
        self.window = self.constructor(network_address, parent=self.parent)
        self.window.show()
    
def networked_main(constructor):
    app = QtWidgets.QApplication(sys.argv)
    network_application = GeneralNetworkedApplication(constructor, parent=None)
    sys.exit(app.exec())