import PyQt6.QtCore as QtCore
import PyQt6.QtWidgets as QtWidgets
import PyQt6.QtGui as QtGui

import trodesnetwork.socket as socket

'''
'''
class NetworkSelectDialog(QtWidgets.QDialog):
    finished = QtCore.pyqtSignal(object)

    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.setWindowTitle("Network Address")

        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)

        flo = QtWidgets.QFormLayout()
        layout.addLayout(flo)

        self.address = QtWidgets.QLineEdit()
        self.address.setPlaceholderText("tcp://127.0.0.1")
        self.address.setText("tcp://127.0.0.1")
        flo.addRow("Address", self.address)

        self.port = QtWidgets.QLineEdit()
        self.port.setPlaceholderText("49152")
        self.port.setText("49152")
        flo.addRow("Port", self.port)

        self.status = QtWidgets.QLabel("Ready")
        layout.addWidget(self.status)

        self.launch = QtWidgets.QPushButton("Launch")
        layout.addWidget(self.launch)
        self.launch.clicked.connect(self.submit)

    def submit(self):
        address = self.address.text()
        port = self.port.text()

        self.__get_thread = ConnectThread(self.finished, address, port)
        self.__get_thread.started.connect(lambda: self.status.setText("Connecting..."))
        self.__get_thread.finished.connect(lambda: self.status.setText("Ready..."))
        self.__get_thread.start()

class ConnectThread(QtCore.QThread):
    def __init__(self, finished, address, port):
        QtCore.QThread.__init__(self)

        self.finished = finished
        self.network_string = f'{address}:{port}'
    
    def run(self):
        # just test the connection to see if it's valid
        socket.connection(self.network_string)
        self.finished.emit(self.network_string)
