# trodes-python

The purpose of this module is to provide examples of how to build graphical
user interfaces (GUIs) integrated with `trodesnetwork`.

## Using conda

We can create

```
conda env remove -n trodes-python
conda env create -f environment.yml
```

This `environment.yml` file uses the `trodesnetwork` from pip instead of the
local development version for simplicity.