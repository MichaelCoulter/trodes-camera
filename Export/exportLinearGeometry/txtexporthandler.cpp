#include <QDebug>
#include <QtCore>
#include "txtexporthandler.h"
#include "geometry.h"

TXTExportHandler::TXTExportHandler(const QStringList& arguments):
    filename(""),
    directory(""),
    basename(""),
    fileNameSpecifiedFlag(false),
    fileNameFoundFlag(false),
    argumentsSupportedFlag(true),
    wasHelpMenuDisplayedFlag(false) {

    parseArguments(arguments);

}

TXTExportHandler::~TXTExportHandler() {

}

void TXTExportHandler::parseArguments(const QStringList& arguments) {
    int argumentsProcessed = 0;

    int optionInd = 1;
    while (optionInd < arguments.length()) {
       if ( arguments.at(optionInd).compare("-h",Qt::CaseInsensitive)==0 ||
            arguments.at(optionInd).compare("--help",Qt::CaseInsensitive)==0 ) {
           printHelpMenu();
           return;
       }
       if ( (arguments.at(optionInd).compare("-i", Qt::CaseInsensitive)==0 ||
             arguments.at(optionInd).compare("--input", Qt::CaseInsensitive)==0) &&
             arguments.length() > optionInd+1) {
           filename = arguments.at(optionInd+1);
           argumentsProcessed += 2;
       }
       if ( (arguments.at(optionInd).compare("-d", Qt::CaseInsensitive)==0 ||
             arguments.at(optionInd).compare("--directory", Qt::CaseInsensitive)==0) &&
             arguments.length() > optionInd+1) {
           directory = arguments.at(optionInd+1);
           argumentsProcessed += 2;
       }
       if ( (arguments.at(optionInd).compare("-b", Qt::CaseInsensitive)==0 ||
             arguments.at(optionInd).compare("--basename", Qt::CaseInsensitive)==0) &&
             arguments.length() > optionInd+1) {
           basename = arguments.at(optionInd+1);
           argumentsProcessed += 2;
       }
       optionInd++;
    }

    if (!filename.isEmpty()) {
        fileNameSpecifiedFlag = true;
    }

    fileNameFoundFlag = QFileInfo::exists(filename) && QFileInfo(filename).isFile();

    if (argumentsProcessed != arguments.length() - 1) {
        argumentsSupportedFlag = false;
    }
}

int TXTExportHandler::processData() {

    QString outputDirectory;
    if (directory.isEmpty()) {
        outputDirectory = QFileInfo(filename).absolutePath();
    } else {
        outputDirectory = directory;
    }

    QString outputBasename;
    if (basename.isEmpty()) {
        outputBasename = QFileInfo(filename).completeBaseName();
    } else {
        outputBasename = basename;
    }

    QDir dir(outputDirectory);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error: Could not create output directory" << outputDirectory;
            return -1;
        }
    }

    QFile infile;
    infile.setFileName(filename);
    if (!infile.open(QIODevice::ReadOnly)) {
        qDebug() << "Error: unable to open input file '" << filename << "'";
        return -1;
    }

    QVector<QPointF> nodes;
    QVector<LineNodeIndex> geoLines;
    bool dataFound = false;

    try {
        infile.setTextModeEnabled(false);
        QDataStream inputStream(&infile);
        inputStream.setByteOrder(QDataStream::LittleEndian);

        inputStream >> nodes;
        inputStream >> geoLines;

        dataFound = true;
    } catch(std::exception& e) { // file has text tag before binary data
        infile.seek(0);
        infile.setTextModeEnabled(true);
        QTextStream tstream(&infile);
        QString line;

        while(!infile.atEnd()) {
            line = tstream.readLine();
            if (line.contains("<Linearization Object> 1")) {
                infile.setTextModeEnabled(false);
                QDataStream inputStream(&infile);
                inputStream.device()->seek(tstream.pos());
                inputStream.setByteOrder(QDataStream::LittleEndian);

                inputStream >> nodes;
                inputStream >> geoLines;

                dataFound = true;
                break;
            }
        }
    }

    if (!dataFound) {
        qDebug() << "Could not find linear geometry data within input file";
        return -1;
    }

    QString outfilename = outputDirectory + QString(QDir::separator()) + outputBasename + QString(".txt");
    QFile outfile(outfilename);
    if (outfile.open(QIODevice::WriteOnly) ){
        qDebug() << "Writing to output file" << QFileInfo(outfile).absoluteFilePath();
        QTextStream outputStream(&outfile);
        outputStream << "x1" << " " << "y1" << " " << "x2" << " " << "y2" << Qt::endl;
        for (int i=0; i<geoLines.length(); ++i) {
            outputStream << geoLines[i].line.x1() << " "
                         << geoLines[i].line.y1() << " "
                         << geoLines[i].line.x2() << " "
                         << geoLines[i].line.y2() << Qt::endl;
        }
    } else {
        qDebug() << "Error: Could not create output file" << QFileInfo(outfile).absoluteFilePath();
        return -1;
    }
    return 0;
}

void TXTExportHandler::printHelpMenu() {
    printf("\n"
           "Used to extract the line segment endpoints defined in a .trackgeometry file to a human-readable text file.\n"
           "Usage: exportLinearGeometry -i INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...\n\n"
           "Input arguments\n\n"
           "--General--\n"
           "-i, --input <filename>          -- Path to the .trackgeometry file\n"
           "-d, --directory <dir>           -- A root directory to extract output files to (default is directory of the .trackgeometry file). \n"
           "-b, --basename <name>           -- The base name for the output file (default is the basename (suffix excluded) of the .trackgeometry file). \n");
    wasHelpMenuDisplayedFlag = true;
}

bool TXTExportHandler::fileNameSpecified() {
    return fileNameSpecifiedFlag;
}

bool TXTExportHandler::fileNameFound() {
    return fileNameFoundFlag;
}

bool TXTExportHandler::argumentsSupported() {
    return argumentsSupportedFlag;
}

bool TXTExportHandler::wasHelpMenuDisplayed() {
    return wasHelpMenuDisplayedFlag;
}



