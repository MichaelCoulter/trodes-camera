#ifndef DATEXPORTHANDLER_H
#define DATEXPORTHANDLER_H
//#include "abstractexporthandler.h"
#include "iirFilter.h"
#include "spikeDetectorThread.h"

#include <QObject>
#include <QtCore>
#include "configuration.h"
#include "iirFilter.h"
#include "streamprocesshandlers.h"
#include "vectorizedneuraldatahandler.h"
#include <stdio.h>

struct nTrodeInfo {
    bool nTrodeRefOn;
    bool filterOn;
    bool lfpFilterOn;
    bool lfpRefOn;
    bool rawRefOn;
    int nTrodeRefNtrode;
    int nTrodeRefNtrodeChannel;
    int nTrodeLowPassFilter;
    int nTrodeHighPassFilter;
    int numChannels;
    int lfpChannel;
};


class DATExportHandler
{
    //Q_OBJECT

public:
    DATExportHandler(QStringList arguments);
    int processData();

    bool fileNameFound();
    bool argumentsOK();
    bool argumentsSupported();
    bool fileConfigOK();
    bool wasHelpMenuDisplayed();
    void displayVersionInfo();


    //The exported data falls into different catagories, each with specific flags available.

    const int numberOfDataCategories = 11;
    enum DataCategories {
      SpikeBand         = 0x00000001,
      LfpBand           = 0x00000002,
      RawBand           = 0x00000004,
      DigitalIO         = 0x00000008,
      AnalogIO          = 0x00000010,
      SpikeEvents       = 0x00000020,
      Stimulation       = 0x00000040,
      Events            = 0x00000080,
      MountainSort      = 0x00000100,
      KiloSort          = 0x00000200,
      Time              = 0x00000400
    };

    enum FilePointerOrder {
      fSpikeBand         = 0,
      fLfpBand           = 1,
      fRawBand           = 2,
      fDigitalIO         = 3,
      fAnalogIO          = 4,
      fSpikeEvents       = 5,
      fStimulation       = 6,
      fEvents            = 7,
      fMountainSort      = 8,
      fKiloSort          = 9,
      fTime              = 10
    };

    enum SpikeSortingGroupMode {
        ByNtrodes       = 0,
        BySortingGroup  = 1,
        OneGroup        = 2,
        SingleChannels  = 3
    };

    const QStringList fileTypeNames = QStringList() << "spikeband" << "LFP"
        << "raw" << "DIO" << "analog" << "spikes" << "stimulation" << "events" << "mountainsort" << "kilosort" << "time";

    ~DATExportHandler();


protected:
    virtual void printHelpMenu();
    virtual void parseArguments();


    void createFilters();
    int readConfig();
    int readNextConfig(QString configName);
    void calculateChannelInfo();
    //void calculateChannelInfo_New();
    void calculateCARDataInPacket(int16_t* startOfNeuralData);
    bool openInputFile();
    void printProgress();
    int findEndOfConfigSection(QString fileName);
    void setDefaultMenuEnabled(bool enabled);
    void writeHeaderInfo(QFile*,QString description);
    bool getNextPacket();
    bool createTimeOffsetFile();

    void writeSpikeData();
    void writeSpikeBandData();
    void writeLfpBandData();
    void writeTimeData();
    void writeRawBandData();
    void writeStimBandData();
    void writeMountainSortData();
    void writeKiloSortData();
    void writeDIOData();
    void writeAnalogData();


    bool createAllOutputFiles();


    bool createSpikeBandOutputFiles();
    bool createLFPOutputFiles();
    bool createTimeOutputFiles();
    bool createRawOutputFiles();
    bool createDIOOutputFiles();
    bool createAnalogIOOutputFiles();
    bool createSpikeEventOutputFiles();
    bool createStimulationOutputFiles();
    bool createEventsOutputFiles();
    bool createMountainSortOutputFiles();
    bool createKiloSortOutputFiles();
    bool createChannelLocationFile(QString fileName, QVector<int> ntIndices,QVector<int> chIndices);

    QString fileBaseName;
    QString directory;



    //Pointers to the output files (data)
    QList<QList<QFile*> > outputFilePtrs;
    QList<QList<QDataStream*> > outputStreamPtrs;


    //QList<QFile*> neuroFilePtrs;
    //QList<QDataStream*> neuroStreamPtrs;

    //Pointers to the output files (time)
    QList<QList<QFile*> > timeFilePtrs;
    QList<QList<QDataStream*> > timeStreamPtrs;

    uint32_t supportedDataCategories;
    bool neuralDataNeedsProcessing;
    bool avxsupported;

    AbstractNeuralDataHandler* neuralDataHandler;
    QList<int> nTrodeIndexList;  //list of all nTrodes indices to process
    QString configString;
    TrodesConfiguration embeddedWorkspace;
    TrodesConfiguration externalWorkspace;
    TrodesConfiguration* loadedWorkspacePtr; //pointer to the loaded workspace (embedded or external).
    TrodesConfigurationPointers conf_ptrs;

    GlobalConfiguration *globalConf;
    HardwareConfiguration *hardwareConf;
    headerDisplayConfiguration *headerConf;
    streamConfiguration *streamConf;
    SpikeConfiguration *spikeConf;
    ModuleConfiguration *moduleConf;
    BenchmarkConfig *benchConfig;

    //virtual void printCustomMenu() = 0; //To be defined in inheriting class
    //virtual void parseCustomArguments(int &argumentsProcessed) = 0; //To be defined in inheriting class


    QStringList argumentList;
    QString recFileName;
    QStringList recFileNameList;
    QString externalWorkspaceFile;
    QString outputFileName;
    QString outputDirectory;

    QList<int> sortedDeviceList;

    bool firstPacketRead;

    int filterLowPass_SpikeBand;
    int filterHighPass_SpikeBand;
    int filterLowPass_LFPBand;
    int64_t maxGapSizeInterpolation;
    int64_t backupInterpCounter;

    bool override_Spike_Refs;
    bool override_LFPRefs;
    bool override_RAWRefs;
    bool override_useSpikeFilters;
    bool override_useLFPFilters;
    bool override_useNotchFilters;
    bool override_LFPRate;

    bool useRefs;
    bool useLFPRefs;
    bool useRAWRefs;
    bool useSpikeFilters;
    bool useLFPFilters;
    bool useNotchFilters;


    bool invertSpikes;
    bool threshOverride;
    int threshOverrideVal;
    //int threshOverrideVal_rangeConvert;

    SpikeSortingGroupMode sortingMode; //applies only to some of the processing modes

    bool defaultMenuEnabled;
    int argumentsProcessed;
    bool argumentReadOk;
    bool _argumentsSupported;
    bool _fileNameFound;
    bool _configReadOk;
    bool helpMenuPrinted;
    int dataStartLocBytes;
    int decimation;
    int outputSamplingRate;
    int paddingBytes;
    bool enableAVX;

    bool abortOnBackwardsTimestamp;


    bool requireRecFile;


    int numChannelsInFile;
    int packetHeaderSize;
    int packetTimeLocation;
    int filePacketSize;
    uint64_t startOffsetTime;

    uint64_t packetsRead;


    QList<int> nTrodeForChannels; //contains theuser-specified ID number for the nTrode
    QList<int> nTrodeIndForChannels; //contains the 0-based index of the nTrode;
    QList<int> channelInNTrode;
    QList<int> channelPacketLocations;
    QList<bool> refOn;
    QList<bool> lfpRefOn;
    QList<bool> groupRefOn;
    QList<int> refGroup;
    QList<int> refPacketLocations;
    QList<QList<int> > spikeSortingGroupLookup;

    //Buffer, variables for data reading
    //----------------------------------
    QVector<char> buffer;
    QVector<char> currentPacket;
    QVector<char> lastReadPacket;
    char* bufferPtr;
    uint32_t* tPtr;
    uint32_t currentTimeStamp;
    qint64 currentSysTimestamp;
    QVector<int16_t> CARData;
    uint32_t nextTimeStampInFile;
    uint32_t lastTimeStampInFile;
    qint64 nextSysTimeStampInFile;
    qint64 lastSysTimeStampInFile;
    int64_t systemTimeAtCreation;
    uint32_t timestampAtCreation;
    bool firstRecProcessed;
    int16_t* tempDataPtr;
    int16_t tempDataPoint;
    int16_t tempFilteredDataPoint;
    int16_t tempRefPoint;
    QVector<int16_t> dataLastTimePoint;
    int pointsSinceLastLog;
    QList<BesselFilter*> channelFilters;
    QVector<bool> filterOn;
    QVector<int> lowPassFilters;
    QVector<int> highPassFilters;

    QVector<bool> lfpFilterOn;
    QVector<int> lfpLowPassFilters;

    //For nTrode-based organization
    QList <ThresholdSpikeDetector *> spikeDetectors;
    QList <nTrodeInfo> nTrodeSettings;

    qint64 progressMarkerTrigger;
    qint64 progressMarkerCounter;
    double currentProgressMarker;
    //int lastProgressMod;

    //For AUX DIO processing
    QList<int> DIOChannelInds;
    QList<uint8_t> DIOLastValue;
    QVector<int> DIOinterleavedDataBytes;

    //For AUX analog processing
    QList<int> analogChannelInds;
    QVector<int16_t> analogLastValues;
    QVector<int> analogInterleavedDataBytes;

    //Input file
    QFile* filePtr;




private:




};



#endif // DATEXPORTHANDLER_H
