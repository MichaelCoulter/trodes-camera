#include "qtmoduleclient.h"

#include <TrodesNetwork/Generated/AcquisitionCommand.h>
#include <TrodesNetwork/Generated/InfoRequest.h>
#include <TrodesNetwork/Generated/InfoResponse.h>

// for the file status observer
#include <memory>
#include <TrodesNetwork/Resources/SourceSubscriber.h>
#include <TrodesNetwork/Generated/FileStatus.h>

#include "trodesmsg.h"

QtModuleClient::QtModuleClient(const char *id, const char* addr, int port)
    : address(std::string(addr))
    , port(port)
    , acquisitionConsumer(address, port, "trodes.acquisition.service")
    , infoConsumer(address, port, "trodes.info")
    , eventSender(address, port, "trodes.event.inbox")
    , lastRequestedTimeValue(0)
    , timestampRequestLock(false) {

    std::shared_ptr<QtModuleClient> mainWindow(this);

    auto file_subscriber_thread = std::thread([ address = address, port = port, mainWindow ]() {
        qDebug("subscribing to trodes.file.pub");

        trodes::network::SourceSubscriber<trodes::network::FileStatus> subscriber(address, port, "trodes.file.pub");
        while (true) {
            auto data = subscriber.receive();
            auto message = data.message;
            auto filename = data.filename;

            if (message == "open") {
                mainWindow->recv_file_open(filename);
            } else if (message == "close") {
                mainWindow->recv_file_close();
            } else if (message == "quit") {
                mainWindow->recv_quit();
            } else {
                qDebug("received unknown file status message");
            }
        }
    });
    file_subscriber_thread.detach();



    // acquisition sub
    // TODO: store as a field
    auto acquisition_subscriber_thread = std::thread([ address = address, port = port, mainWindow ]() {
        trodes::network::SourceSubscriber<trodes::network::AcquisitionCommand> subscriber(address, port, "trodes.acquisition");

        while (true) {
            auto data = subscriber.receive();
            // this will send out a signal
            mainWindow->recv_acquisition(data.command, data.timestamp);
        }
    });
    acquisition_subscriber_thread.detach();

}

QtModuleClient::~QtModuleClient() {

}

int QtModuleClient::initialize() {
    qDebug() << "initialize is a no-op (returning true)";
    return true;
}

void QtModuleClient::qtRequestEventList(){
    qDebug() << "qtRequestEventList() is a no-op.";
}

void QtModuleClient::qtSubscribeToEvent(QString module, QString event){
    qDebug() << "qtSubscribeToEvent() is a no-op.";
}

void QtModuleClient::qtUnsubscribeFromEvent(QString module, QString event){
    qDebug() << "qtUnsubscribeFromEvent() is a no-op.";
}

void QtModuleClient::qtProvideEvent(QString event) {
    qDebug() << "qtProvideEvent is a no-op";

}

void QtModuleClient::qtUnprovideEvent(QString event) {
    qDebug() << "qtUnprovideEvent is a no-op.";

}

//void QtModuleClient::configChanged() {
//    qDebug() << "New Configuration info recieved!";
////    qDebug() << getTrodesConfig().getPrintStr().c_str();

//}

void QtModuleClient::qtSendTimeRequest() {
    sendTimeRequest();
}

void QtModuleClient::qtSendPlaybackCommand(QString cmd, uint32_t time) {
    trodes::network::AcquisitionCommand command{cmd.toStdString(), time};
    // we don't need the response here, but it waits anyway
    acquisitionConsumer.request(command);
}

void QtModuleClient::qtSendStream(QString subject, TrodesMsg msg) {
    qDebug() << "qtSendStream() is a no-op.";
}

void QtModuleClient::qtSendEvent(QString event, TrodesMsg msg){

    //Request new time value
    eventSender.publish({event.toStdString(), latestTrodesTimestamp(), -1});

}
//Handle Event Messages

void QtModuleClient::recv_event(std::string origin, std::string event, TrodesMsg &msg) {
    //qDebug() << "recv_event() forwards signal";

    emit sig_event_received(QString::fromStdString(origin), QString::fromStdString(event), msg.copy());
}

//Handle Command Messages

void QtModuleClient::recv_file_open(std::string filename) {
    //qDebug() << "recv_file_open() forwards signal";
    emit sig_cmd_openFile(QString::fromStdString(filename));
}

void QtModuleClient::recv_file_close() {
    //qDebug() << "recv_file_close() forwards signal";
    emit sig_cmd_closeFile();
}

void QtModuleClient::recv_source(std::string source) {
    //qDebug() << "recv_source() forwards signal";
    emit sig_cmd_source(QString::fromStdString(source));
}

void QtModuleClient::recv_acquisition(std::string command, uint32_t time) {
    //qDebug() << "recv_acquisition() forwards signal";
    emit sig_cmd_acquisition(QString::fromStdString(command), time);
}

void QtModuleClient::recv_quit() {
    //qDebug() << "recv_quit() forwards signal";
    emit sig_cmd_quit();
}

void QtModuleClient::recv_time(uint32_t time) {

    emit sig_cmd_time((quint32)time);
}

void QtModuleClient::recv_timerate(int timerate) {
    //qDebug() << "recv_timerate() forwards signal";
    emit sig_cmd_timerate(timerate);
}

void QtModuleClient::sendTimeRequest() {
    recv_time(latestTrodesTimestamp());
}

void QtModuleClient::sendTimeRateRequest() {
    //qDebug() << "sendTimeRateRequest is a no-op.";
    qDebug() << "Sending time rate request.";
    recv_timerate(trodesTimeRate());
}

uint32_t QtModuleClient::lastRequestedTimestamp() {
    return lastRequestedTimeValue;
}

uint32_t QtModuleClient::latestTrodesTimestamp() {

    //This method is made thrad safe with a lock. Multiple threads can access this method directly.
    while (timestampRequestLock) {
        QThread::msleep(1);
    }

    timestampRequestLock = true;
    // sends a network request and waits for reply
    trodes::network::InfoRequest request{"time"};
    trodes::network::InfoResponse result = infoConsumer.request(request);
    trodes::network::IRTime time = std::get<trodes::network::IRTime>(result);
    lastRequestedTimeValue = time.a.time;
    timestampRequestLock = false;
    return time.a.time;
}

uint32_t QtModuleClient::trodesTimeRate() {
    // sends a network request and waits for reply
    trodes::network::InfoRequest request{"timerate"};
    trodes::network::InfoResponse result = infoConsumer.request(request);
    trodes::network::IRTimerate time = std::get<trodes::network::IRTimerate>(result);
    return time.a.timerate;
}

