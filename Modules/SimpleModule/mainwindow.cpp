#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../cameraModule/src/cameramodulenetworkdefines.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qDebug() << "Starting up SIMPLE MODULE";
    ui->setupUi(this);
    connect(this, &MainWindow::editmessagetext, ui->messagetext, &QLabel::setText);
    connect(this, &MainWindow::editeventtext, ui->eventtext, &QLabel::setText);
    qDebug() << "Starting Network";
    network = new SimpleModuleClient(DEFAULT_SERVER_ADDRESS, DEFAULT_SERVER_PORT);
    if(network->initialize() == 0){
        network->provideEvent("test");

        //SimpleModule is subscribed to these events form camera module
        network->subscribeToEvent(CAMERA_MODULE_NETWORK_ID, CAMERA_2DPOS);
        network->subscribeToEvent(CAMERA_MODULE_NETWORK_ID, CAMERA_LIN);
        network->subscribeToEvent(CAMERA_MODULE_NETWORK_ID, CAMERA_LIN_TRACK);
        network->subscribeToEvent(CAMERA_MODULE_NETWORK_ID, CAMERA_VELOCITY);
        network->subscribeToEvent(CAMERA_MODULE_NETWORK_ID, CAMERA_ZONE);
        connect(ui->pushButton_3, &QPushButton::clicked, this, &MainWindow::buttonpress);
        subhandle = network->subscribeAnalogData(1000, {"ECU,Ain1", "headstageSensor,GyroX"});
//        subhandle->initialize();
        subdata = new int16_t[2];
        network->initializeHardwareConnection();
//        network->sendStimulationParams();
        network->subscribeToEvent("pythonmod", "test");
        connect(network, &SimpleModuleClient::sig_cmd_openFile, this, &MainWindow::fileOpened);
        connect(network, &SimpleModuleClient::sig_cmd_closeFile, this, &MainWindow::fileClosed);
        connect(network, &SimpleModuleClient::sig_cmd_quit, this, &MainWindow::close);
        connect(network, &SimpleModuleClient::sig_cmd_source, this, &MainWindow::newsource);
        connect(network, &SimpleModuleClient::sig_cmd_acquisition, this, &MainWindow::acquisition);

        std::vector<std::string> cl = network->getClients();
        qDebug() << "Printing all connected clients to network:";
        for(auto const &c : cl){
            qDebug() << "--" << c.c_str();
        }
    }
    else{
        qDebug() << "Trodes server was not detected ... ";
    }

}

void MainWindow::buttonpress(){
    TrodesMsg msg("s",
                  "function 1"
                  "  portout[1] = flip"
                  "end");
    network->sendMsgToModule("StateScript", "StatescriptCommand", msg);



//    qDebug() << "t: " << network->latestTrodesTimestamp();
//    StimulationCommand s;
//    s.setGroup(1);
//    s.setSlot(1);
//    s.setChannels(1, 1);
//    s.setBiphasicPulseShape(5, 5, 5, 5, 5, 20, 5);
//    network->sendStimulationParams(s);
//    qDebug() << "t: " << network->latestTrodesTimestamp();
//    network->sendClearStimulationParams(1);
//    qDebug() << "t: " << network->latestTrodesTimestamp();
//    network->sendStimulationStartSlot(2);
//    qDebug() << "t: " << network->latestTrodesTimestamp();
//    network->sendStimulationStartGroup(3);
//    qDebug() << "t: " << network->latestTrodesTimestamp();
//    network->sendStimulationStopSlot(4);
//    qDebug() << "t: " << network->latestTrodesTimestamp();
//    network->sendStimulationStopGroup(5);
//    qDebug() << "t: " << network->latestTrodesTimestamp();
//    network->sendSettleCommand();
//    qDebug() << "t: " << network->latestTrodesTimestamp();


//    int av = subhandle->available(1000);
//    qDebug() << "Available: " << av;
//    for(int i = 0; i < av; i++){
//        uint32_t t = subhandle->getData(subdata);
//        qDebug() << t << subdata[0] << subdata[1];
//    }
}

void SimpleModuleClient::recv_event(std::string origin, std::string event, TrodesMsg &msg){
    QtModuleClient::recv_event(origin, event, msg); //the QtModuleClient only emits a signal relaying the event msg, it does not modify the variables in any way

    QString e = QString::fromStdString(event);
    if(e =="spike"){
        int ntrode, cluster;
        msg.popcontents("ii", ntrode, cluster);
//        int64_t lat = CZHelp::timer_usecs()-*(int64_t*)data.data();
//        totallat += lat;
//        totalspikes++;
        qDebug() << ntrode<< cluster;
    }
    else if(e.contains("test")){
        std::string s;
        int i;
        msg.popcontents("si", s, i);
        std::cout << origin << " " << event << " " << s << " " << i << "\n";
    }
    else if (event == CAMERA_2DPOS) {
        int time;
        uint16_t x, y;
        msg.popcontents("i22", time, x, y);
        qDebug() << "Got position data at [" << time << "]: (" << x << ", " << y << ")";

    }
    else if (event == CAMERA_LIN) {
        uint16_t segLoc;
        double linPos;
        msg.popcontents("2d", segLoc, linPos);
        qDebug() << "Got Linear Position data seg(" << segLoc << ") pos(" << linPos << ")";
    }
    else if (event == CAMERA_LIN_TRACK) {
        uint16_t numLines = 0;
        qDebug() << "Got Track info";
        msg.popcontents("2", numLines);
        qDebug() << "   -numLines = " << numLines;

        for (int i = 0; i < numLines; i++) {
            uint16_t curSegNum;
            double x1,y1,x2,y2;
            msg.popcontents("2dddd", curSegNum, x1, y1, x2, y2);
            qDebug() << "       - Seg[" << curSegNum << "] geometry:(" << x1 << ", " << y1 << ") -> (" << x2 << ", " << y2 << ")";

        }

    }
    else if (event == CAMERA_ZONE) {
        uint8_t polyId;
        uint16_t numPolyPts;
        msg.popcontents("12", polyId, numPolyPts);
        qDebug() << "Zone [" << polyId << "] w/ " << numPolyPts << " points received";

        for (int i = 0; i < numPolyPts; i++) {
            double x, y;
            msg.popcontents("dd", x, y);
            qDebug() << "   - (" << x << ", " << y << ")";
        }

    }
    else if (event == CAMERA_VELOCITY) {
        double velocity;
        msg.popcontents("d", velocity);
        qDebug() << "Got velocity " << velocity;
    }
}

int SimpleModuleClient::processOtherMsg(const char *sender, const char *subject, TrodesMsg &msg) {

    qDebug() << "Recieved othermsg [" << subject << "] msg from [" << sender << "]";
    return 0;
}

void SimpleModuleClient::processNeuroData(void *data, size_t bytes) {
//    qDebug() << "Neuro data received!";
    counterN++;
    if (counterN%30000 == 0) {
        size_t timestampOffset = 1*sizeof(uint16_t); //note: this is a test offset, will only work for specific config files
        uint32_t timestamp = 0;
        memcpy(&timestamp, (byte*)data+timestampOffset, sizeof(uint32_t));


        qDebug() << "Neuro data received at time [" << timestamp << "] Cur Count (" << counterN << ")";
    }

}

void SimpleModuleClient::processSpikeData(void *data, size_t bytes) {
//    qDebug() << "Spike data received!";
    counterS++;
    if(counterS%200 == 0)
        qDebug() << "Spike data received! Cur Count (" << counterS << ")";

}

MainWindow::~MainWindow()
{
    delete ui;
    delete network;
}

void MainWindow::fileOpened(QString f){
    emit editmessagetext("Trodes: Open file " + f);
}
void MainWindow::fileClosed(){
    emit editmessagetext("Trodes: Close file");
}
void MainWindow::newsource(QString s){
    emit editmessagetext("Trodes: Connect to source " + s);
}
void MainWindow::acquisition(QString c, quint32 time){
    emit editmessagetext("Trodes:  -- " + c + "--");
}
void MainWindow::printevent(QString e){
    emit editeventtext(e);
}

void MainWindow::on_pushButton_clicked()
{
//    qDebug() << "Subscribing to neurological data!";
////    HighFreqSub *sub = network->subscribeHighFreqData(hfType_NEURO, TRODES_NETWORK_ID);
//    HighFreqSub *sub = network->subscribeHighFreqData(hfType_NEURO, TRODES_NETWORK_ID, process_cont_data);

//    if (sub == NULL)
//        qDebug() << "   - FAILED";
//    else {
//        qDebug() << "   - SUCCESS " << sub << " type " << sub->getSubSockType();
//        sub->initialize();
//    }

//    network->subscribeContinuousNeuroData();
//    network->subscribeContinuousSpikeData();
//    network->subscribeStream()

//    int rc = network->subscribeStream(NETWORK_ID_CAMERA_MODULE, ".*");
//    qDebug() << "Subscribing to the " << NETWORK_ID_CAMERA_MODULE << " stream " << rc;
}

void MainWindow::on_pushButton_2_clicked()
{
//    network->unsubscribeContinuousNeuroData();
//    network->unsubscribeContinuousSpikeData();
//    int rc = network->unsubscribeHighFreqData(hfType_NEURO, TRODES_NETWORK_ID);
//    qDebug() << "Unsubscribed to Neurological data! " << rc;
}
