#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <TrodesNetwork/AbstractModuleClient.h>
//#include <configuration.h>
#include <qtmoduleclient.h>
namespace Ui {
class MainWindow;
}

static const char SIMPLE_MODULE_NETWORK_ID [] = "SimpleModule";

class SimpleModuleClient : public QtModuleClient{
    Q_OBJECT
public:
    SimpleModuleClient() : QtModuleClient(SIMPLE_MODULE_NETWORK_ID, DEFAULT_SERVER_ADDRESS, DEFAULT_SERVER_PORT){
        counterN = 0;
        counterS = 0;
    }
    SimpleModuleClient(const char* a, int p) : QtModuleClient(SIMPLE_MODULE_NETWORK_ID, a, p){}
    ~SimpleModuleClient(){
        std::cout << "total latency of spikes: " << (double)totallat/totalspikes << std::endl;
    }

    void recv_event(std::string origin, std::string event, TrodesMsg &msg);


    int64_t totallat=0;
    int totalspikes=0;
    int counterN;
    int counterS;

    int processOtherMsg(const char *sender, const char *subject, TrodesMsg &msg);

    void processNeuroData(void *data, size_t bytes);
    void processSpikeData(void *data, size_t bytes);
public slots:
    void sendevent(){
        TrodesMsg msg("si", "str1", 2);
        sendOutEvent("test", msg);
    }
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    SimpleModuleClient *network;
    AnalogConsumer *subhandle;
    int16_t *subdata;
    static void process_cont_data(void *data, size_t bytes, void *args);

signals:
    void editmessagetext(QString);
    void editeventtext(QString);

public slots:
    void fileOpened(QString);
    void fileClosed();
    void newsource(QString);
    void acquisition(QString, quint32 time);
    void printevent(QString);
    void buttonpress();
private slots:


    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
};

#endif // MAINWINDOW_H
