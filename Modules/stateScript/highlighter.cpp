/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QtGui>

#include "highlighter.h"

Highlighter::Highlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    keywordFormat.setForeground(Qt::blue);
    //keywordFormat.setFontWeight(QFont::Bold);
    QStringList keywordPatterns;
    keywordPatterns << "\\bint\\b" << "\\bcallback\\b" << "\\bif\\b"
                    << "\\bend\\b" << "\\belse\\b" << "\\bwhile\\b"
                    << "\\bdown\\b" << "\\bup\\b" << "\\bdo\\b"
                    << "\\bdo in\\b" << "\\bdo every\\b"
                    << "\\bthen\\b" << "\\bfunction\\b";
    foreach (const QString &pattern, keywordPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }


    //Compiler commands, such as the compile signal ';'
    systemFormat.setForeground(Qt::magenta);
    systemFormat.setFontWeight(QFont::Bold);
    QStringList systemPatterns;
    systemPatterns << ";";
    foreach (const QString &pattern, systemPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = systemFormat;
        highlightingRules.append(rule);
    }



    ioFormat.setForeground(Qt::black);
    ioFormat.setFontWeight(QFont::Bold);
    QStringList ioPatterns;
    ioPatterns << "\\bportin+\\[\\d+\\]" << "\\bportout+\\[\\d+\\]";

    foreach (const QString &pattern, ioPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = ioFormat;
        highlightingRules.append(rule);
    }

    commandFormat.setForeground(Qt::red);
    //commandFormat.setFontWeight(QFont::Bold);
    QStringList commandPatterns;
    commandPatterns << "\\bupdates on\\b" << "\\bupdates off\\b"
                    << "\\bserial buffer\\b" << "\\bserial send\\b";

    foreach (const QString &pattern, commandPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = commandFormat;
        highlightingRules.append(rule);
    }

    /*
    compileFormat.setFontWeight(QFont::Bold);
    compileFormat.setForeground(Qt::blue);
    rule.pattern = QRegExp(";");
    rule.format = compileFormat;
    highlightingRules.append(rule);
    */

    /*
    classFormat.setFontWeight(QFont::Bold);
    classFormat.setForeground(Qt::darkMagenta);
    rule.pattern = QRegExp("\\bQ[A-Za-z]+\\b");
    rule.format = classFormat;
    highlightingRules.append(rule);
    */

    quotationFormat.setForeground(Qt::darkMagenta);
    rule.pattern = QRegularExpression("\'.*\'");
    rule.format = quotationFormat;
    highlightingRules.append(rule);


    functionFormat.setFontItalic(true);
    functionFormat.setForeground(Qt::black);
    QStringList functionPatterns;
    functionPatterns << "\\bdisp\\b" << "\\bsound\\b"
                    << "\\bvolume\\b" << "\\brandom\\b"
                    << "\\bclock\\b" << "\\btrigger\\b"  ;
    foreach (const QString &pattern, functionPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = functionFormat;
        highlightingRules.append(rule);
    }

    singleLineCommentFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegularExpression("%[^\n]*");
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);

    //multiLineCommentFormat.setForeground(Qt::darkGreen);
    //commentStartExpression = QRegExp("/\\*");
    //commentEndExpression = QRegExp("\\*/");
}

void Highlighter::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, highlightingRules) {
        QRegularExpression expression(rule.pattern);
        QRegularExpressionMatch m = expression.match(text);

        if (m.hasMatch()) {
            for (int i=1; i < m.lastCapturedIndex();i++) {
                setFormat(m.capturedStart(i),m.capturedLength(i),rule.format);
            }
        }

    }
    setCurrentBlockState(0);

   /*
    int startIndex = 0;
    if (previousBlockState() != 1)
        startIndex = commentStartExpression.indexIn(text);

    while (startIndex >= 0) {
        int endIndex = commentEndExpression.indexIn(text, startIndex);
        int commentLength;
        if (endIndex == -1) {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        } else {
            commentLength = endIndex - startIndex
                    + commentEndExpression.matchedLength();
        }
        setFormat(startIndex, commentLength, multiLineCommentFormat);
        startIndex = commentStartExpression.indexIn(text, startIndex + commentLength);
    }*/
}

void Highlighter::asHtml(QTextEdit *editor, QString& html) {



    // Create a new document from all the selected text document.
    QTextCursor cursor(document());
    cursor.select(QTextCursor::Document);

    QTextDocument* tempDocument(new QTextDocument);
    Q_ASSERT(tempDocument);
    QTextCursor tempCursor(tempDocument);

    tempCursor.insertFragment(cursor.selection());
    tempCursor.select(QTextCursor::Document);

    // Set the default foreground for the inserted characters.
    QTextCharFormat textfmt = tempCursor.charFormat();
    textfmt.setForeground(Qt::gray);
    //tempCursor.setCharFormat(textfmt);

    // Apply the additional formats set by the syntax highlighter
    QTextBlock start = document()->findBlock(cursor.selectionStart());
    QTextBlock end = document()->findBlock(cursor.selectionEnd());
    end = end.next();
    const int selectionStart = cursor.selectionStart();
    const int endOfDocument = tempDocument->characterCount() - 1;
    for(QTextBlock current = start; current.isValid() && current != end; current = current.next()) {
        const QTextLayout* layout(current.layout());

        foreach(const QTextLayout::FormatRange &range, layout->formats()) {
            const int start = current.position() + range.start - selectionStart;
            const int end = start + range.length;
            if(end <= 0 || start >= endOfDocument)
                continue;
            tempCursor.setPosition(qMax(start, 0));
            tempCursor.setPosition(qMin(end, endOfDocument), QTextCursor::KeepAnchor);
            tempCursor.setCharFormat(range.format);
        }
    }

    // Reset the user states since they are not interesting
    for(QTextBlock block = tempDocument->begin(); block.isValid(); block = block.next())
        block.setUserState(-1);

    // Make sure the text appears pre-formatted, and set the background we want.
    tempCursor.select(QTextCursor::Document);
    QTextBlockFormat blockFormat = tempCursor.blockFormat();
    blockFormat.setNonBreakableLines(true);
    //blockFormat.setBackground(Qt::black);
    tempCursor.setBlockFormat(blockFormat);

    // Finally retreive the syntax higlighted and formatted html.
    html = tempCursor.selection().toHtml();
    delete tempDocument;

}

