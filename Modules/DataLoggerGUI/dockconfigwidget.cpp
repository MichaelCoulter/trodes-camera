#include "dockconfigwidget.h"
#include "abstractprocess.h"
DockConfigWidget::DockConfigWidget(QWidget *parent) : QDialog (parent)
{
    QGridLayout *layout = new QGridLayout;

    QProcess getsettings;
    getsettings.start(DOCKINGPATH, {"-m"});
    getsettings.waitForFinished();
    QString output = getsettings.readAllStandardOutput();
    QTextStream stream(&output);

    QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Apply | QDialogButtonBox::Cancel);
    connect(buttons->button(QDialogButtonBox::Apply), &QPushButton::clicked, this, &QDialog::accept);
    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);

    int row = 1;
    QString firstline = stream.readLine();
    if(firstline.contains("Docking station settings", Qt::CaseInsensitive)){
        layout->addWidget(new QLabel("Docking station settings"), 0, 0, 1, 2);
        while(!stream.atEnd()){
            QString line = stream.readLine();
            int splitind = line.indexOf(':'); //first index of ':'
            if(splitind == -1){
                continue;
            }
            QString left = line.left(splitind+1);
            QString right = line.mid(splitind+1).trimmed();

            QLabel *label = new QLabel(left);
            layout->addWidget(label, row, 0);
            labels.append(label);

            QLineEdit *text = new QLineEdit(right);
            layout->addWidget(text, row, 1);
            textfields.append(text);

            row++;
        }
    }
    else if(firstline.contains("Not supported", Qt::CaseInsensitive)){
        buttons->button(QDialogButtonBox::Apply)->setEnabled(false);
        QLabel *label = new QLabel("Not supported in this firmware version. Check for firmware updates");
        layout->addWidget(label, 0, 0, 1, 2);
    }

    layout->addWidget(buttons);
    setLayout(layout);
}

void DockConfigWidget::accept(){
    if(QMessageBox::Yes != QMessageBox::warning(this, "Confirm changes", "Changing dock settings. Continue?", QMessageBox::Yes|QMessageBox::No)){
        return;
    }
    QProcess setsettings;
    QStringList args("-n");
    qDebug() << "Writing settings for dock:";
    for(int i = 0; i < labels.length(); ++i){
        auto label = labels[i];
        auto lineEdit = textfields[i];
        qDebug() << label->text() << lineEdit->text();
        args << lineEdit->text();
    }

    setsettings.start(DOCKINGPATH, args);
    if(setsettings.waitForFinished()){
        //Sucessful, show message
        QMessageBox::information(this, "Successful settings write", "Settings successfully written to Dock");
        QDialog::accept();
    }
    else{
        //Something wrong, pop up message
        QMessageBox::warning(this, "Error writing settings",
                             QString("Error when writing settings to dock. Returned error (code %1): %2").
                                    arg(setsettings.exitCode()).
                                    arg(QString(setsettings.readAllStandardOutput())));
    }
}
