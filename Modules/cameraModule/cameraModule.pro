include(../module_defaults.pri)

QT       += opengl core gui network xml multimedia widgets openglwidgets

TARGET = cameraModule
TEMPLATE = app

# Use Qt Resource System for images
RESOURCES += \
    $$TRODES_REPO_DIR/Resources/Images/buttons.qrc

INCLUDEPATH  += ../../Trodes/src-main
INCLUDEPATH  += ../../Trodes/src-config
INCLUDEPATH  += ../../Trodes/src-network
INCLUDEPATH  += ../../Trodes/src-display
INCLUDEPATH  += ./src
INCLUDEPATH  += $$REPO_LIBRARY_DIR
INCLUDEPATH  += $$REPO_LIBRARY_DIR/Utility

# trodesnetwork: includes, libs
include(../../trodesnetwork_defaults.pri)

#########################################
#Source files and libraries
#########################################
SOURCES += src/main.cpp\
        src/mainwindow.cpp \
        src/videoDisplay.cpp \
        src/videoDecoder.cpp \
        src/videoEncoder.cpp \
        src/dialogs.cpp \
        src/abstractCamera.cpp \
        src/webcamWrapper.cpp \
        src/avtWrapper.cpp \
        src/CameraModuleClient.cpp \
	src/geometry.cpp \
        ../../Trodes/src-main/trodesSocket.cpp \
        ../../Trodes/src-config/configuration.cpp \
        ../../Trodes/src-main/trodesdatastructures.cpp \
        ../../Trodes/src-main/eventHandler.cpp \
        ../../Trodes/src-display/sharedtrodesstyles.cpp \
        ../../Libraries/qtmoduleclient.cpp \
        ../../Libraries/qtmoduledebug.cpp \

HEADERS  += src/mainwindow.h \
        src/videoDisplay.h \
        src/videoDecoder.h \
        src/videoEncoder.h \
        src/dialogs.h \
        src/abstractCamera.h \
        src/webcamWrapper.h \
        src/avtWrapper.h \
        src/CameraModuleClient.h \
        ../../Trodes/src-main/trodesSocket.h \
        ../../Trodes/src-config/configuration.h \
        ../../Trodes/src-main/trodesdatastructures.h \
        ../../Trodes/src-main/eventHandler.h \
        ../../Trodes/src-display/sharedtrodesstyles.h \
        ../../Libraries/qtmoduleclient.h \
        ../../Libraries/qtmoduledebug.h


#RESOURCES += ./src/pyCameraModule.py

DISTFILES += \
    src/pythonPath.txt

unix:!macx: {
    # FFMPEG libraries
    INCLUDEPATH += ../cameraModule/libraries/linux/x264
    LIBS += -L"../cameraModule/libraries/linux/x264" -lavcodec -lavformat -lswscale -lavutil -lx264 -lavresample

    # PvAPI library
    DEFINES += AVT_GIGE
    INCLUDEPATH += "$$PWD/libraries/linux/GigESDK/inc-pc"
    LIBS += -L"$$PWD/libraries/linux/GigESDK/bin-pc/x64" -lPvAPI
}

win32 {
#    INCLUDEPATH +=  $$TRODES_REPO_DIR/Libraries/Windows \
#                    $$TRODES_REPO_DIR/Libraries \
#                    $$TRODES_REPO_DIR/Libraries/Utility \
#                    $$TRODES_REPO_DIR/Libraries/Windows/TrodesNetwork/include

#    LIBS += -L$$TRODES_REPO_DIR/Libraries/Windows/TrodesNetwork/lib -lTrodesNetwork

    RC_ICONS += ./src/trodesWindowsIcon_camera.ico
    DEFINES += AVT_GIGE


    win32-g++ { # MinGW
        LIBS += -L$$quote($$PWD/Libraries/Windows/FTDI/i386) -lftd2xx
        LIBS += -L../Libraries/Windows/TrodesNetwork/lib -lTrodesNetwork

        # FFMPEG libraries
        INCLUDEPATH += ../cameraModule/libraries/win32
        INCLUDEPATH += ../cameraModule/libraries/win32/libx264
        LIBS += -L$$quote($${PWD}\libraries\win32) -lavutil -lavcodec -lavformat -lswscale -lx264-142


        # PvAPI library
        INCLUDEPATH += "$$PWD/libraries/win32/GigESDK/inc-pc"
        LIBS += -L"$$PWD/libraries/win32/GigESDK/lib-pc" -lPvAPI
    }
    else {
        !contains(QMAKE_TARGET.arch, x86_64) : { # MSVC-32
        } else { # MSVC-64

            LIBS += -L$$REPO_LIBRARY_DIR/Windows64/lib -lTrodesNetwork
            INCLUDEPATH += $$REPO_LIBRARY_DIR/Windows64/include
#            DEPENDPATH += $$PWD/../../../../newlibinstallsmsvc/include

            # FFMPEG libraries
            INCLUDEPATH += $$PWD/libraries/win64/ffmpeg/include/
            LIBS += -L$$PWD/libraries/win64/ffmpeg/lib/ -lavutil -lavcodec -lavformat -lswscale

            # x264 library
            INCLUDEPATH += $$PWD/libraries/win64/x264/include
            LIBS += -L$$PWD/libraries/win64/x264/lib/ -lx264


            # PvAPI library
            INCLUDEPATH += "$$PWD/libraries/win64/GigESDK/inc-pc"
            LIBS += -L$$PWD/libraries/win64/GigESDK/lib-pc/x64 -lPvAPI
        }
    }
}

macx {
    ICON        = $$PWD/src/trodesMacIcon_camera.icns
    QMAKE_INFO_PLIST = $$PWD/src/Info.plist

    # FFMPEG libraries
    INCLUDEPATH += $$PWD/libraries/osx/ffmpeg/include $$PWD/libraries/osx/x264/include
    LIBS += -L$${PWD}/libraries/osx/ffmpeg/lib -lavutil.55 -lavcodec.57 -lavformat.57 -lswscale.4
    LIBS += -L$${PWD}/libraries/osx/x264/lib -lx264
    # PvAPI
    INCLUDEPATH += $$PWD/libraries/osx/GigESDK/inc-pc
    LIBS += -L"$$PWD/libraries/osx/GigESDK/bin-pc/x64" -lPvAPI

    INCLUDEPATH += ../../Libraries/MacOS/include
    LIBS += -L../../Libraries/MacOS/lib -lTrodesNetwork
    QMAKE_POST_LINK += "cp $$TRODES_REPO_DIR/Libraries/MacOS/lib/libTrodesNetwork.*.dylib $$DESTDIR/$${TARGET}.app/Contents/MacOS/"
}

#########################################
#Install
#########################################

unix:!macx{

    #QMAKE_LFLAGS += '-Wl,-rpath,\'\$$ORIGIN/Libraries\''
    #QMAKE_LFLAGS += '-Wl,-rpath-link,\'\$$ORIGIN/Libraries\''

    # libraries.path defined in build_defaults.pri
    libraries.files += \
        ./libraries/linux/x264/libavcodec.so.56*\
        ./libraries/linux/x264/libavformat.so.56*\
        ./libraries/linux/x264/libswscale.so.3*\
        ./libraries/linux/x264/libavutil.so.54*\
        ./libraries/linux/x264/libx264.so.148*\
        ./libraries/linux/x264/libavresample.so.2*\
        ./libraries/linux/GigESDK/bin-pc/x64/libPvAPI.so\
        $$TRODES_REPO_DIR/PythonQt3.0/Python/lib/libpython3.5m.so.1.0\
        $$TRODES_REPO_DIR/PythonQt3.0/lib/libPythonQt.so.1\
        $$TRODES_REPO_DIR/PythonQt3.0/lib/libPythonQt_QtAll.so.1
#        ../../Libraries/Linux/TrodesNetwork/lib/libTrodesNetwork.so*
    INSTALLS += libraries

    cameramoduleicon.path = $$INSTALL_DIR/Resources/Images
    cameramoduleicon.files += $$PWD/src/cameramoduleIcon.png
    INSTALLS += cameramoduleicon

    QtDeploy.commands += cp -a $$TRODES_REPO_DIR/Libraries/Linux/TrodesNetwork/lib/libTrodesNetwork.so* $$INSTALL_DIR/Libraries/

}

win32{
    # libraries.path defined in build_defaults.pri
    win32-g++ { # MinGW
        libraries.files += \
            ./libraries/win32/avcodec-56.dll \
            ./libraries/win32/avformat-56.dll \
            ./libraries/win32/avutil-54.dll \
            ./libraries/win32/swscale-3.dll \
            ./libraries/win32/swresample-1.dll\
            ./libraries/win32/libx264-142.dll \
            ./libraries/win32/GigESDK/lib-pc/PvAPI.dll
        INSTALLS += libraries
    }
    else {
        !contains(QMAKE_TARGET.arch, x86_64) : { # MSVC-32
        } else { # MSVC-64




            libraries.files += \
                ./libraries/win64/ffmpeg/bin/avcodec-58.dll \
                ./libraries/win64/ffmpeg/bin/avdevice-58.dll \
                ./libraries/win64/ffmpeg/bin/avfilter-7.dll \
                ./libraries/win64/ffmpeg/bin/avformat-58.dll \
                ./libraries/win64/ffmpeg/bin/avutil-56.dll \
                ./libraries/win64/ffmpeg/bin/swresample-3.dll \
                ./libraries/win64/ffmpeg/bin/swscale-5.dll \
                ./libraries/win64/GigESDK/bin-pc/x64/PvAPI.dll

#                ./libraries/win64/x264/ # accidentally built static version of library, so no need for libx264.dll right now.
            INSTALLS += libraries
        }
    }

}

macx{
    libraries.files += \
        ./libraries/osx/ffmpeg/lib/libavcodec.57.dylib \
        ./libraries/osx/ffmpeg/lib/libavdevice.57.dylib \
        ./libraries/osx/ffmpeg/lib/libavfilter.6.dylib \
        ./libraries/osx/ffmpeg/lib/libavformat.57.dylib \
        ./libraries/osx/ffmpeg/lib/libavutil.55.dylib \
#        ./libraries/osx/ffmpeg/lib/libpostproc.54.dylib \
        ./libraries/osx/ffmpeg/lib/libswresample.2.dylib \
        ./libraries/osx/ffmpeg/lib/libswscale.4.dylib \
#        ./libraries/osx/ffmpeg/lib/libidn2.0.dylib \
        ./libraries/osx/x264/lib/libx264.dylib \
        ./libraries/osx/x264/lib/libx264.148.dylib \
        ./libraries/osx/GigESDK/bin-pc/x64/libPvAPI.dylib
#        ../../Libraries/MacOS/lib/libTrodesNetwork.*.dylib
    INSTALLS += libraries

    QtDeploy.commands += "install_name_tool -change @loader_path/libavutil.55.dylib @executable_path/../Frameworks/libavutil.55.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
    QtDeploy.commands += "install_name_tool -change @loader_path/libavcodec.57.dylib @executable_path/../Frameworks/libavcodec.57.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
    QtDeploy.commands += "install_name_tool -change @loader_path/libavformat.57.dylib @executable_path/../Frameworks/libavformat.57.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
    QtDeploy.commands += "install_name_tool -change @loader_path/libswscale.4.dylib @executable_path/../Frameworks/libswscale.4.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
    QtDeploy.commands += "install_name_tool -change @loader_path/libx264.148.dylib @executable_path/../Frameworks/libx264.148.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
    QtDeploy.commands += "install_name_tool -change @loader_path/libPvAPI.dylib @executable_path/../Frameworks/libPvAPI.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
#    QtDeploy.commands += "install_name_tool -change libTrodesNetwork.0.dylib @executable_path/../Frameworks/libTrodesNetwork.0.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/$${TARGET})" ;
    QtDeploy.commands += $$shell_path($$[QT_INSTALL_BINS]/macdeployqt) $$shell_path($$INSTALL_DIR/$${TARGET}.app) -libpath=$$TRODES_REPO_DIR/Libraries/MacOS/lib/  -libpath=$${PWD}/libraries/osx/ffmpeg/lib -libpath=${PWD}/libraries/osx/x264/lib -libpath=$$PWD/libraries/osx/GigESDK/bin-pc/x64 -always-overwrite ;

}
##################################################
#        OLD STUFF
##################################################

#unix: QMAKE_CXXFLAGS += -D__STDC_CONSTANT_MACROS
#QMAKE_CFLAGS += -g -O3
# Requied for some C99 defines
#DEFINES += __STDC_CONSTANT_MACROS

#-------------------------------------------------

# Set up embedded python. Uncommenting this will require PythonQT to be compiled first.
#CONFIG += EMBEDPYTHON

#EMBEDPYTHON {
#    DEFINES += PYTHONEMBEDDED

#    SOURCES += src/pythoncamerainterface.cpp
#    HEADERS += src/pythoncamerainterface.h

#    # Set up the python libs & headers the same way as they were set up for PythonQt build
#    # (location/version of Python used on each platform is set there)
#    include ( $$TRODES_REPO_DIR/PythonQt3.0/build/python.prf )

#    # Set up PythonQt libs & headers (NB don't include 'common.prf'--it's
#    # used for building PythonQt itself and will munge your target names)

#    include ( $$TRODES_REPO_DIR/PythonQt3.0/build/PythonQt.prf )
#    include ( $$TRODES_REPO_DIR/PythonQt3.0/build/PythonQt_QtAll.prf )

#}
#-------------------------------------------------


#-------------------------------------------------
# Set up shared libraries for Mac build/deployment.
#
# NB On the Mac, each shared library (dylib) has an 'install name" which should
# be a path at which the dylib can be found by a calling executable. The name
# is copied to the executable at link time, and used to find the library at run
# time. If that link is incomplete (e.g. 'libPythonQt.dylib', instead of a full
# path), then the system loader will fall back to searching system library
# paths (~/lib, /System/Frameworks, etc), and so cameraModule will often still
# run.
#
# Unfortunately 'macdeployqt' (a tool provided by Qt to copy external libraries
# into an app bundle and rewrite all the shared library paths) is not as smart
# as the system loader. It will only succeed if all the library links in the
# executable are full paths to those libraries.
#
# We therefore want to ensure that any external shared libraries have 'install
# names' set to absolute paths *before* linking cameraModule. This has the
# result that 1) camMod 'just works', and 2) we get to use macdeployqt and save
# ourselves a lot of deployment hassle
#
# Here's the overall strategy:
#
# 1) Use install names beginning with @rpath *only* for the Qt libraries (this
# is handled well by macdeployqt). Setting the @rpath correctly for the
# executable is handled by build_defaults.pri, since it applies to all
# projects.
#
# 2) For all other dependencies, make sure we are linking against libraries
# that have correct, absolute paths as their 'install name'
#
#  -On the Mac, if you have FFMPEG libs (libavcodec, libswscale) and libx264
#   provided by homebrew (in /usr/local/lib), they should already be correctly
#   installed.
#
#  -libPythonQt libs do *not* have their install name set correctly at build
#   time, so we run a script (fix_pythonqt_dylib.sh) to fix this before #
#   compiling. (In addition, libPythonQt_QtAll.dylib links to
#   libPythonQt.dylib. # In order to have this link survive the deploy process,
#   we update this link # to use @loader_path.)
#
#  -libPvAPI (provided in the Trodes repository) also needs to have its
#   install name set to an absolute path. We do this with a single call to #
#   install_name_tool
#
# 3) [**Optional, not done as part of the build process**] We can now run
# 'macdeployqt' against the resulting bundles. It will successfully copy over all
# the needed Qt libs, Qt plugins, *and* external libraries to the app bundle's
# 'Contents/Frameworks' folder, and update all the links to be relative to the
# @executable_path. This bundle can then be copied and run on a machine without
# Qt installed.
#
# Gotchas:
# There is a bug in macdeployqt for 5.5 (fixed in 5.7) that won't correctly
# deploy libs if they're in a folder called 'lib'. So we rely on a symlink
# called dylibs/ instead, which we use for the install_names.
#
# See this link for a good discussion of install_names, dylib loading, etc:
# https://www.mikeash.com/pyblog/friday-qa-2009-11-06-linking-and-install-names.html

#macx{

#EMBEDPYTHON{
## Run the fix_pythonqt_dylibs.sh shell script:
#PQT_DYLIB_DIR=$${TRODES_REPO_DIR}/PythonQt3.0/dylibs # NB No trailing slash--macdeployqt's path handling is fragile
#fix_dylibs.commands += "$${PQT_DYLIB_DIR}/fix_pythonqt_dylib.sh $${PQT_DYLIB_DIR} $$DEBUG_EXT ;" # ($$DEBUG_EXT is set in PythonQt.prf)
#}

## Set the install_name of PvAPI.dylib to its absolute location so that it will
## be linked correctly
#PVAPI_DYLIB=$${PWD}/libraries/osx/GigESDK/bin-pc/x64/libPvAPI.dylib
#fix_dylibs.commands += "install_name_tool -id $$PVAPI_DYLIB $$PVAPI_DYLIB && echo \"==> PvAPI.dylib install_name updated before build:\" ;"
#fix_dylibs.commands += "otool -L $$PVAPI_DYLIB | head -2;"

## Set up the Makefile to run these commands before compiling
#first.depends = $(first) fix_dylibs
#export(first.depends)
#export(fix_dylibs.commands)
#QMAKE_EXTRA_TARGETS += first fix_dylibs
#}

#-------------------------------------------------

#-------------------------------------------------



#macx {
## build against FFMPEG installed in /usr/local/{include,lib} (e.g. by homebrew,
## using 'brew install ffmpeg x264')
#INCLUDEPATH += /usr/local/include
#LIBS += -L"/usr/local/lib" -lavcodec -lavformat -lswscale -lavutil  -lavresample -lx264
#}


#-------------------------------------------------


##-------------------------------------------------
## Settings required for Allied Vision GigE libraries
#CONFIG += AVTGIGECAMERA

#AVTGIGECAMERA {
#DEFINES += AVT_GIGE
#SOURCES +=
#HEADERS +=

#macx {
#LIBS += -L"$$PWD/libraries/osx/GigESDK/bin-pc/x64" -lPvAPI
#INCLUDEPATH += "$$PWD/libraries/osx/GigESDK/inc-pc"
#}

#win32 {
#LIBS += -L"$$PWD/libraries/win32/GigESDK/lib-pc" -lPvAPI
#INCLUDEPATH += "$$PWD/libraries/win32/GigESDK/inc-pc"
#}


#}



##-------------------------------------------------


##----------------------------------------------------------
## Settings required for GigE Vision (aravis) interface

##CONFIG += GIGECAMERA

#GIGECAMERA {
#DEFINES += ARAVIS
#QMAKE_CXXFLAGS += -std=gnu++11
#SOURCES += src/araviswrapper.cpp
#HEADERS += src/araviswrapper.h

#unix: LIBS += -L/usr/local/lib/ -laravis-0.4

#INCLUDEPATH += /usr/local/include/aravis-0.4
#DEPENDPATH += /usr/local/include/aravis-0.4


#unix:!macx: LIBS += -L/usr/lib/x86_64-linux-gnu/ -lglib-2.0 -lgobject-2.0


#INCLUDEPATH += /usr/lib/x86_64-linux-gnu/glib-2.0/include/



## .pro file debugging messages
##message(Inside $$TARGET: QMAKE_RPATH: $$QMAKE_RPATH)
##message(Inside $$TARGET: QMAKE_RPATHDIR: $$QMAKE_RPATHDIR)



#unix:!macx{
#    cameramoduleicon.path = $$RESOURCES_COPY_PATH/Images
#    cameramoduleicon.files += $$PWD/src/cameramoduleIcon.png
#    INSTALLS += cameramoduleicon
#}
