#ifndef ABSTRACTCAMERA_H
#define ABSTRACTCAMERA_H

#include <QtWidgets>
#include <stdint.h>



class AbstractCamera: public QObject {

    Q_OBJECT

public:
    AbstractCamera();

    //Currently supported video formtats:
    enum videoFmt {
          Fmt_Invalid,
          Fmt_RGB24,
          Fmt_RGB32,
          Fmt_ARGB32,
          Fmt_Bayer8
    };

    virtual QStringList availableCameras() = 0; //Get a list of available device names
    void setFormat(videoFmt format); //set the current video format
    videoFmt getFormat(); //get the current video format

public slots:

    //These are pure virtual public slots, so they need to be defined in your derived camera class.
    //The camera module uses these slots to operate the camera
    //---------------------------------------------
    virtual bool open(int cameraID) = 0; //opens a camera using the index of the list from availableCameras()
    virtual void close() = 0; //closes the camera
    virtual bool start() = 0; //starts acquistion
    virtual void stop() = 0; //stops acquisition   
    //---------------------------------------------

    virtual void blinkAcquire(); //used to create a brief pause in frame acquisition.  Useful for timestamping
    virtual void resetCurrentCamera(); //close and reconnect to current camera


protected slots:

    void sendFrameSignals(QImage* img, uint32_t frameCount, uint64_t hwTimestamp, bool flip, qint64 fileTimestamp);


protected:
    quint32 numFramesRecieved;
    videoFmt fmt;

signals:

    void newFrame(QImage* img, quint32 frameCount, quint64 hwTimestamp, bool flip, qint64 fileTimestamp); //signal picked up by the video processor
    void newFrame_timerequest(); //used to send a message to Trodes to get the current time
    void formatSet(AbstractCamera::videoFmt format);

    //void debugSig(void); //just a debugging signal to check connectivitiy

};


#endif // ABSTRACTCAMERA_H
