/* rewardControl.cpp: qt widget for Frank Laboratory reward control in trodes
 *
 * Copyright 2012 Loren M. Frank
 *
 * This program is part of the nspike data acquisition package.
 * nspike is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * nspike is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with nspike; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


/* Note: 2-1-14: setRewardsDialog code is currently excluded via #ifdef below*/

#include "rewardControl.h"
#include <string>
#include <sstream>



rewardControl::rewardControl(QStringList arguments, QWidget *parent) :
    QMainWindow(parent)
{
    this->setWindowTitle("Frank Lab Reward Control");

    //QRect r(0, 0, 600, 400);
    //this->setGeometry(r);


    int optionInd = 1;
    bool trodes = false;


    moduleNet = new TrodesModuleNetwork();

    while (optionInd < arguments.length()) {
        if ((arguments.at(optionInd).compare("-trodesConfig", Qt::CaseInsensitive) == 0) && (arguments.length() > optionInd + 1)) {
            nsParseTrodesConfig(arguments.at(++optionInd));
            qDebug() << "RewardGUI parsing trodesConfig file " << arguments.at(optionInd);
            trodes = true;
        }
        else if ((arguments.at(optionInd).compare("-serverAddress", Qt::CaseInsensitive) == 0) && (arguments.length() > optionInd + 1)) {
            moduleNet->trodesServerHost = arguments.at(++optionInd);
            trodes = true;
        }
        else if ((arguments.at(optionInd).compare("-serverPort", Qt::CaseInsensitive) == 0) && (arguments.length() > optionInd + 1)) {
            moduleNet->trodesServerPort = (quint16)(arguments.at(++optionInd).toInt());
        }
        else if ((arguments.at(optionInd).compare("-config", Qt::CaseInsensitive) == 0) && (arguments.length() > optionInd + 1)) {
            rewardConfigFile = arguments.at(++optionInd);
        }
        optionInd++;
    }
    if ((moduleNet->trodesServerPort == 0) && (trodes == 1)) {
        // try setting this from the network config file
        moduleNet->trodesServerHost = networkConf->trodesHost;
        moduleNet->trodesServerPort = networkConf->trodesPort;
    }
    else if (trodes == false) {
        qDebug() << "Error: RewardGUI must be run with trodes";
        exit(1);
    }


    /* Parse the command line arguments.  Currently two arguments are defined:
     *  -config  config_file_name       specifies the local configuration file
     *  -trodesconfig config_file_name  specifies the global trodes configuration file */




    // Next we need to establish a connection with trodes if a trodes config file was specified
   qDebug() << "RewardGui: starting Network Messaging to trodes on host " << networkConf->trodesHost;

   moduleNet->dataNeeded = TRODESDATATYPE_DIGITALIO;

   moduleNet->useQTSocketsForData = true;
   connect(moduleNet, SIGNAL(quitReceived()), this, SLOT(close()));

   // Open a connection to the MCU

    int row = 0;

    readyToClose = false;

    nTrialBits = 0;

    this->setWindowTitle("Frank Lab Reward Control");



    // Create a multitabbed window
    qtab = new QTabWidget(this);
    this->ntabs = 4;
    qtab->setTabPosition(QTabWidget::South);

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(qtab);

    QWidget *window = new QWidget();
    window->setLayout(mainLayout);
    setCentralWidget(window);


    QRect r(0, 0, 600, 400);
    this->setGeometry(r);

    /* initialze the random number generator */
    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv, &tz);
    srand48((long)tv.tv_sec);


    /* Tab 1 */
    QWidget *w = new QWidget();

    QGridLayout *grid0 = new QGridLayout;

    fileLoad = new QPushButton(QString("Load File"), w);
    connect(fileLoad, SIGNAL(clicked(void)), this, SLOT(load(void)));
    grid0->addWidget(fileLoad, row, 0, 1, 1);
    fileSave = new QPushButton(QString("Save File"), w);
    connect(fileSave, SIGNAL(clicked(void)), this, SLOT(save(void)));
    grid0->addWidget(fileSave, row++, 1, 1, 1);

    nWellsLabel = new QLabel("Number of Wells", w);
    grid0->addWidget(nWellsLabel, row, 0, 1, 1);
    nWells = new QSpinBox(w);
    nWells->setRange(1, 64);
    nWells->setRange(1, MAX_WELLS);
    grid0->addWidget(nWells, row++, 1, 1, 1);
    nWells->setValue(3);

    nOutputBitsLabel = new QLabel("Number of Output Bits", w);
    grid0->addWidget(nOutputBitsLabel, row, 0, 1, 1);
    nOutputBits = new QSpinBox(w);
    nOutputBits->setRange(1, MAX_REWARD_BITS);
    grid0->addWidget(nOutputBits, row++, 1, 1, 1);

    /* set to the default value of 1 output bit */
    nOutputBits->setValue(1);

    /* when the create tabs button is pressed we recreate the second and third
     * tabs with the appropriate number of wells */
    useSequenceButton = new QRadioButton("Use Sequence", w);
    useSequenceButton->setAutoExclusive(false);
    grid0->addWidget(useSequenceButton, row++, 0, 1, 1);
    connect(useSequenceButton, SIGNAL(toggled(bool)), this,
            SLOT(enableOutputNextBits(bool)));

    nOutputNextBitsLabel = new QLabel("# Output if Next Bits", w);
    grid0->addWidget(nOutputNextBitsLabel, row, 0, 1, 1);
    nOutputNextBits = new QSpinBox(w);
    nOutputNextBits->setRange(0, MAX_REWARD_BITS);
    nOutputNextBits->setValue(0);
    grid0->addWidget(nOutputNextBits, row++, 1, 1, 1);

    /* this is only enabled when the useSequenceButton is checked */
    nOutputNextBits->setEnabled(false);

    ignoreWellsButton = new QRadioButton("Ignore Wells", w);
    ignoreWellsButton->setAutoExclusive(false);
    grid0->addWidget(ignoreWellsButton, row++, 0, 1, 1);

    avoidButton = new QRadioButton("Avoidance Task", w);
    avoidButton->setAutoExclusive(false);
    grid0->addWidget(avoidButton, row, 0, 1, 1);

    audioButton = new QRadioButton("Audio Task", w);
    audioButton->setAutoExclusive(false);
    audioButton->setChecked(false);
    grid0->addWidget(audioButton, row++, 1, 1, 1);

    audioBitButton = new QRadioButton("Audio on Output", w);
    audioBitButton->setAutoExclusive(false);
    audioBitButton->setChecked(false);
    grid0->addWidget(audioBitButton, row, 0, 1, 1);
    connect(audioBitButton, SIGNAL(clicked()), this, SLOT(enableAudioBit()));

    audioBitSoundFile = new QPushButton("No File", this);
    audioBitSoundFile->setEnabled(false);
    grid0->addWidget(audioBitSoundFile, row++, 1, 1, 1);
    /* we use the same architecture as for the sound that is specific to an goal well, but specify -1 as the
     * well number */
    connect(audioBitSoundFile, SIGNAL(clicked()), this, SLOT(setSoundFileName()));

    audioOutputBitLabel = new QLabel("Audio Output Bit", w);
    grid0->addWidget(audioOutputBitLabel, row, 0, 1, 1);
    audioOutputBit = new QSpinBox(w);
    audioOutputBit->setRange(-1, MAX_BITS);
    audioOutputBit->setValue(-1);
    audioOutputBit->setEnabled(false);
    grid0->addWidget(audioOutputBit, row++, 1, 1, 1);

    errorBitButton = new QRadioButton("Output on Error");
    errorBitButton->setAutoExclusive(false);
    errorBitButton->setChecked(false);
    grid0->addWidget(errorBitButton, row++, 0, 1, 1);


    createTabsButton = new QPushButton("Update Logic / Status", w);
    connect(createTabsButton, SIGNAL(clicked()), this, SLOT(createTabs()));
    grid0->addWidget(createTabsButton, row, 0, 1, 1);

    close = new QPushButton("Close", w);
    connect(close, SIGNAL(clicked()), this, SLOT(close()));
    grid0->addWidget(close, row++, 1, 1, 1);




    tablabel = QString("Well Info");
    w->setLayout(grid0);
    qtab->addTab(w, tablabel);

    //mainGrid->addWidget(qtab, 0, 0, 9, 9);

    /* Create the other tabs */
    createTabs();

    /* check to see if a configuration file has been specified, and if so, load it */
    if (rewardConfigFile.length() > 0) {
        readRewardConfig(rewardConfigFile);
    }

    if (moduleNet != NULL) {
        connect(moduleNet, SIGNAL(dataClientStarted()), this, SLOT(startDataClientStreaming()));
        qDebug() << "RewardGUI trying to connect to trodes server on" << moduleNet->trodesServerHost.toLatin1() << moduleNet->trodesServerPort;
        moduleNet->trodesClientConnect(moduleNet->trodesServerHost, moduleNet->trodesServerPort, false);
        // also set up the udp socket for direct communication with the ECU
        moduleNet->trodesECUClientConnect();
    }
    show();
}


rewardControl::~rewardControl()
{
}


void rewardControl::closeGUI(void)
{
    readyToClose = true;
    emit closeNow();
}

void rewardControl::closeEvent(QCloseEvent *)
{
    /* if we were started by trodes, we want trodes to tell us when to exit */
    if ((moduleNet != NULL) && !readyToClose) {
        //event->ignore();
    }
    else {
        if (moduleNet != NULL) {
            moduleNet->trodesClient->disconnectFromHost();
            moduleNet->trodesClient->deleteLater();
        }
    }
}

void rewardControl::startDataClientStreaming()
{
    // this gets called once the data clients have connected.
   qDebug() << "In startDataClientStreaming";


   if (moduleNet->dataClient.length() < 1) {
       qDebug() << "Error: no dataClients started";
       return;
   }
   disconnect(moduleNet->dataClient[0]->tcpSocket, SIGNAL(readyRead()), moduleNet->dataClient[0], SLOT(readMessage()));
   connect(moduleNet->dataClient[0]->tcpSocket, SIGNAL(readyRead()), this, SLOT(readData()));


   // tell the first data client (which is for Digital IO Info) to start streaming
   moduleNet->dataClient[0]->sendMessage((qint8) TRODESMESSAGE_TURNONDATASTREAM);
   qDebug() << "Data stream turned on";\
   // test: do we get data?

}

void rewardControl::readData()
{
    quint8 dataType;
    uint32_t dataSize;
    static DIOBuffer dioBuffer;

    moduleNet->dataClient[0]->readData(&dataType, &dataSize, (char *) &dioBuffer);

    if (dioBuffer.input == true) {
        // input port, so process it
        DIOInput(&dioBuffer);
    }
}

void rewardControl::connectSlots()
/* connect the required slots to handle the moduleID and quit messages from trodes */
{
    connect(moduleNet->trodesClient, SIGNAL(quitCommandReceived()), this, SLOT(closeGUI()));
    //connect(this, SIGNAL(closeNow()), this, SLOT(close()));

}


void rewardControl::enableAudioBit(void)
{
    bool enabled;

    enabled = audioBitButton->isChecked();
    audioOutputBit->setEnabled(enabled);
    audioBitSoundFile->setEnabled(enabled);
}

void rewardControl::loadFile(void)
{
    QString fileName = QFileDialog::getOpenFileName(this, "Choose a file to load",
                                                    ".", "All Files (*)", 0, 0);

    if (!fileName.isEmpty()) {
        readRewardConfig(fileName);
    }
}

void rewardControl::saveFile(void)
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Reward Config File"),
                                                    "",
                                                    tr("All Files (*)"));

    if (!fileName.isEmpty()) {
        /* write out the configuration to the file */
        qDebug() << "Writing reward config";
        writeRewardConfig(fileName);
    }
}

void rewardControl::writeRewardConfig(QString fileName)
{
    QFile file(fileName);
    int i, j;

    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream stream(&file);
        /* number of wells */
        stream << QString("nWells\t\t%1\n").arg(nWells->value());
        stream << QString("nOutputBits\t\t%1\n").arg(nOutputBits->value());
        stream << QString("useSequence\t\t%1\n").arg(useSequenceButton->isChecked());
        stream << QString("nOutputNextBits\t\t%1\n").arg(nOutputNextBits->value());

        /* task information */

        stream << QString("ignoreWells\t\t%1\n").arg(ignoreWellsButton->isChecked());
        stream << QString("avoidTask\t\t%1\n").arg(avoidButton->isChecked());
        stream << QString("audioTask\t\t%1\n").arg(audioButton->isChecked());
        stream << QString("audioOnOutput\t\t%1\n").arg(audioBitButton->isChecked());
        if (audioBitButton->isChecked()) {
            stream << QString("audioOutputBit\t%2\n").arg(audioOutputBit->value());
            stream << QString("audioBitSoundFile\t\t%1\n").arg(audioOutputBitFile);
        }

        stream << QString("errorOutput\t\t%1\n").arg(errorBitButton->isChecked());

        /* Well Information */
        for (i = 0; i < nWells->value(); i++) {
            stream << QString("well\t%1\t\n").arg(i);
            stream << QString("\tprev\t");
            for (j = 0; j < nWells->value(); j++) {
                stream << QString("%1  ").arg(prev[i]->item(j)->isSelected());
            }
            stream << QString("\n");
            stream << QString("\tcurr\t");
            for (j = 0; j < nWells->value(); j++) {
                stream << QString("%1  ").arg(curr[i]->item(j)->isSelected());
            }
            stream << QString("\n");
            /* if we have a list of ignore wells, write it out */
            if (ignoreWellsButton->isChecked()) {
                stream << QString("\tignore\t");
                for (j = 0; j < nWells->value(); j++) {
                    stream << QString("%1  ").arg(ignore[i]->item(j)->isSelected());
                }
                stream << QString("\n");
            }

            stream << QString("\tinputBit\t%1\t\n").arg(inputBit[i]->value());
            stream << QString("\ttriggerHigh\t%1\t\n").arg(triggerHigh[i]->isChecked());
            for (j = 0; j < nOutputBits->value(); j++) {
                stream << QString("\toutputBit\t%1\t%2\n").arg(j).
                    arg(outputBit[i][j]->value());
                stream << QString("\toutputBitLength\t%1\t%2\n").arg(j).
                    arg(outputBitLength[i][j]->value());
                stream << QString("\toutputBitPercent\t%1\t%2\n").arg(j).
                    arg(outputBitPercent[i][j]->value());
                stream << QString("\toutputBitDelay\t%1\t%2\n").arg(j).
                    arg(outputBitDelay[i][j]->value());
                if (errorBitButton->isChecked()) {
                    stream << QString("\terrorOutputBit\t%1\n").arg(errorOutputBit[i]->value());
                    stream << QString("\terrorOutputBitLength\t%1\n").arg(errorOutputBitLength[i]->value());
                    stream << QString("\terrorOutputBitDelay\t%1\n").arg(errorOutputBitDelay[i]->value());
                }
                for (j = 0; j < nOutputNextBits->value(); j++) {
                    stream << QString("\toutputNextBit\t%1\t%2\n").arg(j).
                        arg(outputNextBit[i][j]->value());
                    stream << QString("\toutputNextBitLength\t%1\t%2\n").arg(j).
                        arg(outputNextBitLength[i][j]->value());
                    stream << QString("\toutputNextBitDelay\t%1\t%2\n").arg(j).
                        arg(outputNextBitDelay[i][j]->value());
                }
                if (audioButton->isChecked()) {
                    stream << QString("\toutputNextAudioFile\t%1\t\n").arg(audioFile[i]);
                }
            }
        }
        stream << QString("firstReward\t%1\t\n").arg(firstRewardWell->value());
        file.close();
    }
    else {
        QMessageBox::critical(this, "Error", QString("Unable to write file").arg(fileName));
    }
}


void rewardControl::readRewardConfig(QString fileName)
{
    QFile file(fileName);
    int i, well;
    int nwells;

    QString s;
    QStringList slist;
    bool tabsCreated = false;

    if (file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);
        while (!stream.atEnd()) {
            s = stream.readLine();
            // trim off any leading or following spaces
            s = s.trimmed();

            /* Skip comments */
            if (s.contains("%")) {
                continue;
            }
            // split the string into sections separated by spaces.
            slist = s.split(QRegExp("\\s+"));

            //for(i = 0; i < slist.length(); i++)
            //  fprintf(stderr,"slist[%d] = %s\n", i, qPrintable(slist[i]));

            if (slist[0] == "nWells") {
                nwells = slist[1].toInt();
                nWells->setValue(nwells);
            }
            else if (slist[0] == "nOutputBits") {
                nOutputBits->setValue(slist[1].toInt());
            }
            else if (slist[0] == "nOutputNextBits") {
                nOutputNextBits->setValue(slist[1].toInt());
            }
            else if (slist[0] == "ignoreWells") {
                ignoreWellsButton->setChecked(slist[1].toInt());
            }
            else if (slist[0] == "useSequence") {
                useSequenceButton->setChecked(slist[1].toInt());
            }
            else if (slist[0] == "avoidTask") {
                avoidButton->setChecked(slist[1].toInt());
            }
            else if (slist[0] == "audioTask") {
                audioButton->setChecked(slist[1].toInt());
            }
            else if (slist[0] == "audioOnOutput") {
                audioBitButton->setChecked(slist[1].toInt());
            }
            else if (slist[0] == "audioOutputBit") {
                audioOutputBit->setValue(slist[1].toInt());
            }
            else if (slist[0] == "audioOutputBitFile") {
                audioOutputBitFile = slist[1];
                startSound(audioOutputBitFile);
            }
            else if (slist[0] == "errorOutput") {
                errorBitButton->setChecked(slist[1].toInt());
            }
            else if (slist[0] == "well") {
                /* this is the first entry after all of the task information,
                 * so we create the tabs if that hasn't been done */
                if (!tabsCreated) {
                    createTabs();
                    tabsCreated = true;
                }
                well = slist[1].toInt();
            }
            else if (slist[0] == "prev") {
                // read in the list of previous wells
                for (i = 0; i < nwells; i++) {
                    prev[well]->item(i)->setSelected(slist[i + 1].toInt());
                }
            }
            else if (slist[0] == "curr") {
                // read in the list of current wells
                for (i = 0; i < nwells; i++) {
                    curr[well]->item(i)->setSelected(slist[i + 1].toInt());
                }
            }
            else if (slist[0] == "ignore") {
                // read in the list of ignore wells
                for (i = 0; i < nwells; i++) {
                    ignore[well]->item(i)->setSelected(slist[i + 1].toInt());
                }
            }
            else if (slist[0] == "inputBit") {
                inputBit[well]->setValue(slist[1].toInt());
            }
            else if (slist[0] == "triggerHigh") {
                triggerHigh[well]->setChecked(slist[1].toInt());
            }
            else if (slist[0] == "outputBitLength") {
                outputBitLength[well][slist[1].toInt()]->setValue(slist[2].toInt());
            }
            else if (slist[0] == "outputBitDelay") {
                outputBitDelay[well][slist[1].toInt()]->setValue(slist[2].toInt());
            }
            else if (slist[0] == "outputBitPercent") {
                outputBitPercent[well][slist[1].toInt()]->setValue(slist[2].toInt());
            }
            else if (slist[0] == "outputBit") {
                outputBit[well][slist[1].toInt()]->setValue(slist[2].toInt());
            }
            else if (slist[0] == "outputNextBitLength") {
                outputNextBitLength[well][slist[1].toInt()]->setValue(slist[2].toInt());
            }
            else if (slist[0] == "outputNextBitDelay") {
                outputNextBitDelay[well][slist[1].toInt()]->setValue(slist[2].toInt());
            }
            else if (slist[0] == "outputNextBit") {
                outputNextBit[well][slist[1].toInt()]->setValue(slist[2].toInt());
            }
            else if (slist[0] == "outputNextSoundFile") {
                audioFile[well] = slist[1];
                startSound(well, audioFile[well]);
            }
            else if (slist[0] == "errorOutputBit") {
                errorOutputBit[well]->setValue(slist[1].toInt());
            }
            else if (slist[0] == "errorOutputBitLength") {
                errorOutputBitLength[well]->setValue(slist[1].toInt());
            }
            else if (slist[0] == "errorOutputBitDelay") {
                errorOutputBitDelay[well]->setValue(slist[1].toInt());
            }
            else if (slist[0] == "firstReward") {
                firstRewardWell->setValue(slist[1].toInt());
            }
            else {
                QMessageBox::critical(this, "Error",
                                      QString("Unable to parse config file\nText = %1").arg(s));
            }
        }
    }
    else {
        QMessageBox::critical(this, "Error",
                              QString("Unable to read file %1").arg(fileName));
    }
}


void rewardControl::loadSequence(void)
{
    QString s;
    int i, nextWell;

    QString fileName = QFileDialog::getOpenFileName(this, "Load sequence",
                                                    ".", "All Files (*)", 0, 0);

    if (!fileName.isEmpty()) {
        QFile file(fileName);
        if (file.open(QIODevice::ReadOnly)) {
            QTextStream stream(&file);
            /* clear out current items */
            wellSequenceListWidget->clear();
            while (!stream.atEnd()) {
                s = stream.readLine();
                //fprintf(stderr,"%s\n",s.ascii());
                /* put the current value into the list widget */
                wellSequenceListWidget->addItem(s);
            }
        }
        wellSequenceListWidget->setCurrentRow(0);
        /* set the next variables correctly */
        nextWell = wellSequenceListWidget->currentItem()->text().toInt();
        for (i = 0; i < nWells->value(); i++) {
            if (i == nextWell) {
                next[i]->setChecked(true);
            }
            else {
                next[i]->setChecked(false);
            }
        }
        nextWellOnErrorButton->setEnabled(true);
        setStatus();
    }
    else {
        QMessageBox::critical(this, "Error",
                              QString("Unable to read file %1").arg(fileName));
    }
}


void rewardControl::createLogicTab(int n)
{
    int i, j, col, row, nrows;
    /* create the logic tab (tab index 1) with the correct number of wells. We first get rid
     * of the current tab if it exists */
    /* see if the first page has been created */
    QWidget *w = qtab->widget(1);

    if (w) {
        qtab->removeTab(1);
        delete w;
    }

    prevWell = -1;      /* set as first trial */

    /* allocate space for the outputs */
    outputBitLabel = new QLabel* [nOutputBits->value()];
    outputBitLengthLabel = new QLabel* [nOutputBits->value()];
    outputBitPercentLabel = new QLabel* [nOutputBits->value()];
    outputBitDelayLabel = new QLabel* [nOutputBits->value()];

    outputBit = new QSpinBox * * [n];
    outputBitLength = new QSpinBox * * [n];
    outputBitPercent = new QSpinBox * * [n];
    outputBitDelay = new QSpinBox * * [n];

    /* the second index for the bits is the number of outputs */
    for (i = 0; i < nWells->value(); i++) {
        outputBit[i] = new QSpinBox* [nOutputBits->value()];
        outputBitLength[i] = new QSpinBox* [nOutputBits->value()];
        outputBitPercent[i] = new QSpinBox* [nOutputBits->value()];
        outputBitDelay[i] = new QSpinBox* [nOutputBits->value()];
    }


    /* allocate space for the output if next bits */
    if (nOutputNextBits->value() > 0) {
        outputNextBitLabel = new QLabel* [nOutputNextBits->value()];
        outputNextBitLengthLabel = new QLabel* [nOutputNextBits->value()];
        outputNextBitDelayLabel = new QLabel* [nOutputNextBits->value()];

        outputNextBit = new QSpinBox * * [n];
        outputNextBitLength = new QSpinBox * * [n];
        outputNextBitDelay = new QSpinBox * * [n];

        /* the second index for the bits is the number of outputNexts */
        for (i = 0; i < n; i++) {
            outputNextBit[i] = new QSpinBox* [nOutputNextBits->value()];
            outputNextBitLength[i] = new QSpinBox* [nOutputNextBits->value()];
            outputNextBitDelay[i] = new QSpinBox* [nOutputNextBits->value()];
        }
    }


    nrows = 7 + 4 * nOutputBits->value();
    if (audioButton->isChecked()) {
        nrows += 2;
    }


    w = new QWidget();
    QString tablabel2 = QString("Logic");
    qtab->insertTab(1, w, tablabel2);

    QGridLayout *grid1 = new QGridLayout;

    row = 1;

    /* create the well labels and the logic */
    prevLabel = new QLabel(QString("Prev Prev Well(s)"), w);
    grid1->addWidget(prevLabel, row++, 0, 1, 1);
    currLabel = new QLabel(QString("Prev Well(s)"), w);
    grid1->addWidget(currLabel, row++, 0, 1, 1);

    /* if we need to add the list of ignore wells, add the label */
    if (ignoreWellsButton->isChecked()) {
        ignoreLabel = new QLabel(QString("Ignore wells if next"), w);
        grid1->addWidget(ignoreLabel, row++, 0, 1, 1);
    }

    inputBitLabel = new QLabel(QString("Input Bit"), w);
    grid1->addWidget(inputBitLabel, row++, 0, 1, 1);

    row++;
    for (i = 0; i < nOutputBits->value(); i++) {
        outputBitLabel[i] = new QLabel(QString("Output Bit %1").arg(i), w);
        grid1->addWidget(outputBitLabel[i], row++, 0, 1, 1);

        outputBitLengthLabel[i] = new QLabel(QString("Output %1 Length (100 usec)").arg(i), w);
        grid1->addWidget(outputBitLengthLabel[i], row++, 0, 1, 1);

        outputBitDelayLabel[i] = new QLabel(QString("Output %1 Delay (100 usec)").arg(i), w);
        grid1->addWidget(outputBitDelayLabel[i], row++, 0, 1, 1);

        outputBitPercentLabel[i] = new QLabel(QString("Output %1 Percentage").arg(i), w);
        grid1->addWidget(outputBitPercentLabel[i], row++, 0, 1, 1);
    }
    if (errorBitButton->isChecked()) {
        /* add lines for the error bit */
        errorOutputBitLabel = new QLabel(QString("Error Output Bit"), w);
        grid1->addWidget(errorOutputBitLabel, row++, 0, 1, 1);

        errorOutputBitLengthLabel = new QLabel(QString("Error Output Length (100 usec)"), w);
        grid1->addWidget(errorOutputBitLengthLabel, row++, 0, 1, 1);
        errorOutputBitDelayLabel = new QLabel(QString("Error Output Delay (100 usec)"), w);
        grid1->addWidget(errorOutputBitDelayLabel, row++, 0, 1, 1);
    }

    /* add lines for the contingent output @ well 0 bits */
    for (i = 0; i < nOutputNextBits->value(); i++) {
        outputNextBitLabel[i] = new QLabel(QString("Output if Next Bit %1").arg(i),
                                           w);
        grid1->addWidget(outputNextBitLabel[i], row++, 0, 1, 1);

        outputNextBitLengthLabel[i] = new QLabel(QString("    Length (100 usec)"), w);
        grid1->addWidget(outputNextBitLengthLabel[i], row++, 0, 1, 1);

        outputNextBitDelayLabel[i] = new QLabel(QString("    Delay (100 usec)"), w);
        grid1->addWidget(outputNextBitDelayLabel[i], row++, 0, 1, 1);
    }


    if (audioButton->isChecked()) {
        outputNextSoundFileLabel = new QLabel(QString("Sound @ Well 0\n"), w);
        grid1->addWidget(outputNextSoundFileLabel, row++, 0, 1, 1);
        outputNextSoundFile = new QPushButton * [n];
        outputNextSoundButtonGroup = new QButtonGroup(this);
        connect(outputNextSoundButtonGroup, SIGNAL(buttonClicked(int)),
                this, SLOT(setSoundFileName(int)));

        /* create the audio file and sound pointers*/
        audioFile = new QString [n];
        sound = new QSound* [n];
        for (i = 0; i < n; i++) {
            sound[i] = NULL;
        }
        soundBit = NULL;
    }


    wellLabel = new QLabel* [n];
    prev = new QListWidget* [n];
    curr = new QListWidget* [n];
    ignore = new QListWidget* [n];
    inputBit = new QSpinBox* [n];
    triggerHigh = new QRadioButton* [n];

    if (errorBitButton->isChecked()) {
        errorOutputBit = new QSpinBox* [n];
        errorOutputBitLength = new QSpinBox* [n];
        errorOutputBitDelay = new QSpinBox* [n];
    }

    for (i = 0; i < n; i++) {
        col = i + 1;
        row = 0;
        wellLabel[i] = new QLabel(QString("Activate Well %1").arg(i), w);
        wellLabel[i]->setAlignment(Qt::AlignHCenter);
        grid1->addWidget(wellLabel[i], row++, col, 1, 1);
        prev[i] = new QListWidget(w);
        createRewardListWidget(prev[i]);
        connect(prev[i], SIGNAL(itemSelectionChanged()), this, SLOT(updateSendButton()));
        grid1->addWidget(prev[i], row++, col, 1, 1);
        curr[i] = new QListWidget(w);
        createRewardListWidget(curr[i]);
        connect(curr[i], SIGNAL(itemSelectionChanged()), this, SLOT(updateSendButton()));
        grid1->addWidget(curr[i], row++, col, 1, 1);

        if (ignoreWellsButton->isChecked()) {
            /* create the list of wells to ignore if this well is next*/
            ignore[i] = new QListWidget(w);
            createRewardListWidget(ignore[i]);
            connect(ignore[i], SIGNAL(itemSelectionChanged()), this, SLOT(updateSendButton()));
            grid1->addWidget(ignore[i], row++, col, 1, 1);
        }

        inputBit[i] = new QSpinBox(w);
        inputBit[i]->setRange(0, MAX_BITS);
        connect(inputBit[i], SIGNAL(valueChanged(int)), this, SLOT(updateSendButton()));
        grid1->addWidget(inputBit[i], row++, col, 1, 1);

        triggerHigh[i] = new QRadioButton("Trigger High", w);
        triggerHigh[i]->setAutoExclusive(false);
        connect(triggerHigh[i], SIGNAL(clicked()), this, SLOT(updateSendButton()));

        grid1->addWidget(triggerHigh[i], row++, col, 1, 1);

        for (j = 0; j < nOutputBits->value(); j++) {
            outputBit[i][j] = new QSpinBox(w);
            outputBit[i][j]->setRange(-1, MAX_BITS);
            outputBit[i][j]->setValue(-1);
            connect(outputBit[i][j], SIGNAL(valueChanged(int)), this, SLOT(updateSendButton()));

            grid1->addWidget(outputBit[i][j], row++, col, 1, 1);

            outputBitLength[i][j] = new QSpinBox(w);
            outputBitLength[i][j]->setRange(0, 1000000);
            connect(outputBitLength[i][j], SIGNAL(valueChanged(int)), this, SLOT(updateSendButton()));

            grid1->addWidget(outputBitLength[i][j], row++, col, 1, 1);

            outputBitDelay[i][j] = new QSpinBox(w);
            outputBitDelay[i][j]->setRange(0, 1000000);
            outputBitDelay[i][j]->setValue(0);
            connect(outputBitDelay[i][j], SIGNAL(valueChanged(int)), this, SLOT(updateSendButton()));

            grid1->addWidget(outputBitDelay[i][j], row++, col, 1, 1);

            outputBitPercent[i][j] = new QSpinBox(w);
            outputBitPercent[i][j]->setRange(0, 100);
            outputBitPercent[i][j]->setValue(100);
            connect(outputBitPercent[i][j], SIGNAL(valueChanged(int)), this, SLOT(updateSendButton()));
            grid1->addWidget(outputBitPercent[i][j], row++, col, 1, 1);
        }
        if (errorBitButton->isChecked()) {
            errorOutputBit[i] = new QSpinBox(w);
            errorOutputBit[i]->setRange(-1, MAX_BITS);
            errorOutputBit[i]->setValue(-1);
            connect(errorOutputBit[i], SIGNAL(valueChanged(int)), this, SLOT(updateSendButton()));
            grid1->addWidget(errorOutputBit[i], row++, col, 1, 1);

            errorOutputBitLength[i] = new QSpinBox(w);
            errorOutputBitLength[i]->setRange(0, 1000000);
            errorOutputBitLength[i]->setValue(0);
            connect(errorOutputBitLength[i], SIGNAL(valueChanged(int)), this, SLOT(updateSendButton()));
            grid1->addWidget(errorOutputBitLength[i], row++, col, 1, 1);

            errorOutputBitDelay[i] = new QSpinBox(w);
            errorOutputBitDelay[i]->setRange(0, 1000000);
            errorOutputBitDelay[i]->setValue(0);
            connect(errorOutputBitDelay[i], SIGNAL(valueChanged(int)), this, SLOT(updateSendButton()));
            grid1->addWidget(errorOutputBitDelay[i], row++, col, 1, 1);
        }

        for (j = 0; j < nOutputNextBits->value(); j++) {
            outputNextBit[i][j] = new QSpinBox(w);
            outputNextBit[i][j]->setRange(-1, MAX_BITS);
            outputNextBit[i][j]->setValue(-1);
            connect(outputNextBit[i][j], SIGNAL(valueChanged(int)), this, SLOT(updateSendButton()));
            grid1->addWidget(outputNextBit[i][j], row++, col, 1, 1);

            /* set this spin box to allow a value of -1 to correspond to on until next well */
            outputNextBitLength[i][j] = new QSpinBox(w);
            outputNextBitLength[i][j]->setRange(0, 1000000);
            outputNextBitLength[i][j]->setValue(0);
            outputNextBitLength[i][j]->setSpecialValueText("Trial");
            connect(outputNextBitLength[i][j], SIGNAL(valueChanged(int)), this, SLOT(updateSendButton()));
            grid1->addWidget(outputNextBitLength[i][j], row++, col, 1, 1);

            outputNextBitDelay[i][j] = new QSpinBox(w);
            outputNextBitLength[i][j]->setRange(0, 1000000);
            outputNextBitDelay[i][j]->setValue(0);
            connect(outputNextBitDelay[i][j], SIGNAL(valueChanged(int)), this, SLOT(updateSendButton()));
            grid1->addWidget(outputNextBitDelay[i][j], row++, col, 1, 1);
        }


        if (audioButton->isChecked()) {
            outputNextSoundFile[i] = new QPushButton("No File", this);
            outputNextSoundButtonGroup->addButton(outputNextSoundFile[i], i);
            connect(outputNextSoundFile[i], SIGNAL(clicked()), this, SLOT(updateSendButton()));
            grid1->addWidget(outputNextSoundFile[i], row++, col, 1, 1);
        }
    }
    // Add the button to send the script to the data acquisition system
    sendScriptButton = new QPushButton("Send script");
    connect(sendScriptButton, SIGNAL(pressed()), this, SLOT(sendScript()));

    grid1->addWidget(sendScriptButton, row++, 0, 1, n+1, Qt::AlignHCenter);
    w->setLayout(grid1);

}

void rewardControl::updateSendButton() {
    sendScriptButton->setText("Send script");
    sendScriptButton->setStyleSheet("color: rgb(255, 0, 0)");
    qtab->widget(2)->setEnabled(false);
}

void rewardControl::sendScript() {
    // create the state script that will allow us to trigger a sequence corresponding to each well

    QString *scriptString = new QString;
    int well, bit, delay, maxDelay, funcNum;
    QTextStream script(scriptString, QIODevice::WriteOnly);

    rewardWellFunction.clear();
    errorWellFunction.clear();
    // declare the random number variable
    script << "int randNum" << endl;
    for (well = 0; well < nWells->value(); well++) {
        // keep track of the maximum delay before everything is done so we can emit a finished message at the right time
        maxDelay = 0;
        rewardWellFunction.append(well+1);
        script << "function " << rewardWellFunction.last() << endl;
        for (bit = 0; bit < nOutputBits->value(); bit++) {
            if (outputBitPercent[well][bit]->value() != 100) {
                script << "    randNum = random(99)" << endl;
                script << "    if randNum > " << outputBitPercent[well][bit]->value() << " do " << endl;
            }
            // turn on the bit
            if ((delay = outputBitDelay[well][bit]->value()) > 0) {
                  script << "        do in " << delay;
            }
            script << "        portout[" << outputBit[well][bit]->value() << "]=1" << endl;
            // turn off the bit at the right time afterwards
            script << "        do in " << delay + outputBitLength[well][bit]->value() << " portout[" << outputBit[well][bit]->value() << "]=0" << endl;
            if (delay + outputBitLength[well][bit]->value() > maxDelay) {
                maxDelay = delay + outputBitLength[well][bit]->value();
            }
            if (outputBitPercent[well][bit]->value() != 100) {
                // end the if statement
                script << "    end" << endl;
            }
        }
        script << "    do in " << maxDelay << " disp('function " << rewardWellFunction.last() << " done')" << endl;
        script << "end;" << endl;
    }

    // if we have an error output defined, then we need to add functions for that as well.
    // We set the error functions to be the well number + 1 + MAX_WELLS to avoid overlap
    if (errorBitButton->isChecked()) {
        for (well = 0; well < nWells->value(); well++) {
            errorWellFunction.append(rewardWellFunction.last() + well + 1);
            script << "function " << errorWellFunction.last() << endl;
            // turn on the bit
            if ((delay = errorOutputBitDelay[well]->value()) > 0) {
                  script << "\tdo in " << delay << endl;
            }
            script << " portout[" << errorOutputBit[well]->value() << "]=1" << endl;
            // turn off the bit at the right time afterwards
            script << "\tdo in " << delay + errorOutputBitLength[well]->value() << " portout[" << errorOutputBit[well]->value() << "]=0" << endl;

            script << "end;" << endl;
        }
    }

    // finally, we need a function to turn everything off (TO DO)

    moduleNet->sendStateScript(scriptString);
    sendScriptButton->setText("Script Sent");
    sendScriptButton->setStyleSheet("color: rgb(0, 0, 0)");
    qtab->widget(2)->setEnabled(true);

}

void rewardControl::createRewardListWidget(QListWidget *lw)
{
    int i;

    QString s;
    QStringList l;

    s.truncate(0);
    for (i = 0; i < nWells->value(); i++) {
        l << QString("%1").arg(i);
    }
    lw->insertItems(0, l);
    /* allow multiple selections */
    lw->setSelectionMode(QAbstractItemView::ExtendedSelection);
}


void rewardControl::createStatusTab(int n)
{
    int i;

    /* create the status tab with the correct number of wells. We first get rid
     * of the current tab if it exists */
    QWidget *w = qtab->widget(2);

    if (w) {
        qtab->removeTab(2);
        delete w;
    }
    w = new QWidget(qtab);
    /* create the status tab with the correct number of wells */
    QString tablabel2 = QString("Status");
    qtab->insertTab(2, w, tablabel2);
    QGridLayout *grid2 = new QGridLayout(w);

    /* create the well labels and the logic  */
    firstTrial = new QRadioButton("First Trial", w);
    firstTrial->setChecked(true);
    grid2->addWidget(firstTrial, 1, 0, 1, 1);
    firstRewardLabel = new QLabel("First Reward Well");
    grid2->addWidget(firstRewardLabel, 1, 1, 1, 1);
    firstRewardWell = new QSpinBox(w);
    firstRewardWell->setRange(0, nWells->value() - 1);
    grid2->addWidget(firstRewardWell, 1, 2, 1, 1);
    connect(firstRewardWell, SIGNAL(valueChanged(int)), this,
            SLOT(setFirstReward(int)));


    /* create the well labels and the logic */
    reward = new QPushButton *[n];
    rewardButtonGroup = new QButtonGroup(this);
    next = new QRadioButton *[n];
    status = new QLabel *[n];
    rewardCounter.resize(n);
    for (i = 0; i < n; i++) {
        reward[i] = new QPushButton(QString("Well %1").arg(i), w);
        grid2->addWidget(reward[i], 2, i, 1, 1);
        rewardButtonGroup->addButton(reward[i], i);
        next[i] = new QRadioButton("Next", w);
        grid2->addWidget(next[i], 3, i, 1, 1);
        status[i] = new QLabel("status", w);
        status[i]->setAlignment(Qt::AlignHCenter);
        grid2->addWidget(status[i], 4, i, 1, 1);

        rewardCounter[i] = 0;
    }
    connect(rewardButtonGroup, SIGNAL(buttonClicked(int)), this,
            SLOT(setReward(int)));
    nextReward = new QPushButton("Next Reward", w);
    grid2->addWidget(nextReward, 5, n - 1, 1, 1, Qt::AlignTop);
    connect(nextReward, SIGNAL(clicked()), this, SLOT(setNextReward()));

    QPushButton *resetRewardCounterButton = new QPushButton("Reset Counters", w);
    grid2->addWidget(resetRewardCounterButton, 5, 0, 1, 1, Qt::AlignTop);
    connect(resetRewardCounterButton, SIGNAL(clicked()), this, SLOT(resetRewardCounters()));

    nextWellOnErrorButton = new QPushButton("0 Next on Error", w);
    nextWellOnErrorButton->setCheckable(true);
    grid2->addWidget(nextWellOnErrorButton, 7, 0, 1, 1, Qt::AlignVCenter);
    nextWellOnErrorButton->setEnabled(false);

    loadSequenceButton = new QPushButton("Load Sequence", w);
    connect(loadSequenceButton, SIGNAL(clicked()), this, SLOT(loadSequence()));
    // TO DO: get sequences working with new hardware
    loadSequenceButton->setEnabled(false);
    grid2->addWidget(loadSequenceButton, 9, 0, 1, 1, Qt::AlignVCenter);

    wellSequenceListWidget = new QListWidget(w);
    grid2->addWidget(wellSequenceListWidget, 6, 1, 4, 1, Qt::AlignVCenter);

    /* assume well 0 unless W track */
    if (n != 3) {
        firstRewardWell->setValue(0);
    }
    else {
        /* assume W track, so well 1 is the first to be rewarded */
        firstRewardWell->setValue(1);
    }
}

void rewardControl::setStatus()
{
    /* set the status labels correctly */
    int i, nNext = 0;

    for (i = 0; i < nWells->value(); i++) {
        if (next[i]->isChecked()) {
            status[i]->setText(QString("\n %1").arg(rewardCounter[i]));
            nNext++;
        }
        else if (prevWell == i) {
            if (prevRewarded) {
                rewardCounter[i]++;
                status[i]->setText(QString("Rewarded\n %1").arg(rewardCounter[i]));
            }
            else {
                status[i]->setText("Reward Omitted");
            }
        }
        else {
            status[i]->setText(QString("\n %1").arg(rewardCounter[i]));
        }
    }
    if (nNext == 1) {
        nextReward->setEnabled(true);
    }
    else {
        nextReward->setEnabled(false);
    }
}

void rewardControl::resetRewardCounters()
{
    /* reset the reward counters */

    for (int i = 0; i < nWells->value(); i++) {
        rewardCounter[i] = 0;
        status[i]->setText(QString("\n %1").arg(rewardCounter[i]));
    }
}


void rewardControl::rewardWell(int well, bool reward)
{
    /* put a reward at the well if we should and use the logic to update the nextwell
     * variable */
    int i, nBits;
    static int bit[MAX_REWARD_BITS];
    static int length[MAX_REWARD_BITS];
    static int delay[MAX_REWARD_BITS];
    static int percent[MAX_REWARD_BITS];

    int nextWell = 0;

    if (well < 0) {
        return;
    }

//    /* we first set the bit string to turn off any bits that were turned on forever in the last trial * */
      nBits = 0;
//    for (i = 0; i < nTrialBits; i++) {
//        bit[nBits] = trialBit[i];
//        /* Length of 1 will turn it on and then immediately off */
//        length[nBits] = 1;
//        delay[nBits] = 0;
//        percent[nBits++] = 100;
//    }
//    nTrialBits = 0;


    if (reward) {
//        if (useSequenceButton->isChecked()) {
//            if ((wellSequenceListWidget->currentRow() + 1) <
//                wellSequenceListWidget->count()) {
//                /* we first move on to the next row of the list*/
//                wellSequenceListWidget->setCurrentRow(wellSequenceListWidget->currentRow() + 1);
//                /* we need to look up the next well we will be using */
//                nextWell = wellSequenceListWidget->currentItem()->text().toInt();

//                /* set the output if next bits if there are any */
//                for (i = 0; i < nOutputNextBits->value(); i++) {
//                    bit[nBits] = outputNextBit[nextWell][i]->value();
//                    if ((length[nBits] = outputNextBitLength[nextWell][i]->value()) == -1) {
//                        /* add this bit to a list of bits that we need to shut off next time we reward a
//                         * well */
//                        trialBit[nTrialBits] = bit[nBits];
//                        nTrialBits++;
//                    }
//                    percent[nBits] = 100;
//                    delay[nBits++] = outputNextBitDelay[nextWell][i]->value();
//                }
//            }
//            else {
//                QMessageBox::critical(this, "Error",
//                                      "End of sequence list reached");
//                return;
//            }
//        }
        /* set the rest of the bits */
        for (i = 0; i < nOutputBits->value(); i++) {
            bit[nBits] = outputBit[well][i]->value();
            length[nBits] = outputBitLength[well][i]->value();
            delay[nBits] = outputBitDelay[well][i]->value();
            percent[nBits++] = outputBitPercent[well][i]->value();
        }

        /* check to see if we need to play a sound in conjuction with the bits */
        if (audioBitButton->isChecked()) {
            for (i = 0; i < nOutputBits->value(); i++) {
                if ((bit[i] != -1) && (bit[i] == audioOutputBit->value())) {
                    /* play the sound associated with the output bit audio file */
                    soundBit->play();
                    break;
                }
            }
        }
        // trigger the function for this well
        moduleNet->ecuClient->sendFunctionGoCommand(rewardWellFunction.at(well));
        prevRewarded = true;
        setAirTimer();  // no effect if avoidButton is not checked.
    }
    else {
        if (errorBitButton->isChecked()) {
            /* output the specified error bit */
            bit[nBits] = errorOutputBit[well]->value();
            /* check to see if we need to trigger an audio output */
            if (bit[nBits] == audioOutputBit->value()) {
                soundBit->play();
            }
            length[nBits] = errorOutputBitLength[well]->value();
            delay[nBits] = errorOutputBitDelay[well]->value();
            percent[nBits++] = 100;

            // trigger the error function for this well
            moduleNet->ecuClient->sendFunctionGoCommand(errorWellFunction.at(well));
            prevRewarded = false;

        }

//        if (useSequenceButton->isChecked() &&
//            (nextWellOnErrorButton->isChecked()) &&
//            !((well == 0) || (next[0]->isChecked()))) {
//            /* We need to move on to the next well if this was an error at
//             * well other than well 0 */
//            if ((wellSequenceListWidget->currentRow() + 1) <
//                wellSequenceListWidget->count()) {
//                /* we first move on to the next row of the list*/
//                wellSequenceListWidget->setCurrentRow(wellSequenceListWidget->currentRow()
//                                                      + 1);
//                /* we need to look up the next well we will be using */
//                nextWell = wellSequenceListWidget->currentItem()->text().toInt();
//            }
//            else {
//                QMessageBox::critical(this, "Error", "End of sequence list reached");
//                return;
//            }
//        }
//        else if (useSequenceButton->isChecked()) {
//            /* we didn't reward the animal and we shouldn't move on, so we reset the
//               output if next bits to keep them on */
//            for (i = 0; i < nOutputNextBits->value(); i++) {
//                bit[nBits] = outputNextBit[nextWell][i]->value();
//                if ((length[nBits] = outputNextBitLength[nextWell][i]->value()) == -1) {
//                    /* add this bit to a list of bits that we need to shut off next time we reward a
//                     * well */
//                    trialBit[nTrialBits] = bit[nBits];
//                    nTrialBits++;
//                }
//                percent[nBits] = 100;
//                delay[nBits++] = outputNextBitDelay[nextWell][i]->value();
//            }
//            TriggerOutputs(bit, length, delay, percent, nBits);
//            return;
//        }
    }

    /* Trigger the outputs.  Note that the number of bits is nBits */

    /* this is the end of the first trial */
    firstTrial->setChecked(false);

    for (i = 0; i < nWells->value(); i++) {
        //cout<<"i is "<<i<<"\n";
        if (!useSequenceButton->isChecked()) {
            /* we can't reward the same well twice, so we skip the current well's
             * entry */
            if (i != well) {
                if (((prevWell == -1) || prev[i]->item(prevWell)->isSelected()) &&
                    curr[i]->item(well)->isSelected()) {
                    /* we reward this well next. Note that this will update wellStatus*/
                    next[i]->setChecked(true);
                    //fprintf(stderr, "next[%d] true\n",  i);
                }
                else {
                    next[i]->setChecked(false);
                    //fprintf(stderr, "next[%d] false\n", i);
                }
            }
        }
        else {
            if (nextWell == i) {
                next[i]->setChecked(true);
            }
            else {
                next[i]->setChecked(false);
            }
        }
    }

    if (audioButton->isChecked()) {
        if (well == 0) {
            /* make sure we have 3 wells */
            if (nWells->value() == 3) {
                if (next[2]->isChecked()) {
                    /* emit the signal to play sound 2 */
                    sound[2]->play();
                }
                else {
                    sound[1]->play();
                }
            }
        }
        else {
            /* stop both sounds */
            sound[1]->stop();
            sound[2]->stop();
            //cout<<"port "<<well<<"\n";
        }
    }

    prevWell = well;
    this->setStatus();
}

int rewardControl::getNextWell(void)
{
    int i;

    for (i = 0; i < nWells->value(); i++) {
        if (next[i]->isChecked()) {
            return i;
        }
    }
    return -1;
}




void rewardControl::DIOInput(DIOBuffer *diobuf)
{
    int i, port, bit;
    int nextWell;


    /* First figure out which well is next */
    for (i = 0; i < nWells->value(); i++) {
        if (next[i]->isChecked()) {
            nextWell = i;
            break;
        }
    }

    /* check to see if the next well's bits have been set, update the status
     * and deliver reward if appropriate */
    for (i = 0; i < nWells->value(); i++) {
        bit = 1 << (inputBit[i]->value() % 16);
        if ((triggerHigh[i]->isChecked() && (diobuf->port ==  bit) && (diobuf->value == 1)) ||
            ((!triggerHigh[i]->isChecked() && (diobuf->port == bit) && (diobuf->value == 0)))) {
            /* we ignore this if the animal was just rewarded at this well or if it is in the ignore
             * list for the next well*/
            if ((prevWell != i) &&
                ((!ignoreWellsButton->isChecked()) ||
                 ((ignoreWellsButton->isChecked()) &&
                  (!ignore[nextWell]->item(i)->isSelected())))) {
                if (nextWell == i) {
                    fprintf(stderr, "rewardWell %d\n", i);
                    rewardWell(i, true);
                    break;
                }
                else {
                    fprintf(stderr, "rewardWell %d error\n", i);
                    /* this is an error */
                    rewardWell(i, false);
                    break;
                }
            }
        }
    }
}


void rewardControl::createAvoidTab(void)
{
    /* create the avoid tab. We first get rid
     * of the current tab if it exists */
    QWidget *w = qtab->widget(3);

    if (w) {
        qtab->removeTab(3);
        delete w;
    }
    w = new QWidget(qtab);
    /* create the avoid tab with the correct number of wells */
    QString tablabel3 = QString("Avoid");
    qtab->insertTab(3, w, tablabel3);
    QGridLayout *grid3 = new QGridLayout(w);


    restLengthLabel = new QLabel("Rest Length (ms)", w);
    grid3->addWidget(restLengthLabel, 1, 1, 0, 0);
    restLength = new QSpinBox(w);
    restLength->setRange(1, MAX_REST);
    restLength->setValue(30000);
    grid3->addWidget(restLength, 1, 1, 1, 1);

    warnPulseLabel = new QLabel("Warn Pulse (ms)", w);
    grid3->addWidget(warnPulseLabel, 4, 4, 0, 0);
    warnPulse = new QSpinBox(w);
    warnPulse->setRange(1, MAX_REST);
    warnPulse->setValue(200);
    grid3->addWidget(warnPulse, 4, 4, 1, 1);

    warnLengthLabel = new QLabel("Warn Length (ms)", w);
    grid3->addWidget(warnLengthLabel, 3, 3, 0, 0);
    warnLength = new QSpinBox(w);
    warnLength->setRange(1, MAX_REST);
    warnLength->setValue(25000);
    grid3->addWidget(warnLength, 3, 3, 1, 1);

    outputBitAirLabel = new QLabel("Output Bit - Air On", w);
    grid3->addWidget(outputBitAirLabel, 2, 2, 0, 0);
    outputBitAir = new QSpinBox(w);
    outputBitAir->setRange(0, MAX_BITS);
    outputBitAir->setValue(11);
    grid3->addWidget(outputBitAir, 2, 2, 1, 1);

    timerRest = new QTimer(this);
    connect(timerRest, SIGNAL(timeout()), this, SLOT(airOn()));
    timerRest->setSingleShot(true);

    timerWarn = new QTimer(this);
    connect(timerWarn, SIGNAL(timeout()), this, SLOT(airOn()));
    timerWarn->setSingleShot(true);

    timerWarnOff = new QTimer(this);
    connect(timerWarnOff, SIGNAL(timeout()), this, SLOT(warnOff()));
    timerWarnOff->setSingleShot(true);
}

void rewardControl::setAirTimer()
{
    if (avoidButton->isChecked()) {
        timerRest->start(restLength->value());
        timerWarn->start(warnLength->value());
        timerWarnOff->start(warnLength->value() + warnPulse->value());
    }
    ;
}

void rewardControl::setSoundFileName(void)
{
    /* first check to see if the file name is the same as has been set; if so
     * don't do anything.  Otherwise try to open the specified file to make
     * sure it exists, and if it can't be opened file load dialog window */
    /* This version of the function is for the audioOutputBit only */

    QString fileName;

    fileName = QFileDialog::getOpenFileName(this, tr("Open Sound File"),
                                            ".", tr("Sound Files (*.wav)"));
    /* this is a sound that will be associated with a specific bit */
    audioOutputBitFile = fileName;
    startSound(fileName);
}

void rewardControl::setSoundFileName(int well)
{
    /* first check to see if the file name is the same as has been set; if so
     * don't do anything.  Otherwise try to open the specified file to make
     * sure it exists, and if it can't be opened file load dialog window */

    QString fileName;

    fileName = QFileDialog::getOpenFileName(this, tr("Open Sound File"),
                                            ".", tr("Sound Files (*.wav)"));
    if (well != -1) {
        audioFile[well] = fileName;
    }
    else {
        /* this is a sound that will be associated with a specific bit */
        audioOutputBitFile = fileName;
    }

    startSound(fileName);
}

void rewardControl::startSound(int well, QString fileName)
{
    if (!fileName.isEmpty()) {
        /* check to see if the object exists, and if so get rid of it*/
        if (sound[well] != NULL) {
            delete sound[well];
        }
        /* creat the sound object */
        sound[well] = new QSound(fileName);

        /* get just the file for the button text */
        outputNextSoundFile[well]->setText(fileName.section('/', -1));
    }
    else {
        QMessageBox::critical(this, "Error", QString("Unable to load sound file %1").arg(fileName));
    }
}

void rewardControl::startSound(QString fileName)
/* this specifies the sound that will be played when a specific output bit is triggered */
{
    if (!fileName.isEmpty()) {
        /* check to see if the object exists, and if so get rid of it*/
        if (soundBit != NULL) {
            delete soundBit;
        }
        /* creat the sound object */
        soundBit = new QSound(fileName);

        audioBitSoundFile->setText(fileName.section('/', -1));
    }
    else {
        QMessageBox::critical(this, "Error", QString("Unable to load sound file %1").arg(fileName));
    }
}


#ifdef DEFINE_SET_REWARDS

void setRewardsDialog::reject()
{
    QDialog::reject();
}


setRewardsDialog::setRewardsDialog(QWidget* parent,
                                   const char* name, bool modal, Qt::WFlags fl, int port)
    : QDialog(parent, name, modal, fl), port(port)
{
    int ncols = 7;
    int nrows = BITS_PER_PORT + 1;
    int currentbit;
    int i;
    bool enabled;

    QString s;

    Q3GridLayout *grid = new Q3GridLayout(this, nrows, ncols, 0, 0, "CalibrateRewardsDialogLayout");

    QSizePolicy p(QSizePolicy::Preferred, QSizePolicy::Preferred, FALSE);
    QSizePolicy p2(QSizePolicy::Maximum, QSizePolicy::Maximum, FALSE);

    QFont fs("SansSerif", 10, QFont::Normal);

    outputNumberLabel = new QLabel *[BITS_PER_PORT];
    outputLengthLabel = new QLabel *[BITS_PER_PORT];
    outputLength = new SpikeLineEdit *[BITS_PER_PORT];
    pulse = new QPushButton *[BITS_PER_PORT];
    change = new QRadioButton *[BITS_PER_PORT];

    pulseButtonGroup = new Q3ButtonGroup(this);
    changeButtonGroup = new QButtonGroup(this);

    /* set the changeButtonGroup to be non exclusive */
    changeButtonGroup->setExclusive(false);

    enabled = false;
    /* if this an output port enable it */
    if (digioinfo.porttype[port] == OUTPUT_PORT) {
        enabled = true;
    }

    currentbit = port * BITS_PER_PORT;
    for (i = 0; i < BITS_PER_PORT; i++, currentbit++) {
        /* create the output number label */
        s = QString("Bit %1").arg(currentbit);
        outputNumberLabel[i] = new QLabel("outputNumberLabel", this, 0);
        outputNumberLabel[i]->setText(s);
        outputNumberLabel[i]->setFont(fs);
        outputNumberLabel[i]->setSizePolicy(p);
        grid->addWidget(outputNumberLabel[i], i, i, 0, 1);

        /* create the output length label */
        s = QString("Output Length (100 usec units)");
        outputLengthLabel[i] = new QLabel("outputLengthLabel", this, 0);
        outputLengthLabel[i]->setText(s);
        outputLengthLabel[i]->setFont(fs);
        outputLengthLabel[i]->setSizePolicy(p);
        grid->addWidget(outputLengthLabel[i], i, i, 2, 2);

        /* create the outputLength input */
        outputLength[i] = new SpikeLineEdit(this, currentbit);
        /* we set the depth here so that we can update it below  */
        s = QString("%1").arg(digioinfo.length[currentbit]);
        outputLength[i]->setText(s);
        outputLength[i]->setFont(fs);
        QIntValidator *valid = new QIntValidator(0, MAX_OUTPUT_PULSE_LENGTH,
                                                 this, "outputLengthValidator");
        outputLength[i]->setValidator(valid);
        grid->addWidget(outputLength[i], i, i, 3, 3);
        /* connect the update signal to the pulse update function */
        connect(outputLength[i], SIGNAL(updateVal(int, unsigned short)), this,
                SLOT(changePulseLength(int, unsigned short)));

        /* create the pulse button */
        pulse[i] = new QPushButton("Pulse", this, "pulse");
        pulse[i]->setSizePolicy(p);
        pulse[i]->setFont(fs);
        grid->addWidget(pulse[i], i, i, 4, 4);
        pulseButtonGroup->addButton(pulse[i], i);

        /* create the raise radio */
        change[i] = new QRadioButton("  Raise", this, "raise");
        change[i]->setSizePolicy(p);
        change[i]->setFont(fs);
        grid->addWidget(change[i], i, i, 6, 6);
        changeButtonGroup->addButton(change[i], i);

        /* check to see if we need to set this button to be down */
        if (digioinfo.raised[currentbit]) {
            change[i]->setChecked(true);
        }
        /* disable this line if this is an input port */
        outputLength[i]->setEnabled(enabled);
        pulse[i]->setEnabled(enabled);
        change[i]->setEnabled(enabled);
    }
    connect(pulseButtonGroup, SIGNAL(buttonClicked(int)), this, SLOT(pulseBit(int)));
    connect(changeButtonGroup, SIGNAL(buttonClicked(int)), this, SLOT(changeBit(int)));


    /* create the next and prev buttons */
    next = new QPushButton("Next Port", this, "NextPort");
    connect(next, SIGNAL(clicked()), this, SLOT(gotoNextPort()));
    grid->addWidget(next, nrows - 1, nrows - 1, 1, 1, 0);
    prev = new QPushButton("Previous Port", this, "NextPort");
    connect(prev, SIGNAL(clicked()), this, SLOT(gotoPrevPort()));
    grid->addWidget(prev, nrows - 1, nrows - 1, 2, 2, 0);

    /* create a close button at the bottom */
    close = new QPushButton("Close", this, "CloseDialog");
    connect(close, SIGNAL(clicked()), this, SLOT(dialogClosed()));
    grid->addWidget(close, nrows - 1, nrows - 1, ncols - 1, ncols - 1, 0);


    setAttribute(Qt::WA_DeleteOnClose, true);
    show();
}


setRewardsDialog::~setRewardsDialog()
{
}

void setRewardsDialog::dialogClosed()
{
    t
    emit finished();

    accept();
}


void setRewardsDialog::nextPort()
{
    /* create a new Dialog for the next tetrode (if available) */
    if (this->port < NUM_PORTS - 1) {
        new setRewardsDialog(dispinfo.spikeInfo, "rewarddialog", FALSE,
                             0, this->port + 1);
        /* now close the current dialog */
        accept();
    }
}

void setRewardsDialog::prevPort()
{
    /* create a new Dialog for the next tetrode (if available) */
    if (this->port > 0) {
        new setRewardsDialog(dispinfo.spikeInfo, "rewarddialog", FALSE,
                             0, this->port - 1);
        /* now close the current dialog */
        accept();
    }
}
#endif
