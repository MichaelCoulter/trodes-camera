/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "globalObjects.h"
#include "src-display/dialogs.h"

#include <QtGui>
#include <QGridLayout>

TrodesButton::TrodesButton(QWidget *parent)
    :QPushButton(parent) {

    setStyleSheet("QPushButton {border: 2px solid lightgray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton::pressed {border: 2px solid gray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton::checked {border: 2px solid gray;"
                          "border-radius: 4px;"
                          "padding: 2px;}");

    QFont buttonFont;
    buttonFont.setPointSize(12);



}

void TrodesButton::setRedDown(bool yes) {
    if (yes) {
        setStyleSheet("QPushButton {border: 2px solid lightgrey;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::pressed {border: 2px solid red;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::checked {border: 2px solid red;"
                              "border-radius: 4px;"
                              "padding: 2px;}");
    } else {
        setStyleSheet("QPushButton {border: 2px solid lightgray;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::pressed {border: 2px solid gray;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::checked {border: 2px solid gray;"
                              "border-radius: 4px;"
                              "padding: 2px;}");
    }
}

waveformGeneratorDialog::waveformGeneratorDialog(double currentCarrierFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent)
    :QWidget(parent)
{

    carrierFreqSpinBox = new QDoubleSpinBox(this);
    carrierFreqSpinBox->setMaximum(10);
    carrierFreqSpinBox->setMinimum(0.0);
    carrierFreqSpinBox->setSingleStep(0.1);
    carrierFreqSpinBox->setValue(currentCarrierFreq);
    carrierFreqSpinBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    freqSlider = new QSlider(Qt::Vertical);
    freqSlider->setMaximum(1000);
    freqSlider->setMinimum(1);
    freqSlider->setSingleStep(1);
    freqSlider->setValue(currentFreq);
    freqSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    ampSlider = new QSlider(Qt::Vertical);
    ampSlider->setMaximum(1000);
    ampSlider->setMinimum(0);
    ampSlider->setSingleStep(1);
    ampSlider->setValue(currentAmp);
    ampSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Vertical);
    threshSlider->setMaximum(1000);
    threshSlider->setMinimum(0);
    threshSlider->setSingleStep(1);
    threshSlider->setValue(currentThresh);
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);


    freqDisplay = new QLabel;
    freqDisplay->setNum(currentFreq);
    ampDisplay = new QLabel;
    ampDisplay->setNum(currentAmp);
    threshDisplay = new QLabel;
    threshDisplay->setNum(currentThresh);

    carrierFreqTitle = new QLabel(tr("Carrier (Hz)"));
    freqTitle = new QLabel(tr("Frequency (Hz)"));
    ampTitle = new QLabel(tr("Amplitude (uV)"));
    threshTitle = new QLabel(tr("Threshold (uV)"));


    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->addWidget(carrierFreqTitle,0,0,Qt::AlignCenter);
    mainLayout->addWidget(freqTitle,0,1,Qt::AlignCenter);
    mainLayout->addWidget(ampTitle,0,2,Qt::AlignCenter);
    mainLayout->addWidget(threshTitle,0,3,Qt::AlignCenter);

    mainLayout->addWidget(freqDisplay,1,1,Qt::AlignCenter);
    mainLayout->addWidget(ampDisplay,1,2,Qt::AlignCenter);
    mainLayout->addWidget(threshDisplay,1,3,Qt::AlignCenter);

    mainLayout->addWidget(carrierFreqSpinBox,2,0,Qt::AlignHCenter);
    mainLayout->addWidget(freqSlider,2,1,Qt::AlignHCenter);
    mainLayout->addWidget(ampSlider,2,2,Qt::AlignHCenter);
    mainLayout->addWidget(threshSlider,2,3,Qt::AlignHCenter);

    mainLayout->setRowStretch(2,1);
    mainLayout->setMargin(10);

    connect(freqSlider,SIGNAL(valueChanged(int)),freqDisplay,SLOT(setNum(int)));
    connect(ampSlider,SIGNAL(valueChanged(int)),ampDisplay,SLOT(setNum(int)));
    connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

    setLayout(mainLayout);
    setWindowTitle(tr("Waveform generator"));

}

void waveformGeneratorDialog::closeEvent(QCloseEvent* event) {
    emit windowClosed();
    event->accept();
}

//---------------------------------------------
