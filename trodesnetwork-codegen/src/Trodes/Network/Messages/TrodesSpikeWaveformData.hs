module Trodes.Network.Messages.TrodesSpikeWaveformData where

import Data.Int
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data TrodesSpikeWaveformData = TrodesSpikeWaveformData
    { localTimestamp :: Word32
    , nTrodeId :: Word32
    , samples :: [[Int16]]
    , systemTimestamp :: Int64
    } deriving (Generic, Cpp)
