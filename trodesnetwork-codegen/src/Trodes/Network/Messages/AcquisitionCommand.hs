module Trodes.Network.Messages.AcquisitionCommand where

import Data.Text (Text)
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data AcquisitionCommand = AcquisitionCommand
    { command :: Text
    , timestamp :: Word32
    } deriving (Generic, Cpp)
