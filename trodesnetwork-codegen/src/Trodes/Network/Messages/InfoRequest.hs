module Trodes.Network.Messages.InfoRequest where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data InfoRequest = InfoRequest
    { request :: Text
    } deriving (Generic, Cpp)
