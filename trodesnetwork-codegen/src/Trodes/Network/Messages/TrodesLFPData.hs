module Trodes.Network.Messages.TrodesLFPData where

import Data.Int
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data TrodesLFPData = TrodesLFPData
    { localTimestamp :: Word32
    , lfpData :: [Int16]
    , systemTimestamp :: Int64
    } deriving (Generic, Cpp)
