module Trodes.Network.Messages.SourceStatus where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data SourceStatus = SourceStatus
    { message :: Text
    } deriving (Generic, Cpp)
