module Trodes.Network.Types.HWStartStop where

import Data.Text (Text)
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp, CppType)

data HWStartStop = HWStartStop
    { startstop :: Text
    , slotgroup :: Text
    , number :: Word16
    } deriving (Generic, Cpp, CppType)
