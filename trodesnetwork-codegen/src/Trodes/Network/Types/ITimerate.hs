module Trodes.Network.Types.ITimerate where

import Data.Int
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp, CppType)

data ITimerate = ITimerate
    { timerate :: Int32
    } deriving (Generic, Cpp, CppType)
