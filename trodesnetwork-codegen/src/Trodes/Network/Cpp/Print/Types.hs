module Trodes.Network.Cpp.Print.Types where

import Data.Text as T
import Data.Text.Prettyprint.Doc
import Trodes.Network.Cpp.Ast

{- | Pretty printer for primitive Elm types. This pretty printer is used only to
display types of fields.
-}
cppPrimDoc :: CppPrim -> Doc ann
cppPrimDoc x = case x of
    CppBool          -> "bool"
    CppUint8         -> "uint8_t"
    CppUint16        -> "uint16_t"
    CppUint32        -> "uint32_t"
    CppUint64        -> "uint64"
    CppInt8          -> "int8_t"
    CppInt16         -> "int16_t"
    CppInt32         -> "int32_t"
    CppInt64         -> "int64_t"
    CppFloat         -> "float"
    CppDouble        -> "double"
    CppString        -> "std::string"
    CppVector a      -> "std::vector<" <+> cppTypeRefDoc a <+> ">"

cppTypeRefDoc :: TypeRef -> Doc ann
cppTypeRefDoc x = case x of
    RefPrim cppPrim -> cppPrimDoc cppPrim
    RefCustom (TypeName typeName) -> pretty typeName

printJuliaType :: CppRecordField -> Text
printJuliaType x = case cppRecordFieldType x of
    RefPrim prim -> printJuliaPrim prim
    RefCustom (TypeName typeName) -> typeName

printJuliaPrim  :: CppPrim -> Text
printJuliaPrim x = case x of
    CppBool          -> "bool"
    CppUint8         -> "UInt8"
    CppUint16        -> "UInt16"
    CppUint32        -> "UInt32"
    CppUint64        -> "UInt64"
    CppInt8          -> "Int8"
    CppInt16         -> "Int16"
    CppInt32         -> "Int32"
    CppInt64         -> "Int64"
    CppFloat         -> "Float32"
    CppDouble        -> "Float64"
    CppString        -> "AbstractString"
    CppVector a      -> "AbstractVector{" <> (T.pack $ show $ cppTypeRefDoc a) <> "}"

