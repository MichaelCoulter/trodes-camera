#include "displayprocessor.h"
#include "avxutils.h"
#include <immintrin.h> // for AVX
#include <cmath>
//#include "nmmintrin.h" // for SSE4.2

DisplayProcessor::DisplayProcessor()
{
    size_t space = sizeof(int32_t)*48 + 31;
    full_mask_buffer = new uint8_t[space];
    Aligned32Allocator allocator(full_mask_buffer, space);
    blendmask_lfp = allocator.aligned_alloc<int32_t>(16);
    blendmask_spk = allocator.aligned_alloc<int32_t>(16);
    blendmask_stim = allocator.aligned_alloc<int32_t>(16);
}

DisplayProcessor::~DisplayProcessor(){
    delete full_mask_buffer;
}

void DisplayProcessor::neuralSamples(const float *rawdata, const float *spikedata, const float *lfpdata, const float *stimdata, float *minValuesOutput, float *maxValuesOutput, bool newbin)
{
    for(int i = 0; i < 2; ++i){
        //=============================================================
        // display array
        //  disp = blend( blend(raw, lfp, masklfp), spk, maskspk)
        //  disp = min( max( disp, -5000 ), 5000 ) - not done anymore
        //=============================================================
        __m256 raw = _mm256_load_ps( &rawdata[i*8] );
        __m256 spk = _mm256_load_ps( &spikedata[i*8] );
        __m256 lfp = _mm256_load_ps( &lfpdata[i*8] );
        __m256 stim = _mm256_load_ps( &stimdata[i*8] );
        __m256 masklfp = _mm256_load_ps((float*)(&blendmask_lfp[i*8]));
        __m256 maskspk = _mm256_load_ps((float*)(&blendmask_spk[i*8]));
        __m256 maskstim = _mm256_load_ps((float*)(&blendmask_stim[i*8]));
        __m256 disp = _mm256_blendv_ps( _mm256_blendv_ps(raw, lfp, masklfp), spk, maskspk );

        //Final mask is for stimulation.
        disp = _mm256_blendv_ps(disp,stim,maskstim);

//        disp = _mm256_min_ps( _mm256_max_ps(disp, _mm256_set1_ps(-5000)), _mm256_set1_ps(5000));
//        _mm256_store_ps( &disp_output[i*8], disp);

        //=============================================================
        // Finding min/max of each bin
        //=============================================================
        if(!newbin){
            __m256 prevMins = _mm256_load_ps(&minValuesOutput[i*8]);
            __m256 prevMaxs = _mm256_load_ps(&maxValuesOutput[i*8]);
            __m256 min = _mm256_min_ps( disp, prevMins);
            __m256 max = _mm256_max_ps( disp, prevMaxs);

            _mm256_store_ps( &minValuesOutput[i*8], min);
            _mm256_store_ps( &maxValuesOutput[i*8], max);
        }
        else{
            _mm256_store_ps( &minValuesOutput[i*8], disp);
            _mm256_store_ps( &maxValuesOutput[i*8], disp);
        }
    }
}

int DisplayProcessor::setDisplay(int ind, DisplayProcessor::DisplaySetting disp)
{
    if(ind < 0 || ind > 15){
        //bad chan ind
        return -1;
    }

    //ind/8: 0-7 (0) or 8-15 (1)
    //ind%8: map 0-15 to 0-7
    blendmask_lfp[ind] = (disp==DisplaySetting::lfp)  ? 0x80000000 : 0x00000000;
    blendmask_spk[ind] = (disp==DisplaySetting::spike)? 0x80000000 : 0x00000000;
    blendmask_stim[ind] = (disp==DisplaySetting::stim)? 0x80000000 : 0x00000000;
    return 0;
}

//int DisplayProcessor::setStreamSize(int datalength, int timepoints)
//{
//    //hard coded for the case of (15000, 2000), or other fractional bins with 0.5
//    double binsize = (double)datalength / (double)timepoints;
//    if(binsize == (int)binsize){
//        //whole number
//        stream_inc_max[0] = (int)binsize;
//        stream_inc_max[1] = (int)binsize;
//    }
//    else{
//        //bins are 7.5, so need two whole numbers for bin sizes, 7 and 8
//        //fraction
//        stream_inc_max[0] = std::floor(binsize);
//        stream_inc_max[1] = std::ceil(binsize);
//    }
//    return 0;
//}

//int main(){
//    DisplayProcessor proc;
//    proc.setStreamBinSize(10);
//    float data[32] = {0,0,0,0,0,0,0,0,0,0,100,200,400,800,1600,0,-1600,800,400,200,100};
//    //         curmax:0,0,0,0,0,0,0,0,0,0,100,200,400,800,1600,
//    for(int n = 0; n < 10; ++n){
//        for(int i = 0; i < 25; ++i){
//            float data16[16] = {data[i]};
//            proc.neuralSamples(data16, data16, data16);
//            std::cout << proc.getCurrMaxValues()[0] << "\n";
//        }
//        std::cout << "\n";
//    }
//}
