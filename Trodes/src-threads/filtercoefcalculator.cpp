#include "filtercoefcalculator.h"

#include <cstdio>

FilterCoefCalculator::FilterCoefCalculator()
{
    options = order = polemask = 0;
    gain = 0;
}

bool FilterCoefCalculator::setIIRFilter(double corner1, double corner2, int order_in, double rate, uint options_in)
{
    //General purpose IIR filter creation function. Does not give access to specific poles.
    //The second corner input is unused for lowpass and highpass filters (it is required
    //for bandpass and bandstop filters).

    if ((options_in & opt_re) || (options_in & opt_pi) ) {
        //resonator and proportional-integral filters not supported with this access function
        return false;
    }

    options = order = polemask = 0;
    gain = 0;
    options = options_in;
    order = order_in;
    raw_alpha1 = corner1/rate;
    raw_alpha2 = corner2/rate;
    bool ok = checkoptions();
    if (!ok) {
        return false;
    }
    runCalculations();

    if (options & opt_bp) {
        gain = complex::hypot(fc_gain); //use gain at center
    } else if (options & opt_lp) {
        gain = complex::hypot(dc_gain); //use dc gain
    } else if (options & opt_hp) {
        gain = complex::hypot(hf_gain); //use high freq gain
    } else if (options & opt_bs) {
        gain = complex::hypot(dc_gain); //use dc gain
    }
    return true;

}

void FilterCoefCalculator::setBesselBandPass(double corner1, double corner2, double rate)
{
    //Convenience method. 2nd order bandpass filter with z transform

    options = order = polemask = 0;
    gain = 0;
    options |= opt_be; //Bessel filter
    options |= opt_bp; //Band pass filter
    options |= opt_z; //use matched z-transform. Allows near-linear phase characteristic.
    order = 2; //2nd order filter for each corner (so 4)
    raw_alpha1 = corner1/rate;
    raw_alpha2 = corner2/rate;
    runCalculations();
    gain = complex::hypot(fc_gain); //use gain at center
}

void FilterCoefCalculator::setBesselLowPass(double corner1, double rate)
{
    //Convenience method. 2nd order lowpass filter with z-transform

    options = order = polemask = 0;
    options |= opt_be; //Bessel filter
    options |= opt_lp; //Low pass filter
    options |= opt_z; //use matched z-transform. Allows near-linear phase characteristic.
    order = 2; //2nd order filter for each corner
    raw_alpha1 = corner1/rate;
    runCalculations();
    gain = complex::hypot(dc_gain); //use dc gain
}

void FilterCoefCalculator::getACoef(float *aPtr)
{
    if (gain == 0) return;
    for (int i=0; i < zplane.numpoles; i++) {
        *(aPtr+i) = (float)ycoeffs[i];
    }

}

void FilterCoefCalculator::getBCoef(float *bPtr)
{
    //ALL B COEFFICIENTS ARE COMBINED WITH THE 1/GAIN.
    //PRECALCULATING THE DIVISION AHEAD OF TIME SAVES CPU EFFORT
    if (gain == 0) return;
    for (int i = 0; i < zplane.numzeros+1; i++) {
        *(bPtr+i) = (float)(xcoeffs[i]/gain);
    }

}

bool FilterCoefCalculator::checkoptions()
{
    //Make sure all of the options are valid.

    bool optsok = true;
    unless (onebit(options & (opt_be | opt_bu | opt_ch | opt_re | opt_pi)))
            //opterror("must specify exactly one of -Be, -Bu, -Ch, -Re, -Pi");
            if (options & opt_re) {
        unless (onebit(options & (opt_bp | opt_bs | opt_ap))) {
            //must specify exactly one of -Bp, -Bs, -Ap with -Re
            optsok = false;
        }

        if (options & (opt_lp | opt_hp | opt_o | opt_p | opt_w | opt_z)) {
            //can't use -Lp, -Hp, -o, -p, -w, -z with -Re
            optsok = false;
        }
    } else if (options & opt_pi) {
        if (options & (opt_lp | opt_hp | opt_bp | opt_bs | opt_ap)) {
            // -Lp, -Hp, -Bp, -Bs, -Ap illegal in conjunction with -Pi
            optsok = false;
        }
        unless ((options & opt_o) && (order == 1)) {
            //"-Pi implies -o 1
            optsok = false;
        }
    } else {
        unless (onebit(options & (opt_lp | opt_hp | opt_bp | opt_bs))) {
            //must specify exactly one of -Lp, -Hp, -Bp, -Bs
            optsok = false;
        }
        if (options & opt_ap) {
            //-Ap implies -Re
            optsok = false;
        }
        if (options & opt_o) {
            unless (order >= 1 && order <= MAXORDER) {
                //order must be in range 1 .. MAXORDER
                optsok = false;
            }
            if (options & opt_p) {
                uint m = (1 << order) - 1; // "order" bits set
                if ((polemask & ~m) != 0) {
                    //args to -p must be in range 0 .. order-1
                    optsok = false;
                }
            }
        } else {
            //must specify -o
            optsok = false;
        }
    }
    unless (options & opt_a) {
        //must specify -a
        optsok = false;
    }
    return optsok;
}

void FilterCoefCalculator::runCalculations()
{
    setdefaults();
    if (options & opt_re)
    { if (options & opt_bp) compute_bpres();	   // bandpass resonator
        if (options & opt_bs) compute_notch();	   // bandstop resonator (notch)
        if (options & opt_ap) compute_apres();	   // allpass resonator
    }
    else
    { if (options & opt_pi)
        { prewarp();
            splane.poles[0] = 0.0;
            splane.zeros[0] = -TWOPI * warped_alpha1;
            splane.numpoles = splane.numzeros = 1;
        }
        else
        { compute_s();
            prewarp();
            normalize();
        }
        if (options & opt_z) compute_z_mzt(); else compute_z_blt();
    }
    if (options & opt_Z) add_extra_zero();
    expandpoly();
}


void FilterCoefCalculator::setdefaults()
{ unless (options & opt_p) polemask = ~0; /* use all poles */
    unless (options & (opt_bp | opt_bs)) raw_alpha2 = raw_alpha1;
}

void FilterCoefCalculator::compute_s() /* compute S-plane poles for prototype LP filter */
{ splane.numpoles = 0;
    if (options & opt_be)
    { /* Bessel filter */
        int p = (order*order)/4; /* ptr into table */
        if (order & 1) choosepole(bessel_poles[p++]);
        for (int i = 0; i < order/2; i++)
        { choosepole(bessel_poles[p]);
            choosepole(complex::cconj(bessel_poles[p]));
            p++;
        }
    }
    if (options & (opt_bu | opt_ch))
    { /* Butterworth filter */
        for (int i = 0; i < 2*order; i++)
        { double theta = (order & 1) ? (i*PI) / order : ((i+0.5)*PI) / order;
            choosepole(complex::expj(theta));
        }
    }
    if (options & opt_ch)
    { /* modify for Chebyshev (p. 136 DeFatta et al.) */
        if (chebrip >= 0.0)
        { fprintf(stderr, "mkfilter: Chebyshev ripple is %g dB; must be .lt. 0.0\n", chebrip);
            exit(1);
        }
        double rip = pow(10.0, -chebrip / 10.0);
        double eps = sqrt(rip - 1.0);
        double y = asinh(1.0 / eps) / (double) order;
        if (y <= 0.0)
        { fprintf(stderr, "mkfilter: bug: Chebyshev y=%g; must be .gt. 0.0\n", y);
            exit(1);
        }
        for (int i = 0; i < splane.numpoles; i++)
        { splane.poles[i].re *= sinh(y);
            splane.poles[i].im *= cosh(y);
        }
    }
}

void FilterCoefCalculator::choosepole(complex z)
{ if (z.re < 0.0)
    { if (polemask & 1) splane.poles[splane.numpoles++] = z;
        polemask >>= 1;
    }
}

void FilterCoefCalculator::prewarp()
{ /* for bilinear transform, perform pre-warp on alpha values */
    if (options & (opt_w | opt_z))
    { warped_alpha1 = raw_alpha1;
        warped_alpha2 = raw_alpha2;
    }
    else
    { warped_alpha1 = tan(PI * raw_alpha1) / PI;
        warped_alpha2 = tan(PI * raw_alpha2) / PI;
    }
}

void FilterCoefCalculator::normalize()		/* called for trad, not for -Re or -Pi */
{ double w1 = TWOPI * warped_alpha1;
    double w2 = TWOPI * warped_alpha2;
    /* transform prototype into appropriate filter type (lp/hp/bp/bs) */
    switch (options & (opt_lp | opt_hp | opt_bp| opt_bs))
    { case opt_lp:
    { for (int i = 0; i < splane.numpoles; i++) splane.poles[i] = splane.poles[i] * w1;
        splane.numzeros = 0;
        break;
    }

    case opt_hp:
    { int i;
        for (i=0; i < splane.numpoles; i++) splane.poles[i] = w1 / splane.poles[i];
        for (i=0; i < splane.numpoles; i++) splane.zeros[i] = 0.0;	 /* also N zeros at (0,0) */
        splane.numzeros = splane.numpoles;
        break;
    }

    case opt_bp:
    { double w0 = sqrt(w1*w2), bw = w2-w1; int i;
        for (i=0; i < splane.numpoles; i++)
        { complex hba = 0.5 * (splane.poles[i] * bw);
            complex temp = complex::csqrt(1.0 - complex::sqr(w0 / hba));
            splane.poles[i] = hba * (1.0 + temp);
            splane.poles[splane.numpoles+i] = hba * (1.0 - temp);
        }
        for (i=0; i < splane.numpoles; i++) splane.zeros[i] = 0.0;	 /* also N zeros at (0,0) */
        splane.numzeros = splane.numpoles;
        splane.numpoles *= 2;
        break;
    }

    case opt_bs:
    { double w0 = sqrt(w1*w2), bw = w2-w1; int i;
        for (i=0; i < splane.numpoles; i++)
        { complex hba = 0.5 * (bw / splane.poles[i]);
            complex temp = complex::csqrt(1.0 - complex::sqr(w0 / hba));
            splane.poles[i] = hba * (1.0 + temp);
            splane.poles[splane.numpoles+i] = hba * (1.0 - temp);
        }
        for (i=0; i < splane.numpoles; i++)	   /* also 2N zeros at (0, +-w0) */
        { splane.zeros[i] = complex(0.0, +w0);
            splane.zeros[splane.numpoles+i] = complex(0.0, -w0);
        }
        splane.numpoles *= 2;
        splane.numzeros = splane.numpoles;
        break;
    }
    }
}

void FilterCoefCalculator::compute_z_blt() /* given S-plane poles & zeros, compute Z-plane poles & zeros, by bilinear transform */
{ int i;
    zplane.numpoles = splane.numpoles;
    zplane.numzeros = splane.numzeros;
    for (i=0; i < zplane.numpoles; i++) zplane.poles[i] = blt(splane.poles[i]);
    for (i=0; i < zplane.numzeros; i++) zplane.zeros[i] = blt(splane.zeros[i]);
    while (zplane.numzeros < zplane.numpoles) zplane.zeros[zplane.numzeros++] = -1.0;
}

complex FilterCoefCalculator::blt(complex pz)
{ return (2.0 + pz) / (2.0 - pz);
}

void FilterCoefCalculator::compute_z_mzt() /* given S-plane poles & zeros, compute Z-plane poles & zeros, by matched z-transform */
{ int i;
    zplane.numpoles = splane.numpoles;
    zplane.numzeros = splane.numzeros;
    for (i=0; i < zplane.numpoles; i++) zplane.poles[i] = complex::cexp(splane.poles[i]);
    for (i=0; i < zplane.numzeros; i++) zplane.zeros[i] = complex::cexp(splane.zeros[i]);
}

void FilterCoefCalculator::compute_notch()
{ /* compute Z-plane pole & zero positions for bandstop resonator (notch filter) */
    compute_bpres();		/* iterate to place poles */
    double theta = TWOPI * raw_alpha1;
    complex zz = complex::expj(theta);	/* place zeros exactly */
    zplane.zeros[0] = zz; zplane.zeros[1] = complex::cconj(zz);
}

void FilterCoefCalculator::compute_apres()
{ /* compute Z-plane pole & zero positions for allpass resonator */
    compute_bpres();		/* iterate to place poles */
    zplane.zeros[0] = reflect(zplane.poles[0]);
    zplane.zeros[1] = reflect(zplane.poles[1]);
}

complex FilterCoefCalculator::reflect(complex z)
{ double r = complex::hypot(z);
    return z / sqr(r);
}

void FilterCoefCalculator::compute_bpres()
{ /* compute Z-plane pole & zero positions for bandpass resonator */
    zplane.numpoles = zplane.numzeros = 2;
    zplane.zeros[0] = 1.0; zplane.zeros[1] = -1.0;
    double theta = TWOPI * raw_alpha1; /* where we want the peak to be */
    if (infq)
    { /* oscillator */
        complex zp = complex::expj(theta);
        zplane.poles[0] = zp; zplane.poles[1] = complex::cconj(zp);
    }
    else
    { /* must iterate to find exact pole positions */
        complex topcoeffs[MAXPZ+1]; expand(zplane.zeros, zplane.numzeros, topcoeffs);
        double r = exp(-theta / (2.0 * qfactor));
        double thm = theta, th1 = 0.0, th2 = PI;
        bool cvg = false;
        for (int i=0; i < 50 && !cvg; i++)
        { complex zp = r * complex::expj(thm);
            zplane.poles[0] = zp; zplane.poles[1] = complex::cconj(zp);
            complex botcoeffs[MAXPZ+1]; expand(zplane.poles, zplane.numpoles, botcoeffs);
            complex g = complex::evaluate(topcoeffs, zplane.numzeros, botcoeffs, zplane.numpoles, complex::expj(theta));
            double phi = g.im / g.re; /* approx to atan2 */
            if (phi > 0.0) th2 = thm; else th1 = thm;
            if (fabs(phi) < EPS) cvg = true;
            thm = 0.5 * (th1+th2);
        }
        unless (cvg) fprintf(stderr, "mkfilter: warning: failed to converge\n");
    }
}

void FilterCoefCalculator::add_extra_zero()
{ if (zplane.numzeros+2 > MAXPZ)
    { fprintf(stderr, "mkfilter: too many zeros; can't do -Z\n");
        exit(1);
    }
    double theta = TWOPI * raw_alphaz;
    complex zz = complex::expj(theta);
    zplane.zeros[zplane.numzeros++] = zz;
    zplane.zeros[zplane.numzeros++] = complex::cconj(zz);
    while (zplane.numpoles < zplane.numzeros) zplane.poles[zplane.numpoles++] = 0.0;	 /* ensure causality */
}

void FilterCoefCalculator::expandpoly() /* given Z-plane poles & zeros, compute top & bot polynomials in Z, and then recurrence relation */
{ complex topcoeffs[MAXPZ+1], botcoeffs[MAXPZ+1]; int i;
    expand(zplane.zeros, zplane.numzeros, topcoeffs);
    expand(zplane.poles, zplane.numpoles, botcoeffs);
    dc_gain = complex::evaluate(topcoeffs, zplane.numzeros, botcoeffs, zplane.numpoles, 1.0);
    double theta = TWOPI * 0.5 * (raw_alpha1 + raw_alpha2); /* "jwT" for centre freq. */
    fc_gain = complex::evaluate(topcoeffs, zplane.numzeros, botcoeffs, zplane.numpoles, complex::expj(theta));
    hf_gain = complex::evaluate(topcoeffs, zplane.numzeros, botcoeffs, zplane.numpoles, -1.0);
    for (i = 0; i <= zplane.numzeros; i++) xcoeffs[i] = +(topcoeffs[i].re / botcoeffs[zplane.numpoles].re);
    for (i = 0; i <= zplane.numpoles; i++) ycoeffs[i] = -(botcoeffs[i].re / botcoeffs[zplane.numpoles].re);
}

void FilterCoefCalculator::expand(complex pz[], int npz, complex coeffs[])
{ /* compute product of poles or zeros as a polynomial of z */
    int i;
    coeffs[0] = 1.0;
    for (i=0; i < npz; i++) coeffs[i+1] = 0.0;
    for (i=0; i < npz; i++) multin(pz[i], npz, coeffs);
    /* check computed coeffs of z^k are all real */
    for (i=0; i < npz+1; i++)
    { if (fabs(coeffs[i].im) > EPS)
        { fprintf(stderr, "mkfilter: coeff of z^%d is not real; poles/zeros are not complex conjugates\n", i);
            exit(1);
        }
    }
}

void FilterCoefCalculator::multin(complex w, int npz, complex coeffs[])
{ /* multiply factor (z-w) into coeffs */
    complex nw = -w;
    for (int i = npz; i >= 1; i--) coeffs[i] = (nw * coeffs[i]) + coeffs[i-1];
    coeffs[0] = nw * coeffs[0];
}

void FilterCoefCalculator::printresults()
{ if (options & opt_l)
    { /* just list parameters */
        //printcmdline(argv);
        complex gain = (options & opt_pi) ? hf_gain :
                                            (options & opt_lp) ? dc_gain :
                                                                 (options & opt_hp) ? hf_gain :
                                                                                      (options & (opt_bp | opt_ap)) ? fc_gain :
                                                                                                                      (options & opt_bs) ? complex::csqrt(dc_gain * hf_gain) : complex(1.0);
        printf("G  = %.10e\n", complex::hypot(gain));
        printcoeffs("NZ", zplane.numzeros, xcoeffs);
        printcoeffs("NP", zplane.numpoles, ycoeffs);
    }
    else
    { //printf("Command line: ");
        //printcmdline(argv);
        printfilter();
    }
}



void FilterCoefCalculator::printcoeffs(const char *pz, int npz, double coeffs[])
{ printf("%s = %d\n", pz, npz);
    for (int i = 0; i <= npz; i++) printf("%18.10e\n", coeffs[i]);
}

void FilterCoefCalculator::printfilter()
{ printf("raw alpha1    = %14.10f\n", raw_alpha1);
    printf("raw alpha2    = %14.10f\n", raw_alpha2);
    unless (options & (opt_re | opt_w | opt_z))
    { printf("warped alpha1 = %14.10f\n", warped_alpha1);
        printf("warped alpha2 = %14.10f\n", warped_alpha2);
    }
    printgain("dc    ", dc_gain);
    printgain("centre", fc_gain);
    printgain("hf    ", hf_gain);
    putchar('\n');
    unless (options & opt_re) printrat_s();
    printrat_z();
    printrecurrence();
}

void FilterCoefCalculator::printgain(const char *str, complex gain)
{ double r = complex::hypot(gain);
    printf("gain at %s:   mag = %15.9e", str, r);
    if (r > EPS) printf("   phase = %14.10f pi", complex::atan2(gain) / PI);
    putchar('\n');
}

void FilterCoefCalculator::printrat_s()	/* print S-plane poles and zeros */
{ printf("S-plane zeros:\n");
    printpz(splane.zeros, splane.numzeros);
    printf("S-plane poles:\n");
    printpz(splane.poles, splane.numpoles);
}

void FilterCoefCalculator::printrat_z()	/* print Z-plane poles and zeros */
{ printf("Z-plane zeros:\n");
    printpz(zplane.zeros, zplane.numzeros);
    printf("Z-plane poles:\n");
    printpz(zplane.poles, zplane.numpoles);
}

void FilterCoefCalculator::printpz(complex *pzvec, int num)
{ int n1 = 0;
    while (n1 < num)
    { putchar('\t'); prcomplex(pzvec[n1]);
        int n2 = n1+1;
        while (n2 < num && pzvec[n2] == pzvec[n1]) n2++;
        if (n2-n1 > 1) printf("\t%d times", n2-n1);
        putchar('\n');
        n1 = n2;
    }
    putchar('\n');
}


void FilterCoefCalculator::printrecurrence() /* given (real) Z-plane poles & zeros, compute & print recurrence relation */
{ printf("Recurrence relation:\n");
    printf("y[n] = ");
    int i;
    for (i = 0; i < zplane.numzeros+1; i++)
    {
        if (i > 0) printf("     + ");
        double x = xcoeffs[i];
        double f = fmod(fabs(x), 1.0);
        const char *fmt = (f < EPS || f > 1.0-EPS) ? "%3g" : "%14.10f";
        putchar('('); printf(fmt, x); printf(" * x[n-%2d])\n", zplane.numzeros-i);
    }
    putchar('\n');
    for (i = 0; i < zplane.numpoles; i++)
    {
        printf("     + (%14.10f * y[n-%2d])\n", ycoeffs[i], zplane.numpoles-i);
    }
    putchar('\n');
}

void FilterCoefCalculator::prcomplex(complex z)
{ printf("%14.10f + j %14.10f", z.re, z.im);
}



