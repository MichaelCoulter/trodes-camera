#ifndef AUXCHANPROCESSOR_H
#define AUXCHANPROCESSOR_H

#include <stdint.h>
#include <vector>
#include <string>

//Not currently using vectorized instructions here. problem is that there is a variable number of aux channels
//possible fix is to create subclasses for MCU, ECU, and interleaved data. then, since those are fixed in size,
// can write optimized code to calculate values for each one, and then provide functions to only get the data
// the user wants.
class alignas(32) AuxChanProcessor
{
public:
    AuxChanProcessor(bool ECU);

    //Add channels. Returns an id(index) back to user
    //  - All byte/bit info should be with respect to the start of the packet in newSamples(data)
    //  - All data returned to the user is in the order that the channels are added
    void addInt16Channel(int startByte, std::string idstring);
    void addDigitalChannel(int startByte, int digitalBit, std::string idstring);
    void addInterleavedInt16Channel(int IL_IDByte, int IL_IDBit, int startByte, std::string idstring);
    void addInterleavedDigitalChannel(int IL_IDByte, int IL_IDBit, int startByte, int digitalBit, std::string idstring);

    //
    void setStreamBinSize(int size);

    //number of channels
    int numchans() const {return values.size();}

    //Sample data to update channel values
    void newSamples(int16_t *data);

//    //get last int16 values
//    const int16_t* getLastValues() const {return values.data();}

    //get binned values (min and max for display)
    const float* getMaxValues() const {return maxValues.data();}
    const float* getMinValues() const {return minValues.data();}

    //get state changes. one slot for each channel. if >0, subtract 1 to get the state
    //  - 0 if no state changed or not digital.
    //  - 1 if state changed and new state is 0
    //  - 2 if state changed and new state is 1
    const uint8_t* getStateChanges() const {return stateChanges.data();}



private:
    bool ECUConnected;
    int stream_inc;
    int stream_inc_max;

    struct int16chan
    {
        int startByte;
        int pos;
    };
    struct digitalchan
    {
        int startByte;
        int digitalBit;
        int pos;
    };
    struct int16chan_IL
    {
        int startByte;
        int interleavedDataIDByte;
        int interleavedDataIDBit;
        int16_t dataState;
        int pos;
    };
    struct digitalchan_IL{
        int startByte;
        int digitalBit;
        int interleavedDataIDByte;
        int interleavedDataIDBit;
        int16_t dataState;
        int pos;
    };

    std::vector<int16chan> analogChans;
    std::vector<digitalchan> digitalChans;
    std::vector<int16chan_IL> interleavedAnalogChans;
    std::vector<digitalchan_IL> interleavedDigitalChans;

    std::vector<int16_t> values;
    std::vector<float> minValues;
    std::vector<float> maxValues;
    std::vector<uint8_t> stateChanges;

//    uint8_t prevMCU_IO;
//    uint8_t MCUChanges;
//    uint64_t prevECU_DIO;
//    uint64_t ECUChanges;
//    int16_t ECUAnalog[12];
};

#endif // AUXCHANPROCESSOR_H
