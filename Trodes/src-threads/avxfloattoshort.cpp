#include "avxfloattoshort.h"
#include <immintrin.h> // for AVX

//AVXFloatToShort::AVXFloatToShort()
//    : output()
//{

//}

//https://stackoverflow.com/questions/41228180/how-can-i-convert-a-vector-of-float-to-short-int-using-avx-instructions
void AVXFloatToShort::convertToShort(const float *data, int16_t *output)
{
    __m256i tmp1 = _mm256_cvtps_epi32(_mm256_loadu_ps(data));
    __m256i tmp2 = _mm256_cvtps_epi32(_mm256_loadu_ps(data+8));
    tmp1 = _mm256_packs_epi32(tmp1, tmp2);
    tmp1 = _mm256_permute4x64_epi64(tmp1, 0xD8);
    _mm256_storeu_si256((__m256i*)output, tmp1);
}

