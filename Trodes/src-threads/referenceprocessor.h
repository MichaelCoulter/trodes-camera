#ifndef REFERENCEPROCESSOR_H
#define REFERENCEPROCESSOR_H

#include <stdint.h>
#include <array>




//16 channel reference processor
//  fn(16x16-bit ints) => 16x32-bit floats
class alignas(32) ReferenceProcessor
{
public:
//    ReferenceProcessor();

    //must be 16 samples AND must be aligned along 32-byte boundary
    //Algorithm:
    //  * Convert to 32-bit ints
    //  * Zero out NaN values
    //  * Gather references from packetstart
    //  * Apply references
    static void applyReferences(const int16_t *data, const void *packetstart, const void *carvaluestart, float *aligned_output,
                    const int32_t *refindices, const int32_t *refmasks, const int32_t *refgroup_indices, const int32_t *refgroup_masks);

    static void gatherUnreferenced(const void *packetstart, const int32_t *indices, int16_t *output, int startIndex);
//    const float* getReferenceOutput() const {return x;}

//    //Set reference channel for channel at ind
//    //  * refchan:  [0:N-1]: reference channel, where N is num chans in packet
//    //  * refchan:  -1     : no reference
//    //  * refgroup: [0:G-1]: reference group, where G is the num groups
//    //  * refgroup: -1     : no group reference
//    int setReference(int ind, int refchan, int refgroup);

//private:
//    typedef std::array<int32_t, 8> avxi32;

//    float x[16];
//    avxi32 refindices[2];
//    avxi32 refmasks[2];
//    avxi32 refgroup_indices[2];
//    avxi32 refgroup_masks[2];
//    int refs[16];
//    int refgroups[16];
};

#endif // REFERENCEPROCESSOR_H
