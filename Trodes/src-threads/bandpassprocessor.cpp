#include "bandpassprocessor.h"
#include "avxutils.h"
#include <immintrin.h> // for AVX
#include <string.h>




//******************************************
BandPassProcessor::BandPassProcessor()
    : freq(30000)
{

    size_t N = 16*15*sizeof(float) + 31; //14 arrays of 16 floats each. +31 for extra alignment.
    full_buffer = new char[N];
    memset(full_buffer, 0, N);

    Aligned32Allocator a(full_buffer, N);

    bp_x0 = a.aligned_alloc<float>(16);
    bp_x1 = a.aligned_alloc<float>(16);
    bp_x2 = a.aligned_alloc<float>(16);

    bp_y0 = a.aligned_alloc<float>(16);
    bp_y1 = a.aligned_alloc<float>(16);
    bp_y2 = a.aligned_alloc<float>(16);
    bp_y3 = a.aligned_alloc<float>(16);
    bp_y4 = a.aligned_alloc<float>(16);

    bp_b0 = a.aligned_alloc<float>(16);
    bp_b1 = a.aligned_alloc<float>(16);
    bp_b2 = a.aligned_alloc<float>(16);

    bp_a0 = a.aligned_alloc<float>(16);
    bp_a1 = a.aligned_alloc<float>(16);
    bp_a2 = a.aligned_alloc<float>(16);
    bp_a3 = a.aligned_alloc<float>(16);
//    std::cout << "bp: " << bp_x0 <<" "<<bp_y0<<" "<<bp_b0<<" "<<bp_a0<<" "<<bp_a3<<"\n";
}

BandPassProcessor::~BandPassProcessor(){
    delete full_buffer;
}

//void BandPassProcessor::setOutputBuffer(float *output)
//{
//    bp_y4 = output;
//    std::cout << "output:" << bp_y4 << "\n";
//}
void BandPassProcessor::newSamples(const float *data, float *output){
    //shift input data over and copy new data
    std::copy(bp_x1, bp_x1+16, bp_x0);
    std::copy(bp_x2, bp_x2+16, bp_x1);
    std::copy(data, data+16, bp_x2);

    //shift y data over
    std::copy( bp_y1, bp_y1+16, bp_y0 ); //bp_y0 = bp_y1
    std::copy( bp_y2, bp_y2+16, bp_y1 ); //bp_y1 = bp_y2
    std::copy( bp_y3, bp_y3+16, bp_y2 ); //bp_y2 = bp_y3
    std::copy( bp_y4, bp_y4+16, bp_y3 ); //bp_y3 = bp_y4

    //=============================================================
    // bandpass filter
    //  y4 = b0*x0 + b1*x1 + b2*x2 + a0*y0 + a1*y1 + a2*y2 + a3*y3
    //=============================================================

    for(int i = 0; i < 2; i++){

        //load data y and a
        __m256 y0 = _mm256_load_ps( &bp_y0[i*8] );
        __m256 y1 = _mm256_load_ps( &bp_y1[i*8] );
        __m256 y2 = _mm256_load_ps( &bp_y2[i*8] );
        __m256 y3 = _mm256_load_ps( &bp_y3[i*8] );
        __m256 a0 = _mm256_load_ps( &bp_a0[i*8] );
        __m256 a1 = _mm256_load_ps( &bp_a1[i*8] );
        __m256 a2 = _mm256_load_ps( &bp_a2[i*8] );
        __m256 a3 = _mm256_load_ps( &bp_a3[i*8] );

        //multiply step.
        __m256 ay0 = _mm256_mul_ps(y0, a0);
        __m256 ay1 = _mm256_mul_ps(y1, a1);
        __m256 ay2 = _mm256_mul_ps(y2, a2);
        __m256 ay3 = _mm256_mul_ps(y3, a3);

        //load data x and b
        __m256 b0 = _mm256_load_ps( &bp_b0[i*8] );
        __m256 b1 = _mm256_load_ps( &bp_b1[i*8] );
        __m256 b2 = _mm256_load_ps( &bp_b2[i*8] );
        __m256 x0 = _mm256_load_ps( &bp_x0[i*8] );
        __m256 x1 = _mm256_load_ps( &bp_x1[i*8] );
        __m256 x2 = _mm256_load_ps( &bp_x2[i*8] );

        //multiply step.
        __m256 bx0 = _mm256_mul_ps(x0, b0);
        __m256 bx1 = _mm256_mul_ps(x1, b1);
        __m256 bx2 = _mm256_mul_ps(x2, b2);

        //combine step
        __m256 temp0 = _mm256_add_ps(ay0, ay1);
        __m256 temp1 = _mm256_add_ps(ay2, ay3);
        __m256 temp2 = _mm256_add_ps(bx0, bx1);
        temp0 = _mm256_add_ps( _mm256_add_ps(temp0, temp1), _mm256_add_ps(temp2, bx2) );

        //store value to y[4] and output
        _mm256_store_ps( &bp_y4[i*8], temp0 );
        _mm256_store_ps( output+i*8, temp0 );
    }
}

struct BPCoeffs{
    float a[4];
    float b[3];
};

BPCoeffs calculateBandPassCoeffs(int low, int high, int freq){
    BPCoeffs coeffs;

    //Calculate filter coefficients. We use a 2nd-order bandpass Bessel with z-transorm
    FilterCoefCalculator coefCalculator;
    coefCalculator.setBesselBandPass(low,high,freq);
    coefCalculator.getACoef(coeffs.a);
    coefCalculator.getBCoef(coeffs.b);

    //coefCalculator.printresults();

    return coeffs;

    /*if(freqkhz == 20){
        if(high==6000){
            switch (low){
                case 600:
                    coeffs.a[0] = -2.38101038e-02;coeffs.a[1] = 1.58046179e-01; coeffs.a[2] = -1.02350641e+00; coeffs.a[3] = 1.86678165e+00;
                    coeffs.b[0] = 1.0/1.23800264; coeffs.b[1] = -2.0/1.23800264; coeffs.b[2] = 1.0/1.23800264;
                    break;
                case 500:
                    coeffs.a[0] = -2.22178133e-02; coeffs.a[1] = 1.52131530e-01; coeffs.a[2] = -1.05071373e+00; coeffs.a[3] = 1.90545106e+00;
                    coeffs.b[0] = 1.0/1.20545731; coeffs.b[1] = -2.0/1.20545731; coeffs.b[2] = 1.0/1.20545731;
                    break;
                case 400:
                    coeffs.a[0] = -2.07320065e-02; coeffs.a[1] = 1.46194940e-01; coeffs.a[2] = -1.07871832e+00; coeffs.a[3] = 1.94359966e+00;
                    coeffs.b[0] = 1.0/1.17420052; coeffs.b[1] = -2.0/1.17420052; coeffs.b[2] = 1.0/1.17420052;
                    break;
                case 300:
                    coeffs.a[0] = -1.93455624e-02; coeffs.a[1] = 1.40254217e-01; coeffs.a[2] = -1.10748253e+00; coeffs.a[3] = 1.98123459e+00;
                    coeffs.b[0] = 1.0/1.14412319; coeffs.b[1] = -2.0/1.14412319; coeffs.b[2] = 1.0/1.14412319;
                    break;
                case 200:
                    coeffs.a[0] = -1.80518362e-02; coeffs.a[1] = 1.34325714e-01; coeffs.a[2] = -1.13696987e+00; coeffs.a[3] = 2.01836293e+00;
                    coeffs.b[0] = 1.0/1.11512355; coeffs.b[1] = -2.0/1.11512355; coeffs.b[2] = 1.0/1.11512355;
                    break;
                default:
                    break;
            }
        }else
        if(high == 5000){
            switch (low){
                case 600:
                    coeffs.a[0] = -4.75730559e-02; coeffs.a[1] = 3.19724005e-01; coeffs.a[2] = -1.29833010e+00; coeffs.a[3] = 2.00538788e+00;
                    coeffs.b[0] = 1.0/1.37930186; coeffs.b[1] = -2.0/1.37930186; coeffs.b[2] = 1.0/1.37930186;
                    break;
                case 500:
                    coeffs.a[0] = -4.43916281e-02; coeffs.a[1] = 3.08647293e-01; coeffs.a[2] = -1.31597067e+00; coeffs.a[3] = 2.03756722e+00;
                    coeffs.b[0] = 1.0/1.33628491; coeffs.b[1] = -2.0/1.33628491; coeffs.b[2] = 1.0/1.33628491;
                    break;
                case 400:
                    coeffs.a[0] = -4.14229570e-02; coeffs.a[1] = 2.97633780e-01; coeffs.a[2] = -1.33437656e+00; coeffs.a[3] = 2.06929230e+00;
                    coeffs.b[0] = 1.0/1.29535440; coeffs.b[1] = -2.0/1.29535440; coeffs.b[2] = 1.0/1.29535440;
                    break;
                case 300:
                    coeffs.a[0] = -3.86528144e-02; coeffs.a[1] = 2.86703951e-01; coeffs.a[2] = -1.35351316e+00; coeffs.a[3] = 2.10056998e+00;
                    coeffs.b[0] = 1.0/1.25630199; coeffs.b[1] = -2.0/1.25630199; coeffs.b[2] = 1.0/1.25630199;
                    break;
                case 200:
                    coeffs.a[0] = -3.60679240e-02; coeffs.a[1] = 2.75876518e-01; coeffs.a[2] = -1.37334682e+00; coeffs.a[3] = 2.13140696e+00;
                    coeffs.b[0] = 1.0/1.21893637; coeffs.b[1] = -2.0/1.21893637; coeffs.b[2] = 1.0/1.21893637;
                    break;
                default:
                    break;
            }
        }
    }else
    if(freqkhz == 25){
        if(high == 6000){
            switch(low){
                case 600:
                    coeffs.a[0] = -5.02815933e-02;	coeffs.a[1] = 3.48684444e-01; coeffs.a[2] = -1.38919690e+00; coeffs.a[3] = 2.07810697e+00;
                    coeffs.b[0] = 1.0/1.36751963; coeffs.b[1] = -2.0/1.36751963; coeffs.b[2] = 1.0/1.36751963;
                    break;
                case 500:
                    coeffs.a[0] = -4.75730559e-02;	coeffs.a[1] = 3.38729900e-01; coeffs.a[2] = -1.40193286e+00; coeffs.a[3] = 2.10211093e+00;
                    coeffs.b[0] = 1.0/1.33245599; coeffs.b[1] = -2.0/1.33245599; coeffs.b[2] = 1.0/1.33245599;
                    break;
                case 400:
                    coeffs.a[0] = -4.50104204e-02;	coeffs.a[1] = 3.28850323e-01; coeffs.a[2] = -1.41513805e+00; coeffs.a[3] = 2.12584358e+00;
                    coeffs.b[0] = 1.0/1.29875076; coeffs.b[1] = -2.0/1.29875076; coeffs.b[2] = 1.0/1.29875076;
                    break;
                case 300:
                    coeffs.a[0] = -4.25858273e-02;	coeffs.a[1] = 3.19055281e-01; coeffs.a[2] = -1.42879574e+00; coeffs.a[3] = 2.14930825e+00;
                    coeffs.b[0] = 1.0/1.26628842; coeffs.b[1] = -2.0/1.26628842; coeffs.b[2] = 1.0/1.26628842;
                    break;
                case 200:
                    coeffs.a[0] = -4.02918406e-02;	coeffs.a[1] = 3.09353642e-01; coeffs.a[2] = -1.44288954e+00; coeffs.a[3] = 2.17250821e+00;
                    coeffs.b[0] = 1.0/1.23496107; coeffs.b[1] = -2.0/1.23496107; coeffs.b[2] = 1.0/1.23496107;
                    break;
                default:
                    break;
            }
        }else
        if(high == 5000){
            switch (low){
                case 600:
                    coeffs.a[0] = -8.74759847e-02; coeffs.a[1] = 5.69019625e-01; coeffs.a[2] = -1.74058481e+00; coeffs.a[3] = 2.24784666e+00;
                    coeffs.b[0] = 1.0/1.60543552; coeffs.b[1] = -2.0/1.60543552; coeffs.b[2] = 1.0/1.60543552;
                    break;
                case 500:
                    coeffs.a[0] = -8.27638832e-02; coeffs.a[1] = 5.52744730e-01; coeffs.a[2] = -1.74282375e+00; coeffs.a[3] = 2.26521197e+00;
                    coeffs.b[0] = 1.0/1.55430544; coeffs.b[1] = -2.0/1.55430544; coeffs.b[2] = 1.0/1.55430544;
                    break;
                case 400:
                    coeffs.a[0] = -7.83056102e-02; coeffs.a[1] = 5.36691196e-01; coeffs.a[2] = -1.74557249e+00; coeffs.a[3] = 2.28239258e+00;
                    coeffs.b[0] = 1.0/1.50567341; coeffs.b[1] = -2.0/1.50567341; coeffs.b[2] = 1.0/1.50567341;
                    break;
                case 300:
                    coeffs.a[0] = -7.40874927e-02; coeffs.a[1] = 5.20865749e-01; coeffs.a[2] = -1.74881700e+00; coeffs.a[3] = 2.29939113e+00;
                    coeffs.b[0] = 1.0/1.45929276; coeffs.b[1] = -2.0/1.45929276; coeffs.b[2] = 1.0/1.45929276;
                    break;
                case 200:
                    coeffs.a[0] = -7.00965940e-02; coeffs.a[1] = 5.05274487e-01; coeffs.a[2] = -1.75254342e+00; coeffs.a[3] = 2.31621017e+00;
                    coeffs.b[0] = 1.0/1.41493722; coeffs.b[1] = -2.0/1.41493722; coeffs.b[2] = 1.0/1.41493722;
                    break;
                default:
                    break;
            }
        }
    }else
    if(freqkhz == 30){
        if(high == 6000){
            switch(low){
                case 600:
                    coeffs.a[0] = -8.27638832e-02; coeffs.a[1] = 5.52744730e-01; coeffs.a[2] = -1.74282375e+00; coeffs.a[3] = 2.26521197e+00;
                    coeffs.b[0] = 1.0/1.55430544; coeffs.b[1] = -2.0/1.55430544; coeffs.b[2] = 1.0/1.55430544;
                    break;
                case 500:
                    coeffs.a[0] = -7.90316183e-02; coeffs.a[1] = 5.39351119e-01; coeffs.a[2] = -1.74507956e+00; coeffs.a[3] = 2.27954186e+00;
                    coeffs.b[0] = 1.0/1.51361600; coeffs.b[1] = -2.0/1.51361600; coeffs.b[2] = 1.0/1.51361600;
                    break;
                case 400:
                    coeffs.a[0] = -7.54676612e-02; coeffs.a[1] = 5.26115166e-01; coeffs.a[2] = -1.74768127e+00; coeffs.a[3] = 2.29374502e+00;
                    coeffs.b[0] = 1.0/1.47451718; coeffs.b[1] = -2.0/1.47451718; coeffs.b[2] = 1.0/1.47451718;
                    break;
                case 300:
                    coeffs.a[0] = -7.20644219e-02; coeffs.a[1] = 5.13040488e-01; coeffs.a[2] = -1.75062083e+00; coeffs.a[3] = 2.30782293e+00;
                    coeffs.b[0] = 1.0/1.43687527; coeffs.b[1] = -2.0/1.43687527; coeffs.b[2] = 1.0/1.43687527;
                    break;
                case 200:
                    coeffs.a[0] = -6.88146528e-02; coeffs.a[1] = 5.00130406e-01; coeffs.a[2] = -1.75389027e+00; coeffs.a[3] = 2.32177706e+00;
                    coeffs.b[0] = 1.0/1.40056555; coeffs.b[1] = -2.0/1.40056555; coeffs.b[2] = 1.0/1.40056555;
                    break;
                default:
                    break;
            }
        }else
        if(high == 5000){
            switch(low){
            case 600:
                coeffs.a[0] = -1.31292736e-01; coeffs.a[1] = 8.11261017e-01; coeffs.a[2] = -2.13366443e+00; coeffs.a[3] = 2.44719019e+00;
                coeffs.b[0] = 1.0/1.89565447; coeffs.b[1] = -2.0/1.89565447; coeffs.b[2] = 1.0/1.89565447;
                break;
            case 500:
                coeffs.a[0] = -1.25372047e-01; coeffs.a[1] = 7.91119608e-01; coeffs.a[2] = -2.12566535e+00; coeffs.a[3] =  2.45547481e+00;
                coeffs.b[0] = 1.0/1.83293842; coeffs.b[1] = -2.0/1.83293842; coeffs.b[2] = 1.0/1.83293842;
                break;
            case 400:
                coeffs.a[0] = -1.19718353e-01; coeffs.a[1] = 7.71300258e-01; coeffs.a[2] = -2.11808631e+00; coeffs.a[3] =  2.46370798e+00;
                coeffs.b[0] = 1.0/1.77333585; coeffs.b[1] = -2.0/1.77333585; coeffs.b[2] = 1.0/1.77333585;
                break;
            case 300:
                coeffs.a[0] = -1.14319614e-01; coeffs.a[1] = 7.51802234e-01; coeffs.a[2] = -2.11092035e+00; coeffs.a[3] =  2.47189069e+00;
                coeffs.b[0] = 1.0/1.71654394; coeffs.b[1] = -2.0/1.71654394; coeffs.b[2] = 1.0/1.71654394;
                break;
            case 200:
                coeffs.a[0] = -1.09164333e-01; coeffs.a[1] = 7.32624654e-01; coeffs.a[2] = -2.10416047e+00; coeffs.a[3] =  2.48002389e+00;
                coeffs.b[0] = 1.0/1.66228545; coeffs.b[1] = -2.0/1.66228545; coeffs.b[2] = 1.0/1.66228545;
                break;
            }
        }
    }
    return coeffs;
    */
}

int BandPassProcessor::setBandPassFilter(int ind, int low, int high, bool on){
    if(ind < 0 || ind > 15){
        //bad chan ind
        return -1;
    }

    if(!on){
        //Turn off filter by turning equation into y = 1*x
        bp_b2[ind] = 1.0;
        bp_b0[ind] = 0.0;
        bp_b1[ind] = 0.0;
        bp_a0[ind] = 0.0;
        bp_a1[ind] = 0.0;
        bp_a2[ind] = 0.0;
        bp_a3[ind] = 0.0;
        return 0;
    }

    BPCoeffs coeffs = calculateBandPassCoeffs(low, high, freq);
    bp_lo[ind] = low;
    bp_hi[ind] = high;

    //ind/8: 0-7 (0) or 8-15 (1)
    //ind%8: map 0-15 to 0-7
    bp_a0[ind] = coeffs.a[0];
    bp_a1[ind] = coeffs.a[1];
    bp_a2[ind] = coeffs.a[2];
    bp_a3[ind] = coeffs.a[3];

    bp_b0[ind] = coeffs.b[0];
    bp_b1[ind] = coeffs.b[1];
    bp_b2[ind] = coeffs.b[2];

    return 0;
}

int BandPassProcessor::setSamplingRate(int samplingratehz){
    /*if(samplingratehz != 20000 && samplingratehz != 30000){
        return -1;
    }*/

    freq = samplingratehz;
    return 0;
}

void BandPassProcessor::reset()
{
    memset(bp_x0, 0, 16);
    memset(bp_x1, 0, 16);
    memset(bp_x2, 0, 16);
    memset(bp_y0, 0, 16);
    memset(bp_y1, 0, 16);
    memset(bp_y2, 0, 16);
    memset(bp_y3, 0, 16);
}


//-----------------------------------

InvertProcessor::InvertProcessor()

{
    /*size_t N = 16*15*sizeof(float) + 31; //14 arrays of 16 floats each. +31 for extra alignment.
    full_buffer = new char[N];
    memset(full_buffer, 0, N);

    Aligned32Allocator a(full_buffer, N);*/


}

InvertProcessor::~InvertProcessor(){

}

void InvertProcessor::invert(const float *data, float *aligned_output)
{
    //__m256i ones = _mm256_set1_epi8(0xFF);
    __m256 negativeOnes = _mm256_set1_ps(-1.0);


    //load, compare, store first 8 elements
    __m256 dps = _mm256_load_ps(data);
    __m256 multResult = _mm256_mul_ps (dps, negativeOnes);
    _mm256_store_ps(aligned_output, multResult);

    //load, compare, store second 8 elements
    __m256 dps2 = _mm256_load_ps(data + 8);
    __m256 multResult2 = _mm256_mul_ps (dps2, negativeOnes);
    _mm256_store_ps(aligned_output+8, multResult2);



}
