﻿#include "probelayoutpanel.h"

ProbeLayoutPanel::ProbeLayoutPanel(const QList<QList<selectionModeData> > &allSelectionModeArgs, QWidget *parent)
    :QChartView(new QChart(), parent)
{

    numberOfProbes = allSelectionModeArgs.length();

    setRenderHint(QPainter::Antialiasing);
    selectionToolCurrentlyPressed = false;
    selectionLineSeriesPressed = nullptr;

    viewRange = 500;
    viewMinimum = 0;

    rightButtonDown = false;




    //Each output of the neuropixels probe routes to either three or two different channels on the probe
    //This will be used to automatically flip individual channels. In most cases, only one channel per outpuyt channel
    //is on (although multiples are technically allowed).
    for (int i=0;i<384;i++) {
        QList<int> singleChannelRouting;
        singleChannelRouting.push_back(i);
        singleChannelRouting.push_back(i+384);
        if (i < 192) {
            singleChannelRouting.push_back(i+768);
        }
        channelRoutingRules.push_back(singleChannelRouting);
    }

    setupProbeInfo(allSelectionModeArgs);

    chart()->axisX()->setRange(-40.0, 40.0);
    chart()->axisY()->setRange(viewMinimum, viewMinimum+viewRange);   
    chart()->legend()->hide();
    chart()->layout()->setContentsMargins(0, 0, 0, 0);
    //chart()->setTheme(QChart::ChartThemeHighContrast);




}



ProbeLayoutPanel::~ProbeLayoutPanel()
{
}


void ProbeLayoutPanel::setSelectionModeForAllProbes(QList<int> selectionModes)
{
    selectionModeForEachProbe = selectionModes;
    for (int i=0;i<numberOfProbes;i++) {
        setProbeShown(i);
    }

    setProbeShown(0);

    mode = (selectionMode)selectionModeForEachProbe[currentProbeShown];
    //updateChannels();
    emit selectionModeChanged(selectionModeForEachProbe[currentProbeShown]);

}

void ProbeLayoutPanel::copyProbeSettings(int sourceIndex, int destIndex)
{
    //channelsTurnedOn[destIndex] = channelsTurnedOn[sourceIndex];
    //channelColors[destIndex] = channelColors[sourceIndex];

    if (allSelectionModeData[destIndex].length() == allSelectionModeData[sourceIndex].length()) {
        for (int i = 0; i < allSelectionModeData[destIndex].length();i++) {

            if (allSelectionModeData[destIndex][i].arg.length() == allSelectionModeData[sourceIndex][i].arg.length()) {
                allSelectionModeData[destIndex][i].arg = allSelectionModeData[sourceIndex][i].arg;
                allSelectionModeData[destIndex][i].toolName = allSelectionModeData[sourceIndex][i].toolName;
                allSelectionModeData[destIndex][i].controlColor = allSelectionModeData[sourceIndex][i].controlColor;

                for (int j = 0; j < allSelectionModeData[destIndex][i].controlGraphics.length(); j++) {
                    allSelectionModeData[destIndex][i].controlGraphics[j]->replace(allSelectionModeData[sourceIndex][i].controlGraphics[j]->points());
                    allSelectionModeData[destIndex][i].controlGraphics[j]->setColor(allSelectionModeData[sourceIndex][i].controlGraphics[j]->color());
                }
            }
        }
        channelsTurnedOn[destIndex] = channelsTurnedOn[sourceIndex];
        selectionModeForEachProbe[destIndex] = selectionModeForEachProbe[sourceIndex];
    }


    int cProbe = currentProbeShown;
    setProbeShown(destIndex);
    //emit selectionModeChanged(selectionModeForEachProbe[destIndex]);
    updateChannels();
    setProbeShown(cProbe);


}


void ProbeLayoutPanel::setupProbeInfo(const QList<QList<selectionModeData> > &allSelectionModeArgs)
{

    qreal siteXSize = 12.0;
    qreal siteYSize = 12.0;

    QColor defaultChannelColor = QColor(200,50,0);

    //QList<QAreaSeries*> channel_boxes;
    //QList<QScatterSeries*> on_boxes;
    //Placement of the probe sites (Neuropixels 1.0)
    int numRows = 480;
    int numColumns = 2;
    int currentChannelIndex = 0;
    QList<QColor> startChannelColors;
    for (int r=0;r<numRows;r++) {
        for (int c=0;c<numColumns;c++) {
            qreal xOffset;

            if (r%2 > 0) {
                xOffset = 32.0-8.0;
            } else {
                xOffset = 16.0-8.0;
            }
            qreal x = (c*-32.0) + xOffset;
            qreal yOffset = 100.0;
            qreal y = (r*20.0) + yOffset; //20 um vertical spacing
            channelLocations.push_back(QPointF(x,y));
            startChannelColors.push_back(defaultChannelColor);

            QLineSeries* tmpUpper = new QLineSeries();
            *tmpUpper << QPointF(x-(siteXSize/2), y+(siteYSize/2)) << QPointF(x+(siteXSize/2), y+(siteYSize/2));
            QLineSeries* tmpLower = new QLineSeries();
            *tmpLower << QPointF(x-(siteXSize/2), y-(siteYSize/2)) << QPointF(x+(siteXSize/2), y-(siteYSize/2));

            QAreaSeries* tmpBox = new QAreaSeries(tmpUpper,tmpLower);
            tmpBox->setName(QString("%1").arg(currentChannelIndex));
            QPen pen;
            pen.setColor(startChannelColors.last());
            pen.setWidth(1);
            tmpBox->setPen(pen);


            tmpBox->setBrush(startChannelColors.last());
            channel_boxes.push_back(tmpBox);
            chart()->addSeries(tmpBox);
            connect(tmpBox, &QAreaSeries::clicked, this, &ProbeLayoutPanel::channelBoxClicked);



            currentChannelIndex++;
        }
    }

    for (int pnum=0; pnum<numberOfProbes; pnum++) {
        //Scatter series for channels that are turned on...

        //QVector<int> tmpVector;
        //tmpVector.clear();
        //off_scatter_index_to_on_scatter.push_back(tmpVector);

        QVector<bool> tmpOnVector;
        tmpOnVector.fill(true,numRows*numColumns);
        channelsTurnedOn.push_back(tmpOnVector);

        channelColors.push_back(startChannelColors); //each channel has a color

        QList<selectionModeData> tmpSelectionModeData;

        //Create the channel selection tools
        QLineSeries* tmpLineSeries;

        //**Single continuous selection mode data**
        //******************************************
        selectionModeData smd1;
        selectionModeData *smdPtr = &smd1;
        smdPtr->toolName = QString("single_continuous_selection");

        bool toolEntryFound = false;
        int numToolCopies = 0;
        for (int toolIndex = 0; toolIndex < allSelectionModeArgs.at(pnum).length(); toolIndex++) {
            if (allSelectionModeArgs.at(pnum).at(toolIndex).toolName.startsWith(smdPtr->toolName)){
                //This is the right tool
                toolEntryFound = true;

                if (!allSelectionModeArgs.at(pnum).at(toolIndex).arg.isEmpty()) {
                    smdPtr->arg += allSelectionModeArgs.at(pnum).at(toolIndex).arg;
                } else {
                    smdPtr->arg.push_back(10); //minumum value of range
                    smdPtr->arg.push_back(3840); //the length of the selection range
                }

                if ((allSelectionModeArgs.at(pnum).at(toolIndex).controlColor.length() >= (numToolCopies+1))) {
                    smdPtr->controlColor.push_back(allSelectionModeArgs.at(pnum).at(toolIndex).controlColor.at(numToolCopies));

                } else {
                    smdPtr->controlColor.push_back(defaultChannelColor);
                }

                tmpLineSeries = new QLineSeries();
                *tmpLineSeries << QPointF(-35.0, smdPtr->arg.at(0)) << QPointF(-38.0, smdPtr->arg.at(0)) << QPointF(-38.0, smdPtr->arg.at(1)+smdPtr->arg.at(0)) << QPointF(-35.0, smdPtr->arg.at(1)+smdPtr->arg.at(0));
                tmpLineSeries->setProperty("ToolType",QString("%1%2").arg(smdPtr->toolName).arg(numToolCopies+1));
                tmpLineSeries->setColor(smdPtr->controlColor.at(numToolCopies));
                QPen pen;

                pen.setColor(smdPtr->controlColor.at(numToolCopies));
                pen.setWidth(3);
                pen.setStyle(Qt::DashLine);
                tmpLineSeries->setPen(pen);
                chart()->addSeries(tmpLineSeries);
                chart()->legend()->markers(tmpLineSeries)[0]->setVisible(false);
                connect(tmpLineSeries, &QLineSeries::pressed, this, &ProbeLayoutPanel::selectionToolPressed);
                connect(tmpLineSeries, &QLineSeries::released, this, &ProbeLayoutPanel::selectionToolReleased);
                smdPtr->controlGraphics.push_back(tmpLineSeries);

                numToolCopies++;
            }
        }

        if (!toolEntryFound) {
            smdPtr->arg.push_back(10); //minumum value of range1
            smdPtr->arg.push_back(3840); //the length of selection range 1
            smdPtr->controlColor.push_back(defaultChannelColor);
            tmpLineSeries = new QLineSeries();
            *tmpLineSeries << QPointF(-35.0, smdPtr->arg.at(0)) << QPointF(-38.0, smdPtr->arg.at(0)) << QPointF(-38.0, smdPtr->arg.at(1)+smdPtr->arg.at(0)) << QPointF(-35.0, smdPtr->arg.at(1)+smdPtr->arg.at(0));
            tmpLineSeries->setProperty("ToolType",QString("%1%2").arg(smdPtr->toolName).arg(QString("1")));
            tmpLineSeries->setColor(defaultChannelColor);
            QPen pen;
            pen.setColor(defaultChannelColor);
            pen.setWidth(3);
            pen.setStyle(Qt::DashLine);
            tmpLineSeries->setPen(pen);
            chart()->addSeries(tmpLineSeries);
            chart()->legend()->markers(tmpLineSeries)[0]->setVisible(false);
            connect(tmpLineSeries, &QLineSeries::pressed, this, &ProbeLayoutPanel::selectionToolPressed);
            connect(tmpLineSeries, &QLineSeries::released, this, &ProbeLayoutPanel::selectionToolReleased);
            smdPtr->controlGraphics.push_back(tmpLineSeries);
        }
        tmpSelectionModeData.push_back(smd1);
        //*********************************************

        //**Long single continuous selection modes data (half-density pad selection)**
        //*************************************************
        selectionModeData smd2;
        smd2.toolName = QString("long_single_continuous_selection");
        smdPtr = &smd2;

        toolEntryFound = false;
        numToolCopies = 0;
        for (int toolIndex = 0; toolIndex < allSelectionModeArgs.at(pnum).length(); toolIndex++) {
            if (allSelectionModeArgs.at(pnum).at(toolIndex).toolName.startsWith(smdPtr->toolName)){


                //This is the right tool
                toolEntryFound = true;

                if (!allSelectionModeArgs.at(pnum).at(toolIndex).arg.isEmpty()) {
                    smdPtr->arg += allSelectionModeArgs.at(pnum).at(toolIndex).arg;
                } else {
                    smdPtr->arg.push_back(10); //minumum value of range
                    smdPtr->arg.push_back(7680); //the length of the selection range
                }



                if (!(allSelectionModeArgs.at(pnum).at(toolIndex).controlColor.length() > numToolCopies)) {
                    smdPtr->controlColor.push_back(allSelectionModeArgs.at(pnum).at(toolIndex).controlColor.at(numToolCopies));

                } else {
                    smdPtr->controlColor.push_back(defaultChannelColor);
                }

                tmpLineSeries = new QLineSeries();
                *tmpLineSeries << QPointF(-35.0, smdPtr->arg.at(0)) << QPointF(-38.0, smdPtr->arg.at(0)) << QPointF(-38.0, smdPtr->arg.at(1)+smdPtr->arg.at(0)) << QPointF(-35.0, smdPtr->arg.at(1)+smdPtr->arg.at(0));
                tmpLineSeries->setProperty("ToolType",QString("%1%2").arg(smdPtr->toolName).arg(numToolCopies+1));
                tmpLineSeries->setColor(smdPtr->controlColor.at(numToolCopies));
                QPen pen;
                pen.setColor(smdPtr->controlColor.at(numToolCopies));
                pen.setWidth(3);
                pen.setStyle(Qt::DashLine);
                tmpLineSeries->setPen(pen);
                chart()->addSeries(tmpLineSeries);
                chart()->legend()->markers(tmpLineSeries)[0]->setVisible(false);
                connect(tmpLineSeries, &QLineSeries::pressed, this, &ProbeLayoutPanel::selectionToolPressed);
                connect(tmpLineSeries, &QLineSeries::released, this, &ProbeLayoutPanel::selectionToolReleased);
                smdPtr->controlGraphics.push_back(tmpLineSeries);

                numToolCopies++;
            }
        }

        if (!toolEntryFound) {
            //default entry
            smd2.arg.push_back(10); //minumum value of range1
            smd2.arg.push_back(7680); //the length of selection range 1
            smd2.controlColor.push_back(defaultChannelColor);
            tmpLineSeries = new QLineSeries();
            *tmpLineSeries << QPointF(-35.0, smd2.arg.at(0)) << QPointF(-38.0, smd2.arg.at(0)) << QPointF(-38.0, smd2.arg.at(1)+smd2.arg.at(0)) << QPointF(-35.0, smd2.arg.at(1)+smd2.arg.at(0));
            tmpLineSeries->setProperty("ToolType",QString("%1%2").arg(smd2.toolName).arg(QString("1")));
            tmpLineSeries->setColor(defaultChannelColor);
            QPen pen;
            pen.setWidth(3);
            pen.setStyle(Qt::DashLine);
            tmpLineSeries->setPen(pen);
            chart()->addSeries(tmpLineSeries);
            chart()->legend()->markers(tmpLineSeries)[0]->setVisible(false);
            connect(tmpLineSeries, &QLineSeries::pressed, this, &ProbeLayoutPanel::selectionToolPressed);
            connect(tmpLineSeries, &QLineSeries::released, this, &ProbeLayoutPanel::selectionToolReleased);
            smd2.controlGraphics.push_back(tmpLineSeries);
        }
        tmpSelectionModeData.push_back(smd2);
        //**************************************************

        //**Multiple continuous selection modes data (full-density pad selection)**
        //*************************************************
        selectionModeData smd3;
        int defaultMultiBandWidth = 480;  //480 is 1/8, 960 is quarter-length, 1920 half
        int defaultNumBands = 8;

        smd3.toolName = QString("multi_high_density_continuous_selections");
        smd3.arg.push_back(0); //will be the number of bands

        smdPtr = &smd3;

        toolEntryFound = false;
        numToolCopies = 0;
        for (int toolIndex = 0; toolIndex < allSelectionModeArgs.at(pnum).length(); toolIndex++) {
            if (allSelectionModeArgs.at(pnum).at(toolIndex).toolName.startsWith(smdPtr->toolName)){
                //This is the right tool

                toolEntryFound = true;

                if (!allSelectionModeArgs.at(pnum).at(toolIndex).arg.isEmpty()) {
                    smdPtr->arg += allSelectionModeArgs.at(pnum).at(toolIndex).arg;
                } else {
                    smdPtr->arg.push_back(10000); //minumum value of range
                    smdPtr->arg.push_back(defaultMultiBandWidth); //the length of the selection range
                }


                if (!(allSelectionModeArgs.at(pnum).at(toolIndex).controlColor.length() > numToolCopies)) {
                    smdPtr->controlColor += allSelectionModeArgs.at(pnum).at(toolIndex).controlColor;

                } else {
                    smdPtr->controlColor.push_back(defaultChannelColor);
                }


                tmpLineSeries = new QLineSeries();
                //*tmpLineSeries << QPointF(-37.0+lIndex, smdPtr->arg.at((2*lIndex)+1)) << QPointF(-40.0+lIndex, smdPtr->arg.at((2*lIndex)+1)) << QPointF(-40.0+lIndex, smdPtr->arg.at((2*lIndex)+2)+smdPtr->arg.at((2*lIndex)+1)) << QPointF(-37.0+lIndex, smdPtr->arg.at((2*lIndex)+2)+smdPtr->arg.at((2*lIndex)+1));
                *tmpLineSeries << QPointF(-37.0+numToolCopies, smdPtr->arg.at((2*numToolCopies)+1)) << QPointF(-40.0+numToolCopies, smdPtr->arg.at((2*numToolCopies)+1)) << QPointF(-40.0+numToolCopies, smdPtr->arg.at((2*numToolCopies)+2)+smdPtr->arg.at((2*numToolCopies)+1)) << QPointF(-37.0+numToolCopies, smdPtr->arg.at((2*numToolCopies)+2)+smdPtr->arg.at((2*numToolCopies)+1));

                tmpLineSeries->setProperty("ToolType",QString("%1%2").arg(smdPtr->toolName).arg(numToolCopies+1));
                tmpLineSeries->setColor(smdPtr->controlColor.at(numToolCopies));
                QPen pen;
                pen.setColor(smdPtr->controlColor.at(numToolCopies));
                pen.setWidth(3);
                pen.setStyle(Qt::DashLine);
                tmpLineSeries->setPen(pen);
                chart()->addSeries(tmpLineSeries);
                chart()->legend()->markers(tmpLineSeries)[0]->setVisible(false);
                connect(tmpLineSeries, &QLineSeries::pressed, this, &ProbeLayoutPanel::selectionToolPressed);
                connect(tmpLineSeries, &QLineSeries::released, this, &ProbeLayoutPanel::selectionToolReleased);
                smdPtr->controlGraphics.push_back(tmpLineSeries);


                numToolCopies++;



            }
        }


        if (numToolCopies > 0) {
            smd3.arg[0] = numToolCopies;
        } else {
            //default entry
            smd3.arg[0] = defaultNumBands;
            QPen pen;
            pen.setWidth(3);
            pen.setStyle(Qt::DashLine);

            for (int i=0;i<defaultNumBands;i++) {
               //smd3.arg.push_back((i*(multiBandWidth+1))); //minumum value of range1
                smd3.arg.push_back(10000); //minumum value of range1
                smd3.arg.push_back(defaultMultiBandWidth); //the length of selection range 1
                smd3.controlColor.push_back(defaultChannelColor);
                tmpLineSeries = new QLineSeries();
                *tmpLineSeries << QPointF(-37.0+i, smd3.arg.at((2*i)+1)) << QPointF(-40.0+i, smd3.arg.at((2*i)+1)) << QPointF(-40.0+i, smd3.arg.at((2*i)+2)+smd3.arg.at((2*i)+1)) << QPointF(-37.0+i, smd3.arg.at((2*i)+2)+smd3.arg.at((2*i)+1));
                tmpLineSeries->setProperty("ToolType",QString("%1%2").arg(smd3.toolName).arg(i+1));
                tmpLineSeries->setColor(defaultChannelColor);
                tmpLineSeries->setPen(pen);
                chart()->addSeries(tmpLineSeries);
                chart()->legend()->markers(tmpLineSeries)[0]->setVisible(false);
                connect(tmpLineSeries, &QLineSeries::pressed, this, &ProbeLayoutPanel::selectionToolPressed);
                connect(tmpLineSeries, &QLineSeries::released, this, &ProbeLayoutPanel::selectionToolReleased);
                smd3.controlGraphics.push_back(tmpLineSeries);
            }
        }



        tmpSelectionModeData.push_back(smd3);
        //**************************************************

        //**Individual channel toggeling**
        //*********************************************
        selectionModeData smd4;
        smd4.toolName = QString("individual_channel_toggling");
        smdPtr = &smd4;
        toolEntryFound = false;
        numToolCopies = 0;
        for (int toolIndex = 0; toolIndex < allSelectionModeArgs.at(pnum).length(); toolIndex++) {
            if (allSelectionModeArgs.at(pnum).at(toolIndex).toolName.startsWith(smdPtr->toolName)){
                //This is the right tool

                if (allSelectionModeArgs.at(pnum).at(toolIndex).arg.length() == channelsTurnedOn.last().length()) {
                    smdPtr->arg += allSelectionModeArgs.at(pnum).at(toolIndex).arg;
                    toolEntryFound = true;
                }
                break;
            }
        }
        if (!toolEntryFound) {
            for (int i = 0; i < channelsTurnedOn.last().length(); i++) {
                smd4.arg.push_back(-1.0); //-1 is neutral, 0 is off, 1 is on

            }
        }
        tmpSelectionModeData.push_back(smd4);
        //**********************************************

        allSelectionModeData.push_back(tmpSelectionModeData);
        selectionModeForEachProbe.push_back(0);
    }



    chart()->createDefaultAxes();
    chart()->axisX()->setTitleText("Horizontal Distance (uM)");
    chart()->axisY()->setTitleText("Vertical Distance (uM)");
    /*QValueAxis *axisY = new QValueAxis;
    //axisX->setRange(10, 20.5);
    axisY->setTickCount(10);
    axisY->setLabelFormat("%.2f");
    axisY->setTitleText("Vertical Distance (uM)");
    chart()->setAxisY(axisY);*/

    //numberOfProbes = num;



    setProbeShown(0);

}

void ProbeLayoutPanel::setChannelHighlight(int chInd)
{

    QPen pen;
    pen.setWidth(1);

    for (int i=0;i<channelLocations.length();i++) {


        if (channelsTurnedOn[currentProbeShown][i]) {
            pen.setColor(channelColors.at(currentProbeShown).at(i).darker(-50));
            channel_boxes.at(i)->setPen(pen);
            channel_boxes.at(i)->setBrush(channelColors.at(currentProbeShown).at(i));

        } else {
            channel_boxes.at(i)->setBrush(Qt::gray);
            pen.setColor(QColor(100,100,100));
            channel_boxes.at(i)->setPen(pen);
        }
    }



    pen.setColor(channelColors.at(currentProbeShown).at(chInd).darker(50));
    channel_boxes[chInd]->setPen(pen);
    channel_boxes[chInd]->setBrush(channelColors.at(currentProbeShown).at(chInd).darker(50));
}

void ProbeLayoutPanel::setProbeShown(int pInd)
{


    currentProbeShown = pInd;
    int nextMode = selectionModeForEachProbe.at(currentProbeShown);



    for (int i = 0; i<numberOfProbes; i++) {//each probe

        //This probe is now the probe to show
        bool show = (i==pInd);
        for (int j=0; j< allSelectionModeData.at(i).length(); j++) { //each selection tool
            for (int tg=0; tg<allSelectionModeData.at(i).at(j).controlGraphics.length();tg++) { //each graphics item
                allSelectionModeData.at(i).at(j).controlGraphics.at(tg)->setVisible(show);
            }
        }

        //on_scatter.at(i)->setVisible(show);
        //off_scatter.at(i)->setVisible(show);
    }

    //int selectmode = (int)mode;
    setSelectionMode(nextMode);


}

void ProbeLayoutPanel::selectionToolPressed(const QPointF &point)
{

    QLineSeries* obj = (QLineSeries*)sender();
    QString selectionTool = obj->property("ToolType").toString();
    selectionLineSeriesPressed = obj;

    if (rightButtonDown) {
        //The right button was pressed, so we open a context menu
        QMenu *menu = new QMenu;
        menu->addAction(tr("Set color"), this, SLOT(openColorSelector()));
        if (selectionTool.contains(QString("multi_high_density_continuous_selections")) ) {
            //We allow the user to edit the length of the ranges for the multi range tool
            menu->addAction(tr("Set length"), this, SLOT(openRangeLengthEditor()));
        }

        QPoint p = chart()->mapToPosition(point).toPoint();
        menu->exec(this->mapToGlobal(p));

        rightButtonDown = false;


    } else {


        //qDebug() << toolType;
        selectionToolCurrentlyPressed = true;
        selectionLineSeriesPressed = obj;
        selectionLineSeriesPressed->setColor(QColor(150,150,150));
        clickToFirstPoint = point;
        lineSeriesPointsAtPress.clear();
        for (int i=0;i<selectionLineSeriesPressed->points().length();i++) {
            lineSeriesPointsAtPress.push_back(selectionLineSeriesPressed->points().at(i));
        }

        qreal topOfRange = 0.0;
        qreal bottomOfRange = 0.0;
        int bottomChannel = 0;
        int topChannel = 0;
        for (int i=0; i<selectionLineSeriesPressed->points().length();i++) {

            if (i==0) {
                bottomOfRange = lineSeriesPointsAtPress.at(i).y();
                for (int ch=0;ch<channelLocations.count();ch++) {
                    const QPointF currentPoint = channelLocations.at(ch);
                    if (currentPoint.y() >= bottomOfRange) {
                        bottomChannel = ch+1+(1000*(currentProbeShown+1));
                        break;
                    }
                }
            }
            if (i==(selectionLineSeriesPressed->points().length()-1)) {
                topOfRange = lineSeriesPointsAtPress.at(i).y();
                for (int ch=0;ch<channelLocations.count();ch++) {
                    const QPointF currentPoint = channelLocations.at(ch);
                    if (currentPoint.y() > topOfRange) {
                        topChannel = ch+(1000*(currentProbeShown+1));
                        break;
                    }
                }
            }
        }

        emit newStatus(QString("Range: %1 - %2uM    Channels: %3 - %4").arg((int)bottomOfRange).arg((int)topOfRange).arg(bottomChannel).arg(topChannel));

    }
}

void ProbeLayoutPanel::selectionToolReleased(const QPointF &point)
{
    selectionToolCurrentlyPressed = false;
    //selectionLineSeriesPressed->setColor(QColor(100,100,100));


    QString selectionTool = selectionLineSeriesPressed->property("ToolType").toString();

    if (selectionTool == QString("single_continuous_selection1")) {
        //qDebug() << "New single continuous selection. Range:" << selectionLineSeriesPressed->points().at(0).y() << "to" << selectionLineSeriesPressed->points().at(selectionLineSeriesPressed->points().length()-1).y();

        int toolIndex = (int)selectionMode::singleContinuousGroup;
        selectionLineSeriesPressed->setColor(allSelectionModeData[currentProbeShown][toolIndex].controlColor.at(0));
        qreal minRange = selectionLineSeriesPressed->points().at(0).y();
        allSelectionModeData[currentProbeShown][toolIndex].arg[0] = minRange; //The minimum of the range
        allSelectionModeData[currentProbeShown][toolIndex].arg[1] = selectionLineSeriesPressed->points().at(selectionLineSeriesPressed->points().length()-1).y()-minRange; //the length of the range
        turnOnSingleContinuousRange(selectionLineSeriesPressed->points().at(0).y(),selectionLineSeriesPressed->points().at(selectionLineSeriesPressed->points().length()-1).y(),true);

    } else if (selectionTool == QString("long_single_continuous_selection1")) {

        int toolIndex = (int)selectionMode::longSingleContinuousGroup;
        selectionLineSeriesPressed->setColor(allSelectionModeData[currentProbeShown][toolIndex].controlColor.at(0));
        qreal minRange = selectionLineSeriesPressed->points().at(0).y();
        allSelectionModeData[currentProbeShown][toolIndex].arg[0] = minRange; //The minimum of the range
        allSelectionModeData[currentProbeShown][toolIndex].arg[1] = selectionLineSeriesPressed->points().at(selectionLineSeriesPressed->points().length()-1).y()-minRange; //the length of the range
        turnOnSingleContinuousRange(selectionLineSeriesPressed->points().at(0).y(),selectionLineSeriesPressed->points().at(selectionLineSeriesPressed->points().length()-1).y(),false);

    } else if (selectionTool.contains(QString("multi_high_density_continuous_selections")) ) {

        int rangeNum = QString(selectionTool.at(40)).toInt()-1;

        //*tmpLineSeries << QPointF(-35.0+i, smd3.arg.at((2*i)+1)) << QPointF(-38.0+i, smd3.arg.at((2*i)+1)) << QPointF(-38.0+i, smd3.arg.at((2*i)+2)+smd3.arg.at((2*i)+1)) << QPointF(-35.0+i, smd3.arg.at((2*i)+2)+smd3.arg.at((2*i)+1));

        int toolIndex = (int)selectionMode::multiHighDensityGroups;


        qreal minRange = selectionLineSeriesPressed->points().at(0).y();
        allSelectionModeData[currentProbeShown][toolIndex].arg[(rangeNum*2)+1] = minRange; //The minimum of the range
        allSelectionModeData[currentProbeShown][toolIndex].arg[(rangeNum*2)+2] = selectionLineSeriesPressed->points().at(selectionLineSeriesPressed->points().length()-1).y()-minRange; //the length of the range
        selectionLineSeriesPressed->setColor(allSelectionModeData[currentProbeShown][toolIndex].controlColor.at(rangeNum));

        //qreal maxRange1 = allSelectionModeData[toolIndex].arg[2]-allSelectionModeData[toolIndex].arg[1];
        //qreal minRange2 = allSelectionModeData[toolIndex].arg[3];
        //;qreal maxRange2 = allSelectionModeData[toolIndex].arg[4]-allSelectionModeData[toolIndex].arg[3];

        turnOnMultiHighDensityRanges();

    } else if (selectionTool == QString("multi_high_density_continuous_selections2")) {

        /*int toolIndex = (int)selectionMode::twoHighDensityGroups;
        qreal minRange2 = selectionLineSeriesPressed->points().at(0).y();
        allSelectionModeData[toolIndex].arg[3] = minRange2; //The minimum of the range
        allSelectionModeData[toolIndex].arg[4] = selectionLineSeriesPressed->points().at(selectionLineSeriesPressed->points().length()-1).y()+minRange2; //the length of the range

        qreal maxRange1 = allSelectionModeData[toolIndex].arg[2]-allSelectionModeData[toolIndex].arg[1];
        qreal minRange1 = allSelectionModeData[toolIndex].arg[1];
        qreal maxRange2 = allSelectionModeData[toolIndex].arg[4]-allSelectionModeData[toolIndex].arg[3];

        turnOnTwoHighDensityRanges(minRange1,maxRange1,minRange2,maxRange2);*/
    }
    selectionLineSeriesPressed = nullptr;
}

void ProbeLayoutPanel::turnOnSingleContinuousRange(qreal lowerlimit, qreal upperlimit, bool shortLengthMode)
{

    //Figure out the color of the selection tool
    QColor toolColor;
    int m;
    if (shortLengthMode) {
        m = (int)selectionMode::singleContinuousGroup;
    } else {
        m = (int)selectionMode::longSingleContinuousGroup;
    }
    toolColor = allSelectionModeData[currentProbeShown].at(m).controlColor.at(0);
    int singleSelectionTool = (int)selectionMode::singleChannelToggling;

    int channelInRangeCount = 0; //we keep track of the number of channels inside the range
    for (int i=0;i<channel_boxes.count();i++) {
        //qDebug() << i;
        //const QPointF currentPoint = on_scatter[currentProbeShown]->points().at(i);
        const QPointF currentPoint = channelLocations.at(i);


        int channelState = (int)allSelectionModeData[currentProbeShown][singleSelectionTool].arg[i];

        if ( shortLengthMode && (currentPoint.y() >= lowerlimit)&&(currentPoint.y() <= upperlimit) &&(channelState != 0) ) {
            //turn on all channels within the range

            if (channelsTurnedOn[currentProbeShown].at(i)==false) {
                channelsTurnedOn[currentProbeShown][i] = true;

            }


        //} else if ( (!shortLengthMode && (currentPoint.y() >= lowerlimit)&&(currentPoint.y() <= upperlimit)) &&
        //            (((i%4<2)&&channelInRangeCount < 384)||((i%4>=2)&&channelInRangeCount >= 384)) &&(channelState != 0) )  {
        } else if ( (!shortLengthMode && (currentPoint.y() >= lowerlimit)&&(currentPoint.y() <= upperlimit)) &&
                    (((i%2==0)&&channelInRangeCount < 384)||((i%2==1)&&channelInRangeCount >= 384)) &&(channelState != 0) )  {
            //For a longer selection window, we turn on even rows for the first half and odd rows for the second half
            if (channelsTurnedOn[currentProbeShown].at(i)==false) {
                channelsTurnedOn[currentProbeShown][i] = true;
                //deleteFlag.append(i);

            }

        } else {

            //turn off all channels outside of the range (or every other channel within the range if it is a long range)
            if (channelsTurnedOn[currentProbeShown].at(i)==true) {
                channelsTurnedOn[currentProbeShown][i] = false;
                //off_scatter[currentProbeShown]->append(currentPoint.x(),currentPoint.y());
                //off_scatter_index_to_on_scatter[currentProbeShown].append(i);

            }
        }

        if ((currentPoint.y() >= lowerlimit)&&(currentPoint.y() <= upperlimit) ) {
            channelInRangeCount++;
            channelColors[currentProbeShown][i] = toolColor; //all channels within the range get assigned the tool's color
        }
    }

    //qDebug() << "Total channels considered:" << channelInRangeCount;
}

void ProbeLayoutPanel::turnOnMultiHighDensityRanges()
{
    //qDebug() << lowerlimit1 << upperlimit1 << lowerlimit2 << upperlimit2;
    int m = (int)selectionMode::multiHighDensityGroups;
    int singleSelectionTool = (int)selectionMode::singleChannelToggling;
    int numZones = (int)allSelectionModeData[currentProbeShown].at(m).arg[0];

    for (int zoneIndex = 0; zoneIndex < numZones; zoneIndex++) {
        QColor toolColor;
        toolColor = allSelectionModeData[currentProbeShown].at(m).controlColor.at(zoneIndex);
        for (int i=0;i<channelLocations.count();i++) {
            //qDebug() << i;
            qreal lowerlimit = allSelectionModeData[currentProbeShown][m].arg[(2*zoneIndex)+1];
            qreal upperlimit = allSelectionModeData[currentProbeShown][m].arg[(2*zoneIndex)+2]+lowerlimit;
            const QPointF currentPoint = channelLocations.at(i);
            int channelState = (int)allSelectionModeData[currentProbeShown][singleSelectionTool].arg[i];
            if ((currentPoint.y() >= lowerlimit)&&(currentPoint.y() <= upperlimit)&&(channelState != 0) ) {
                channelColors[currentProbeShown][i] = toolColor; //all channels within the range get assigned the tool's color

                //turn on all channels within the range
                if (channelsTurnedOn[currentProbeShown].at(i)==false) {
                    if (zoneIndex == 0) {
                        channelsTurnedOn[currentProbeShown][i] = true;
                        //deleteFlag.append(i);
                    } else {
                        toggleSinglePad(i, true); //This will turn off the competing sites in the first range
                    }
                }
            } else if (zoneIndex==0){

                //turn off all channels outside of the range if this is the first range

                if (channelsTurnedOn[currentProbeShown].at(i)==true) {
                    channelsTurnedOn[currentProbeShown][i] = false;
                    //off_scatter[currentProbeShown]->append(currentPoint.x(),currentPoint.y());
                    //off_scatter_index_to_on_scatter[currentProbeShown].append(i);

                }
            }

        }
        //updateChannels();
    }


}

void ProbeLayoutPanel::openRangeLengthEditor()
{
    bool ok;
    QString selectionTool = selectionLineSeriesPressed->property("ToolType").toString();
    double currentValue;
    if (selectionTool == QString("single_continuous_selection1")) {

        int toolIndex = (int)selectionMode::singleContinuousGroup;
        currentValue = allSelectionModeData[currentProbeShown][toolIndex].arg.at(1);
      } else if (selectionTool == QString("long_single_continuous_selection1")) {

        int toolIndex = (int)selectionMode::longSingleContinuousGroup;
        currentValue = allSelectionModeData[currentProbeShown][toolIndex].arg.at(1);


    } else if (selectionTool.contains(QString("multi_high_density_continuous_selections")) ) {

        int rangeNum = QString(selectionTool.at(40)).toInt()-1;
        int toolIndex = (int)selectionMode::multiHighDensityGroups;
        //data stored is <number of ranges><bottom R0><length R0><bottom R1><Length R1>...
        currentValue = allSelectionModeData[currentProbeShown][toolIndex].arg.at((rangeNum*2)+2);


    }



    double newValue = QInputDialog::getDouble(this,"Range editor", "Select vertical range in uM",currentValue, 10.0, 10000.0, 0.1, &ok);
    if (!ok) return; //Cancel button pressed
    if (selectionTool == QString("single_continuous_selection1")) {

        int toolIndex = (int)selectionMode::singleContinuousGroup;
        allSelectionModeData[currentProbeShown][toolIndex].arg[1] = newValue;
      } else if (selectionTool == QString("long_single_continuous_selection1")) {

        int toolIndex = (int)selectionMode::longSingleContinuousGroup;
        allSelectionModeData[currentProbeShown][toolIndex].arg[1] = newValue;


    } else if (selectionTool.contains(QString("multi_high_density_continuous_selections")) ) {


        int rangeNum = QString(selectionTool.at(40)).toInt()-1;
        int toolIndex = (int)selectionMode::multiHighDensityGroups;

        //data stored is <number of ranges><bottom R0><length R0><bottom R1><Length R1>...
        allSelectionModeData[currentProbeShown][toolIndex].arg[(rangeNum*2)+2] = newValue;

    }

    //Change length of the line series
    QList<QPointF> pointLoc = selectionLineSeriesPressed->points();
    qreal newHeight = pointLoc.at(0).y()+newValue;
    pointLoc[2].setY(newHeight);
    pointLoc[3].setY(newHeight);
    selectionLineSeriesPressed->replace(pointLoc);


    selectionToolReleased(QPoint(0,0));
    updateChannels();


}

void ProbeLayoutPanel::openColorSelector()
{
    QColor c = QColorDialog::getColor(selectionLineSeriesPressed->color(),this); //open a color selection dialog
    if (!c.isValid()) return; //The user clicked cancel

    selectionLineSeriesPressed->setColor(c);
    QString selectionTool = selectionLineSeriesPressed->property("ToolType").toString();

    if (selectionTool == QString("single_continuous_selection1")) {
        //qDebug() << "New single continuous selection. Range:" << selectionLineSeriesPressed->points().at(0).y() << "to" << selectionLineSeriesPressed->points().at(selectionLineSeriesPressed->points().length()-1).y();

        int toolIndex = (int)selectionMode::singleContinuousGroup;
        allSelectionModeData[currentProbeShown][toolIndex].controlColor[0] = c;
      } else if (selectionTool == QString("long_single_continuous_selection1")) {

        int toolIndex = (int)selectionMode::longSingleContinuousGroup;
        allSelectionModeData[currentProbeShown][toolIndex].controlColor[0] = c;


    } else if (selectionTool.contains(QString("multi_high_density_continuous_selections")) ) {

        int rangeNum = QString(selectionTool.at(40)).toInt()-1;
        int toolIndex = (int)selectionMode::multiHighDensityGroups;
        allSelectionModeData[currentProbeShown][toolIndex].controlColor[rangeNum] = c;

    }


    selectionToolReleased(QPoint(0,0));
    updateChannels();

}

void ProbeLayoutPanel::toggleSinglePad(int padIndex, bool state)
{
    //This function is used to toggle a single pas on or off. It also handles conflicts by turning off
    //conflicting channels routing to the same output channel.

    if (state) {
        //Turn on the channel

        if (channelsTurnedOn[currentProbeShown].at(padIndex)==false) {
            channelsTurnedOn[currentProbeShown][padIndex] = true;
            //deleteFlag.append(padIndex);
        }
        //Make sure the other channels that are routed to the same output are turned off

        int outputChannel = findOutputChannelForPad(padIndex);

        for (int i=0;i<channelRoutingRules[outputChannel].length();i++) {

            if ((channelRoutingRules[outputChannel].at(i) != padIndex) && channelsTurnedOn[currentProbeShown][channelRoutingRules[outputChannel].at(i)]) {

                //A conflicting channel needs to be turned off
                int conflictingPadIndex = channelRoutingRules[outputChannel].at(i);
                channelsTurnedOn[currentProbeShown][conflictingPadIndex] = false;
                //const QPointF currentPoint = on_scatter[currentProbeShown]->points().at(conflictingPadIndex);
                //off_scatter[currentProbeShown]->append(currentPoint.x(),currentPoint.y());
                //off_scatter_index_to_on_scatter[currentProbeShown].append(conflictingPadIndex);

            }
        }

    } else {
        //Turn off the channel

        if (channelsTurnedOn[currentProbeShown].at(padIndex)==true) {
            channelsTurnedOn[currentProbeShown][padIndex] = false;
            //const QPointF currentPoint = on_scatter[currentProbeShown]->points().at(padIndex);
            //off_scatter[currentProbeShown]->append(currentPoint.x(),currentPoint.y());
            //off_scatter_index_to_on_scatter[currentProbeShown].append(padIndex);
        }


    }

}

void ProbeLayoutPanel::updateChannels() {

    int m = (int)mode;
    //make only the correct selection tools visible
    for (int i=0;i<allSelectionModeData[currentProbeShown].length();i++) {
        for (int j=0;j<allSelectionModeData[currentProbeShown].at(i).controlGraphics.length();j++) {
            if (i==m) {
                //These tools should be visible
                allSelectionModeData[currentProbeShown].at(i).controlGraphics.at(j)->setVisible(true);
            } else {
                allSelectionModeData[currentProbeShown].at(i).controlGraphics.at(j)->setVisible(false);
            }
        }
    }


    if (mode == selectionMode::singleContinuousGroup) {
        //qDebug() << "Short" << currentProbeShown;
        qreal minRange = allSelectionModeData[currentProbeShown].at(m).arg[0];
        qreal maxRange = allSelectionModeData[currentProbeShown].at(m).arg[1]+minRange;
        turnOnSingleContinuousRange(minRange, maxRange, true);

    } else if (mode == selectionMode::longSingleContinuousGroup) {
        //qDebug() << "Long"<< currentProbeShown;
        qreal minRange = allSelectionModeData[currentProbeShown].at(m).arg[0];
        qreal maxRange = allSelectionModeData[currentProbeShown].at(m).arg[1]+minRange;
        turnOnSingleContinuousRange(minRange, maxRange, false);
    } else if (mode == selectionMode::multiHighDensityGroups) {
        //qDebug() << "Multi"<< currentProbeShown;

        turnOnMultiHighDensityRanges();
    }


    m = (int)selectionMode::singleChannelToggling;

    for (int channelIndex = 0; channelIndex < channelsTurnedOn.at(currentProbeShown).length(); channelIndex++) {
        int channelState = (int)allSelectionModeData[currentProbeShown][m].arg[channelIndex];

        if (channelState == 0) {

            toggleSinglePad(channelIndex,false);
        }
    }

    QPen pen;
    pen.setWidth(1);

    for (int i=0;i<channelLocations.length();i++) {


        if (channelsTurnedOn[currentProbeShown][i]) {
            pen.setColor(channelColors.at(currentProbeShown).at(i).darker(-50));
            channel_boxes.at(i)->setPen(pen);
            channel_boxes.at(i)->setBrush(channelColors.at(currentProbeShown).at(i));

        } else {
            channel_boxes.at(i)->setBrush(Qt::gray);
            pen.setColor(QColor(100,100,100));
            channel_boxes.at(i)->setPen(pen);
        }
    }

    /*if (!deleteFlag.isEmpty()) {

        for (int i=0;i<deleteFlag.length();i++) {
            for (int j=0;j<off_scatter_index_to_on_scatter[currentProbeShown].length();j++) {
                if (off_scatter_index_to_on_scatter[currentProbeShown].at(j) == deleteFlag.at(i)) {
                    off_scatter[currentProbeShown]->remove(j);
                    off_scatter_index_to_on_scatter[currentProbeShown].removeAt(j);
                    break;
                }
            }

        }
        deleteFlag.clear();
    }*/

    emit newActiveChannelCount(calcActiveChannelCount());
    emit selectionChanged();
}

QList<QList<ProbeLayoutPanel::selectionModeData> > ProbeLayoutPanel::getSelectionData()
{
    return allSelectionModeData;
}

QList<QList<QColor> > ProbeLayoutPanel::getChannelColors()
{
    return channelColors;

}

QList<QPointF> ProbeLayoutPanel::getChannelLocations()
{
    return channelLocations;
}

QList<int> ProbeLayoutPanel::getAllSelectionModes()
{
    return selectionModeForEachProbe;
}

QList<QVector<bool> > ProbeLayoutPanel::getChannelsTurnedOn()
{
    return channelsTurnedOn;
}

int ProbeLayoutPanel::findOutputChannelForPad(int padIndex)
{
    //Given a pad index, this function returns the output channel that pad routes to
    //(an output channel routes to up to three pads)
    int outputChannel = -1;
    for (int i=0;i<channelRoutingRules.length();i++) {
        for (int j=0;j<channelRoutingRules[i].length();j++) {
            if (channelRoutingRules[i].at(j) == padIndex) {
                outputChannel = i;
                return outputChannel;
            }
        }
    }
    return outputChannel;
}

int ProbeLayoutPanel::calcActiveChannelCount()
{

    int count = 0;
    for (int p=0;p<numberOfProbes;p++) {
        for (int i=0; i<channelsTurnedOn[p].length(); i++) {
            if (channelsTurnedOn[p].at(i)) {
                count++;
            }
        }
    }

    return count;
}

void ProbeLayoutPanel::setChannelPlotSize(int s)
{

    for (int i=0;i<numberOfProbes;i++) {
        //on_scatter[i]->setMarkerSize(s);
        //off_scatter[i]->setMarkerSize(s);
    }
}

void ProbeLayoutPanel::setSelectionMode(int m)
{

    selectionMode lastMode = mode;
    mode = (selectionMode)m;
    selectionModeForEachProbe[currentProbeShown] = (int)mode;

    //make only the correct selection tools viusible
   /* for (int i=0;i<allSelectionModeData[currentProbeShown].length();i++) {
        for (int j=0;j<allSelectionModeData[currentProbeShown].at(i).controlGraphics.length();j++) {
            if (i==m) {
                //These tools should be visible
                allSelectionModeData[currentProbeShown].at(i).controlGraphics.at(j)->setVisible(true);
            } else {
                allSelectionModeData[currentProbeShown].at(i).controlGraphics.at(j)->setVisible(false);
            }
        }
    }


    if (mode == selectionMode::singleContinuousGroup) {
        //qDebug() << "Short";
        qreal minRange = allSelectionModeData[currentProbeShown].at(m).arg[0];
        qreal maxRange = allSelectionModeData[currentProbeShown].at(m).arg[1]+minRange;
        turnOnSingleContinuousRange(minRange, maxRange, true);

    } else if (mode == selectionMode::longSingleContinuousGroup) {
        //qDebug() << "Long";
        qreal minRange = allSelectionModeData[currentProbeShown].at(m).arg[0];
        qreal maxRange = allSelectionModeData[currentProbeShown].at(m).arg[1]+minRange;
        turnOnSingleContinuousRange(minRange, maxRange, false);
    } else if (mode == selectionMode::multiHighDensityGroups) {
        //qDebug() << "Multi";

        turnOnMultiHighDensityRanges();
    }
    selectionModeForEachProbe[currentProbeShown] = (int)mode;*/

    if (lastMode != mode) {
        emit selectionModeChanged((int)mode);
    }

    updateChannels(); //this needs to come last, because it triggers the writing of settings to the workspace
}

void ProbeLayoutPanel::calculateChannelStates()
{

}

void ProbeLayoutPanel::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton) {
        rightButtonDown = true;

    }

    QChartView::mousePressEvent(event);
}

void ProbeLayoutPanel::mouseReleaseEvent(QMouseEvent *event)
{

    QChartView::mouseReleaseEvent(event);
    rightButtonDown = false;
    updateChannels();
    emit newStatus("");
}

void ProbeLayoutPanel::mouseMoveEvent(QMouseEvent *event)
{

    if (selectionToolCurrentlyPressed) {
        //The user is dragging a selection tool up and down.

        //qDebug() << clickToFirstPoint << event->x() << event->y() << chart()->mapToValue(QPointF(event->x(),event->y()));
        QPointF newLoc = chart()->mapToValue(QPointF(event->x(),event->y())); //where is the mouse in chart coordinates
        QPointF movedVector = newLoc-clickToFirstPoint; //how far has the mouse moved since initial press

        qreal topOfRange = 0.0;
        qreal bottomOfRange = 0.0;
        int bottomChannel = 0;
        int topChannel = 0;
        for (int i=0; i<selectionLineSeriesPressed->points().length();i++) {
            QPointF newPointLoc = lineSeriesPointsAtPress.at(i)+movedVector;
            newPointLoc.setX(lineSeriesPointsAtPress.at(i).x()); //Change the x coodinate back (it does not move with mouse)
            selectionLineSeriesPressed->replace(i,newPointLoc);

            if (i==0) {
                bottomOfRange = newPointLoc.y();
                for (int ch=0;ch<channelLocations.count();ch++) {
                    const QPointF currentPoint = channelLocations.at(ch);
                    if (currentPoint.y() >= bottomOfRange) {
                        bottomChannel = ch+1+(1000*(currentProbeShown+1));
                        break;
                    }
                }
            }
            if (i==(selectionLineSeriesPressed->points().length()-1)) {
                topOfRange = newPointLoc.y();
                for (int ch=0;ch<channelLocations.count();ch++) {
                    const QPointF currentPoint = channelLocations.at(ch);
                    if (currentPoint.y() > topOfRange) {
                        topChannel = ch+(1000*(currentProbeShown+1));
                        break;
                    }
                }
            }
        }



        emit newStatus(QString("Range: %1 - %2uM    Channels: %3 - %4").arg((int)bottomOfRange).arg((int)topOfRange).arg(bottomChannel).arg(topChannel));
    }

    QChartView::mouseMoveEvent(event);
}

void ProbeLayoutPanel::setViewMin(int value)
{
    //qDebug() << "New value" << value;
    viewMinimum = value;
    chart()->axisY()->setRange(viewMinimum, viewMinimum+viewRange);
}

void ProbeLayoutPanel::setViewRange(int range)
{
    viewRange = range;
    chart()->axisY()->setRange(viewMinimum, viewMinimum+viewRange);
}

void ProbeLayoutPanel::channelBoxClicked(const QPointF &point)
{

    /*if (mode != selectionMode::singleChannelToggling) {
        //We only allow single channel toggling if that tool is selected
        return;
    }*/

    QAreaSeries* obj = (QAreaSeries*)sender();
    int channelIndex = obj->name().toInt();

    int m;

    m = (int)selectionMode::singleChannelToggling;

    qreal channelState = allSelectionModeData[currentProbeShown][m].arg[channelIndex];

    if (channelState == -1 && channelsTurnedOn[currentProbeShown].at(channelIndex)) {
        //in neutral state
        allSelectionModeData[currentProbeShown][m].arg[channelIndex] = 0.0;
        //toggleSinglePad(channelIndex,!channelsTurnedOn[currentProbeShown].at(channelIndex));
    } else {
        allSelectionModeData[currentProbeShown][m].arg[channelIndex] = -1; //make it neutral
    }


    updateChannels();

}



//**********************************************************

ProbeLayoutWidget::ProbeLayoutWidget(const TrodesConfiguration &c, QWidget *parent)
    :QWidget(parent)

{
    bool debugOutput = false;
    probeChanging = false;

    editWorkspace = c;
    originalWorkspace = c;
    readFromWorkspace();
    probeChanging = true;


    numberOfProbes = allSelectionModeArgs.length();

    QGridLayout *mainLayout = new QGridLayout(this);
    QGridLayout *chartLayout = new QGridLayout();

    if (debugOutput) qDebug() << "NP widget: read from workspace";
    //Add the chart and a scroll bar
    plotPanel = new ProbeLayoutPanel(allSelectionModeArgs);
    if (debugOutput) qDebug() << "NP widget: created panel";

    chartLayout->addWidget(plotPanel,0,0);
    connect(plotPanel,&ProbeLayoutPanel::newActiveChannelCount,this,&ProbeLayoutWidget::updateActiveChannelCount);
    connect(plotPanel,&ProbeLayoutPanel::selectionChanged,this,&ProbeLayoutWidget::writeToWorkspace);
    plotScrollControl = new QScrollBar(Qt::Vertical);

    int yRange = 10500; //range of entire probe (in uM)
    int pageViewRange = 1000; //zoom range
    int viewMinimum = 0; //location of the bottom of the view window
    plotScrollControl->setRange(0,yRange-pageViewRange);
    plotScrollControl->setPageStep(pageViewRange);
    plotPanel->setViewMin(viewMinimum);
    plotPanel->setViewRange(pageViewRange);
    plotScrollControl->setInvertedAppearance(true);
    chartLayout->addWidget(plotScrollControl,0,1);
    connect(plotScrollControl,&QScrollBar::valueChanged,plotPanel,&ProbeLayoutPanel::setViewMin);

    QFont controlCatFont;
    controlCatFont.setUnderline(true);
    //controlCatFont.setBold(true);
    //Add buttons at the top for tool selection
    QLabel *toolLabel = new QLabel("Channel selection");
    toolLabel->setFont(controlCatFont);
    QGridLayout *controlLayout = new QGridLayout();
    QGridLayout *toolLayout = new QGridLayout();
    toolLayout->setSpacing(2);

    toolButtonGroup = new QButtonGroup();
    toolButtonGroup->setExclusive(true);
    QPushButton *b1 = new QPushButton();
    b1->setCheckable(true);
    b1->setText("Short");
    b1->setFocusPolicy(Qt::NoFocus);
    toolButtonGroup->addButton(b1,ProbeLayoutPanel::singleContinuousGroup);
    toolLayout->addWidget(b1,0,0);
    QPushButton *b2 = new QPushButton();
    b2->setCheckable(true);
    b2->setText("Long");
    b2->setFocusPolicy(Qt::NoFocus);
    toolButtonGroup->addButton(b2,ProbeLayoutPanel::longSingleContinuousGroup);
    toolLayout->addWidget(b2,1,0);
    QPushButton *b3 = new QPushButton();
    b3->setCheckable(true);
    b3->setText("Multi");
    b3->setFocusPolicy(Qt::NoFocus);
    toolButtonGroup->addButton(b3,ProbeLayoutPanel::multiHighDensityGroups);
    toolLayout->addWidget(b3,2,0);
    /*QPushButton *b4 = new QPushButton();
    b4->setCheckable(true);
    b4->setText("Detail");
    b4->setFocusPolicy(Qt::NoFocus);
    toolButtonGroup->addButton(b4,ProbeLayoutPanel::singleChannelToggling);
    toolLayout->addWidget(b4,0,3);*/
    connect(toolButtonGroup,SIGNAL(idPressed(int)),plotPanel,SLOT(setSelectionMode(int)));

    //Indicator for number of channels that are turned on
    activeChannelCountIndicator = new QLabel();
    activeChannelCountIndicator->setText(QString("%1/384").arg(0));
    QLabel *channelCountLabel = new QLabel("Channel count");
    channelCountLabel->setFont(controlCatFont);

    QLabel *locLabel = new QLabel("Location");
    locLabel->setFont(controlCatFont);
    QGridLayout *locationLayout = new QGridLayout();
    locationLayout->setHorizontalSpacing(5);
    locationLayout->setVerticalSpacing(2);
    ap_editbox = new QLineEdit(QString("%1").arg(probeLocations.at(0).at(0)));
    ml_editbox = new QLineEdit(QString("%1").arg(probeLocations.at(0).at(1)));
    dv_editbox = new QLineEdit(QString("%1").arg(probeLocations.at(0).at(2)));
    QLabel *apLocLabel = new QLabel("AP");
    QLabel *mlLocLabel = new QLabel("ML");
    QLabel *dvLocLabel = new QLabel("DV");
    locationLayout->addWidget(ap_editbox,0,1);
    locationLayout->addWidget(ml_editbox,1,1);
    locationLayout->addWidget(dv_editbox,2,1);
    locationLayout->addWidget(apLocLabel,0,0);
    locationLayout->addWidget(mlLocLabel,1,0);
    locationLayout->addWidget(dvLocLabel,2,0);
    locationLayout->addWidget(new QLabel("mm"),0,2);
    locationLayout->addWidget(new QLabel("mm"),1,2);
    locationLayout->addWidget(new QLabel("mm"),2,2);
    ap_editbox->setValidator(new QDoubleValidator(-999.0,
                999.0, 2, ap_editbox));
    ml_editbox->setValidator(new QDoubleValidator(-999.0,
                999.0, 2, ml_editbox));
    dv_editbox->setValidator(new QDoubleValidator(-999.0,
                999.0, 2, dv_editbox));
    connect(ap_editbox,&QLineEdit::editingFinished,this,&ProbeLayoutWidget::apLocEditingFinished);
    connect(ml_editbox,&QLineEdit::editingFinished,this,&ProbeLayoutWidget::mlLocEditingFinished);
    connect(dv_editbox,&QLineEdit::editingFinished,this,&ProbeLayoutWidget::dvLocEditingFinished);



    //Controls for probe settings (gain and reference)
    QLabel *settingsLabel = new QLabel("Probe settings");
    settingsLabel->setFont(controlCatFont);
    QGridLayout *settingsLayout = new QGridLayout();
    settingsLayout->setHorizontalSpacing(5);
    settingsLayout->setVerticalSpacing(2);
    APGainSelector = new QComboBox();
    QStringList GainModes;
    GainModes << "50" << "125" << "250" << "500" << "1000" << "1500" << "2000" << "3000";
    APGainSelector->addItems(GainModes);
    LFPGainSelector = new QComboBox();
    LFPGainSelector->addItems(GainModes);
    referenceModeSelector = new QComboBox();
    QStringList RefModes;
    RefModes << "Wire" << "Tip" << "Internal";
    referenceModeSelector->addItems(RefModes);

    QLabel *apGainLabel = new QLabel("Spike gain");
    QLabel *lfpGainLabel = new QLabel("LFP gain");
    QLabel *refModeLabel = new QLabel("Reference");
    settingsLayout->addWidget(APGainSelector,0,1);
    settingsLayout->addWidget(LFPGainSelector,1,1);
    settingsLayout->addWidget(referenceModeSelector,2,1);
    settingsLayout->addWidget(apGainLabel,0,0,Qt::AlignRight);
    settingsLayout->addWidget(lfpGainLabel,1,0,Qt::AlignRight);
    settingsLayout->addWidget(refModeLabel,2,0,Qt::AlignRight);

    //All three of the settings combo boxes trigger the same slot when the values change


    connect(APGainSelector,SIGNAL(currentIndexChanged(int)),this,SLOT(probeSettingsChanged()));
    connect(LFPGainSelector,SIGNAL(currentIndexChanged(int)),this,SLOT(probeSettingsChanged()));
    connect(referenceModeSelector,SIGNAL(currentIndexChanged(int)),this,SLOT(probeSettingsChanged()));




    QLabel *pSelectorLabel = new QLabel("Probe selection");
    pSelectorLabel->setFont(controlCatFont);
    QGridLayout *probeSelectorLayout = new QGridLayout();
    probeSelectorLayout->setHorizontalSpacing(5);
    probeSelectorLayout->setVerticalSpacing(2);

    probeNumberSelector = new QComboBox();
    for (int i = 0;i<numberOfProbes;i++) {
        probeNumberSelector->addItem(QString("Probe %1").arg(i+1));
    }
    //connect(probeNumberSelector,SIGNAL(currentIndexChanged(int)),plotPanel,SLOT(setProbeShown(int)));
    connect(probeNumberSelector,SIGNAL(currentIndexChanged(int)),this,SLOT(currentProbeChanged(int)));

    copySettingsButton = new QPushButton("Copy to all");
    char tTip[] = "<html><head/><body><p>" \
                  "Copy settings from this probe to all other probes" \
                  "</p></body></html>";
    copySettingsButton->setToolTip(tTip);
    connect(copySettingsButton,&QPushButton::pressed,this,&ProbeLayoutWidget::copySettingsButtonPushed);
    probeSelectorLayout->addWidget(probeNumberSelector,0,0);
    probeSelectorLayout->addWidget(copySettingsButton,1,0);




    //Add buttons at the top for zoom control
    QLabel *zoomLabel = new QLabel("Zoom");
    zoomLabel->setFont(controlCatFont);
    QGridLayout *zoomLayout = new QGridLayout();
    zoomLayout->setSpacing(2);
    zoomButtonGroup = new QButtonGroup();
    zoomButtonGroup->setExclusive(true);
    QPushButton *zb = new QPushButton();
    zb->setCheckable(true);
    zb->setText("High");
    zb->setFocusPolicy(Qt::NoFocus);
    zoomButtonGroup->addButton(zb,0);
    zoomLayout->addWidget(zb,0,0);

    zb = new QPushButton();
    zb->setCheckable(true);
    zb->setText("Medium");
    zb->setFocusPolicy(Qt::NoFocus);
    zoomButtonGroup->addButton(zb,1);
    zoomLayout->addWidget(zb,1,0);

    zb = new QPushButton();
    zb->setCheckable(true);
    zb->setText("Low");
    zb->setChecked(true);
    zb->setFocusPolicy(Qt::NoFocus);
    zoomButtonGroup->addButton(zb,2);
    zoomLayout->addWidget(zb,2,0);
    connect(zoomButtonGroup,SIGNAL(idPressed(int)),this,SLOT(zoomButtonPressed(int)));


    //controlLayout->addWidget(probeNumberSelector,1,0,Qt::AlignTop);
    controlLayout->addLayout(probeSelectorLayout,1,0,Qt::AlignTop);
    controlLayout->addWidget(pSelectorLabel,0,0,Qt::AlignCenter);

    controlLayout->addLayout(toolLayout,1,1);
    controlLayout->addWidget(toolLabel,0,1,Qt::AlignCenter);

    controlLayout->addWidget(activeChannelCountIndicator,1,2,Qt::AlignTop);
    controlLayout->addWidget(channelCountLabel,0,2,Qt::AlignCenter);

    controlLayout->addWidget(settingsLabel,2,0,Qt::AlignCenter);
    controlLayout->addLayout(settingsLayout,3,0,Qt::AlignCenter);

    controlLayout->addWidget(locLabel,2,1,Qt::AlignCenter);
    controlLayout->addLayout(locationLayout,3,1,Qt::AlignCenter);

    controlLayout->addLayout(zoomLayout,3,2);
    controlLayout->addWidget(zoomLabel,2,2,Qt::AlignCenter);
    controlLayout->setColumnStretch(6,1);
    //controlLayout->setContentsMargins(0,0,0,0);
    //controlLayout->setSpacing(2);

    QGridLayout *bottomControlLayout = new QGridLayout();
    closeButton = new QPushButton("Close");
    connect(closeButton,&QPushButton::pressed,this,&QWidget::close);
    bottomControlLayout->addWidget(closeButton,0,10);
    bottomControlLayout->setColumnStretch(0,1);

    applyButton = new QPushButton("Apply");
    connect(applyButton,&QPushButton::pressed,this,&ProbeLayoutWidget::applyButtonPressed);
    bottomControlLayout->addWidget(applyButton,0,9);
    bottomControlLayout->setColumnStretch(0,1);

    QGridLayout *statusLayout = new QGridLayout();
    statusIndicator = new QLabel();
    statusLayout->addWidget(statusIndicator,0,0);
    statusLayout->setColumnStretch(1,1);


    mainLayout->addLayout(controlLayout,0,0);
    mainLayout->addLayout(chartLayout,1,0);
    mainLayout->addLayout(bottomControlLayout,2,0);
    mainLayout->addLayout(statusLayout,3,0);
    setLayout(mainLayout);

    if (debugOutput) qDebug() << "NP widget: created widgets";

    b1->setChecked(true);
    plotPanel->setSelectionMode(0);
    zoomButtonPressed(2);

    connect(plotPanel,&ProbeLayoutPanel::selectionModeChanged,this,&ProbeLayoutWidget::selectionToolInChartChanged);

    plotPanel->setSelectionModeForAllProbes(selectionModeForEachProbe);
    currentProbeChanged(0); //Sets up the settings for the first probe (which is the one that loads up as the displayed probe)

    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("neuropixels_window"));
    QRect tempPosition = settings.value(QLatin1String("position")).toRect();

    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
        resize(tempPosition.width(),tempPosition.height());

    } else {
        resize(0,500);
    }
    settings.endGroup();
    probeChanging = false;
    writeToWorkspace();
    applyButton->setEnabled(true);

    if (debugOutput) qDebug() << "NP widget: wrote to workspace";

    connect(plotPanel,&ProbeLayoutPanel::newStatus,this,&ProbeLayoutWidget::setStatus);

}

void ProbeLayoutWidget::selectionToolInChartChanged(int sMode)
{

    //qDebug() << "Mode" << sMode;

    //Since the selection tool data for each probe is stored in the Panel object, we
    //update the selection tool buttons based on its signal (instead of directly when we change
    //the active probe)
    QList<QAbstractButton*> buttons = toolButtonGroup->buttons();
    for (int i=0;i<buttons.length();i++) {
        buttons.at(i)->setChecked(i == sMode);
    }
    selectionModeForEachProbe[probeNumberSelector->currentIndex()] = sMode;
}

void ProbeLayoutWidget::copySettingsButtonPushed()
{
    //qDebug() << "Copy settings button pushed.";
    int currentProbeIndex = probeNumberSelector->currentIndex();

    for (int i=0; i<selectionModeForEachProbe.length();i++) {
        if (i != currentProbeIndex) {
            APGainForEachProbe[i] = APGainForEachProbe[currentProbeIndex];
            LFPGainForEachProbe[i] = LFPGainForEachProbe[currentProbeIndex];
            refModeForEachProbe[i] = refModeForEachProbe[currentProbeIndex];
            selectionModeForEachProbe[i] = selectionModeForEachProbe[currentProbeIndex];
            plotPanel->copyProbeSettings(currentProbeIndex,i);

        }
    }
}

void ProbeLayoutWidget::apLocEditingFinished()
{
    probeLocations[probeNumberSelector->currentIndex()][0] = ap_editbox->text().toDouble();
    probeSettingsChanged();
}

void ProbeLayoutWidget::mlLocEditingFinished()
{
    probeLocations[probeNumberSelector->currentIndex()][1] = ml_editbox->text().toDouble();
    probeSettingsChanged();
}

void ProbeLayoutWidget::dvLocEditingFinished()
{
    probeLocations[probeNumberSelector->currentIndex()][2] = dv_editbox->text().toDouble();
    probeSettingsChanged();
}

void ProbeLayoutWidget::currentProbeChanged(int probeIndex)
{
    probeChanging = true; //a flag to control the signal behavior of the settings combo boxes
    plotPanel->setProbeShown(probeIndex); //This will update what is displayed in the panel, and the panel will send a signal back to update the selectionTool buttons

    //Update the settings combo boxes
    APGainSelector->setCurrentIndex(APGainForEachProbe.at(probeIndex));
    LFPGainSelector->setCurrentIndex(LFPGainForEachProbe.at(probeIndex));
    referenceModeSelector->setCurrentIndex(refModeForEachProbe.at(probeIndex));

    ap_editbox->setText(QString("%1").arg(probeLocations[probeIndex][0]));
    ml_editbox->setText(QString("%1").arg(probeLocations[probeIndex][1]));
    dv_editbox->setText(QString("%1").arg(probeLocations[probeIndex][2]));


    probeChanging = false;
}

void ProbeLayoutWidget::applySettings()
{
    applyButtonPressed();
}

void ProbeLayoutWidget::applyButtonPressed()
{
    //The apply button was pressed. This initiates the process of removing the current workspace and replacing it with the one edited here.

    //emit requestStopAcquire();
    //QThread::msleep(500);



    emit closeWorkspace();
    workspaceShutdownCheckTimer = new QTimer(this);
    connect(workspaceShutdownCheckTimer, &QTimer::timeout, this, &ProbeLayoutWidget::continueApplyingSettings);
    workspaceShutdownCheckTimer->start(500); //We need to allow enough time for the source controller and streamProcessor threads to end

    applyButton->setEnabled(false);
}

void ProbeLayoutWidget::continueApplyingSettings()
{

    workspaceShutdownCheckTimer->stop();
    workspaceShutdownCheckTimer->deleteLater();
    qDebug() << "Preparing neuropixels info.";
    QList<QVector<bool> > channelsTurnedOn = plotPanel->getChannelsTurnedOn();
    NeuroPixelsSettings s;
    s.setNumProbes(numberOfProbes);

    for (int i=0; i<numberOfProbes;i++) { //Probes in workspace
        s.setAPGainCode((uint8_t)APGainForEachProbe.at(i),i);
        s.setLFPGainCode((uint8_t)LFPGainForEachProbe.at(i),i);
        s.setRefCode((uint8_t)refModeForEachProbe.at(i),i);
        for (int padInd = 0; padInd < channelsTurnedOn[i].length(); padInd++) {
            s.setPadState(padInd, channelsTurnedOn[i].at(padInd),i);
        }
    }

    emit newNeuroPixelsSettings(s);

    emit newWorkspace(editWorkspace);

}

void ProbeLayoutWidget::setStatus(QString status)
{
    statusIndicator->setText(status);
}

void ProbeLayoutWidget::setFocusChannel(int hwChan)
{

    int tIndex = editWorkspace.streamConf.trodeIndexLookupByHWChan.at(hwChan);
    int tID = (editWorkspace.spikeConf.ntrodes.at(tIndex)->nTrodeId%1000)-1;
    //qDebug() << "New focus:" << tID;
    plotPanel->setChannelHighlight(tID);


}

void ProbeLayoutWidget::probeSettingsChanged()
{

    if (probeChanging) return; //We do not want to log the settings if we are simply switching the active probe

    //qDebug() << "Probe settings changed for" << probeNumberSelector->currentIndex();
    //The current index in either the APGain, LFPGain, or referenceMode combo boxes changed.
    //APGainForEachProbe[probeNumberSelector->currentIndex()] = APGainSelector->currentIndex();
    //LFPGainForEachProbe[probeNumberSelector->currentIndex()] = LFPGainSelector->currentIndex();
    //refModeForEachProbe[probeNumberSelector->currentIndex()] = referenceModeSelector->currentIndex();

    //We update all three to be the same
    for (int i=0; i<numberOfProbes;i++) { //Probes in workspace
        APGainForEachProbe[i] = APGainSelector->currentIndex();
        LFPGainForEachProbe[i] = LFPGainSelector->currentIndex();
        refModeForEachProbe[i] = referenceModeSelector->currentIndex();
    }

    //selectionModeForEachProbe = plotPanel->getAllSelectionModes();

    writeToWorkspace();



}

void ProbeLayoutWidget::writeToWorkspace()
{

    //If the user is simply switching the displayed probe, we don't write to workspace
    if (probeChanging) return;

    int currentProbeIndex = 0;

    QList<QList<ProbeLayoutPanel::selectionModeData> > newSelectionArgs = plotPanel->getSelectionData();
    QList<QVector<bool> > channelsTurnedOn = plotPanel->getChannelsTurnedOn();

    QVector<int> gainModes;
    gainModes << 50 << 125 << 250 << 500 << 1000 << 1500 << 2000 << 3000;


    for (int i=0; i<editWorkspace.hardwareConf.sourceControls.length();i++) { //Probes in workspace
        if (editWorkspace.hardwareConf.sourceControls.at(i).name == "NeuroPixels1") {
            //We have found a section for the Neuropixels 1.0 probe
            editWorkspace.hardwareConf.sourceControls[i].customOptions.clear();
            CustomSourceOption op1;
            op1.name = "activeSelectionTool";
            op1.data.push_back(selectionModeForEachProbe.at(currentProbeIndex));
            editWorkspace.hardwareConf.sourceControls[i].customOptions.push_back(op1);
            CustomSourceOption op2;
            op2.name = "APGainMode";
            op2.data.push_back(gainModes.at(APGainForEachProbe.at(currentProbeIndex)));
            editWorkspace.hardwareConf.sourceControls[i].customOptions.push_back(op2);
            CustomSourceOption op3;
            op3.name = "LFPGainMode";
            op3.data.push_back(gainModes.at(LFPGainForEachProbe.at(currentProbeIndex)));
            editWorkspace.hardwareConf.sourceControls[i].customOptions.push_back(op3);
            CustomSourceOption op4;
            op4.name = "RefMode";
            op4.data.push_back(refModeForEachProbe.at(currentProbeIndex));
            editWorkspace.hardwareConf.sourceControls[i].customOptions.push_back(op4);
            CustomSourceOption op5;
            op5.name = "ProbeLocation";
            op5.data.push_back(probeLocations.at(currentProbeIndex).at(0));
            op5.data.push_back(probeLocations.at(currentProbeIndex).at(1));
            op5.data.push_back(probeLocations.at(currentProbeIndex).at(2));

            editWorkspace.hardwareConf.sourceControls[i].customOptions.push_back(op5);



            QStringList toolOrder;
            toolOrder << "single_continuous_selection" << "long_single_continuous_selection" << "multi_high_density_continuous_selections" << "individual_channel_toggling";

            for (int toolSearch = 0; toolSearch < toolOrder.length(); toolSearch++) {


                //workspace.hardwareConf->sourceControls[i].customOptions = toolOrder.at(toolSearch);
                for (int j=0;j<newSelectionArgs.at(currentProbeIndex).length();j++) {
                    if (!newSelectionArgs.at(currentProbeIndex).at(j).toolName.startsWith(toolOrder.at(toolSearch),Qt::CaseInsensitive))  {
                        continue;
                    }
                    //qDebug() << "Tool color during save: Probe "<< currentProbeIndex <<"Tool" << j << "Color" << newSelectionArgs.at(currentProbeIndex).at(j).controlColor;
                    ProbeLayoutPanel::selectionModeData selectorToolArgs;
                    selectorToolArgs.toolName = toolOrder.at(toolSearch);
                    if (toolOrder.at(toolSearch) == "multi_high_density_continuous_selections") {
                        int numRanges = (newSelectionArgs.at(currentProbeIndex).at(j).arg.length()-1)/2;
                        for (int rangeIndex = 0; rangeIndex < numRanges; rangeIndex++) {
                            //copy arguments, then include color info for each range
                            CustomSourceOption co;
                            co.name = toolOrder.at(toolSearch);
                            co.data.push_back(newSelectionArgs.at(currentProbeIndex).at(j).arg.at((rangeIndex*2)+1));
                            co.data.push_back(newSelectionArgs.at(currentProbeIndex).at(j).arg.at((rangeIndex*2)+2));
                            co.data.push_back(newSelectionArgs.at(currentProbeIndex).at(j).controlColor.at(rangeIndex).red());
                            co.data.push_back(newSelectionArgs.at(currentProbeIndex).at(j).controlColor.at(rangeIndex).green());
                            co.data.push_back(newSelectionArgs.at(currentProbeIndex).at(j).controlColor.at(rangeIndex).blue());
                            editWorkspace.hardwareConf.sourceControls[i].customOptions.push_back(co);
                        }
                    } else if (toolOrder.at(toolSearch) == "individual_channel_toggling") {
                        CustomSourceOption co;
                        co.name = toolOrder.at(toolSearch);
                        co.data += newSelectionArgs.at(currentProbeIndex).at(j).arg; //copy all arguments
                        editWorkspace.hardwareConf.sourceControls[i].customOptions.push_back(co);
                    } else {
                        //copy arguments, then include color info                      
                        CustomSourceOption co;
                        co.name = toolOrder.at(toolSearch);
                        co.data.push_back(newSelectionArgs.at(currentProbeIndex).at(j).arg.at(0));
                        co.data.push_back(newSelectionArgs.at(currentProbeIndex).at(j).arg.at(1));
                        co.data.push_back(newSelectionArgs.at(currentProbeIndex).at(j).controlColor.at(0).red());
                        co.data.push_back(newSelectionArgs.at(currentProbeIndex).at(j).controlColor.at(0).green());
                        co.data.push_back(newSelectionArgs.at(currentProbeIndex).at(j).controlColor.at(0).blue());
                        editWorkspace.hardwareConf.sourceControls[i].customOptions.push_back(co);
                    }

                    break;
                }
            }
            //We add a last options entry that lists all of the channels' on/off state
            CustomSourceOption co;
            co.name = "channelsOn";
            for (int i=0; i < channelsTurnedOn.at(currentProbeIndex).length();i++) {
                co.data.push_back(channelsTurnedOn.at(currentProbeIndex).at(i));
            }
            editWorkspace.hardwareConf.sourceControls[i].customOptions.push_back(co);


            //Add the ntrodes to the SpikeConfiguration



            currentProbeIndex++;


        }
    }


    QList<int>  spikeGains;
    QList<int>  lfpGains;
    QList<QVector<int> > probeLocationsToApply;

    for (int p=0;p<APGainForEachProbe.length();p++) {
        spikeGains.push_back(gainModes.at(APGainForEachProbe.at(p)));
        lfpGains.push_back(gainModes.at(APGainForEachProbe.at(p)));
        QVector<int> tmpLoc;

        tmpLoc << (int)(probeLocations[p][0]*1000) << (int)(probeLocations[p][1]*1000) << (int)(probeLocations[p][2]*1000); //AP, ML, DV in microns
        probeLocationsToApply.push_back(tmpLoc);
    }




    editWorkspace.addNeuropixelsNtrodes(plotPanel->getChannelsTurnedOn(),plotPanel->getChannelColors(),spikeGains,lfpGains,plotPanel->getChannelLocations(),probeLocationsToApply);

    if ((currentProbeIndex+1) < newSelectionArgs.length()) {
        qDebug() << "Error: Mismatch in number of Neuropixel entries.";
    }

    applyButton->setEnabled(true);

}


void ProbeLayoutWidget::readFromWorkspace()
{


    QVector<int> gainModes;
    gainModes << 50 << 125 << 250 << 500 << 1000 << 1500 << 2000 << 3000;

    for (int i=0; i<editWorkspace.hardwareConf.sourceControls.length();i++) { //Probes
        if (editWorkspace.hardwareConf.sourceControls.at(i).name == "NeuroPixels1") {
            //We have found a section for the Neuropixels 1.0 probe
            //qDebug() << "Found np section:" << workspace.hardwareConf->sourceControls.at(i).unitNumber;

            //QList<QList<qreal> > probeData;
            QList<ProbeLayoutPanel::selectionModeData> probeData;

            QStringList toolOrder;
            toolOrder << "single_continuous_selection" << "long_single_continuous_selection" << "multi_high_density_continuous_selections" << "individual_channel_toggling";

            for (int toolSearch = 0; toolSearch < toolOrder.length(); toolSearch++) {

                for (int j=0;j<editWorkspace.hardwareConf.sourceControls.at(i).customOptions.length();j++) {
                    if (!editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).name.startsWith(toolOrder.at(toolSearch),Qt::CaseInsensitive))  {
                        continue;
                    }
                    ProbeLayoutPanel::selectionModeData selectorToolArgs;
                    selectorToolArgs.toolName = toolOrder.at(toolSearch);

                    if (editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.length() == 5) {
                        //qDebug() << "Entry: " << editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).name;

                        //color of selector tool
                        selectorToolArgs.controlColor.push_back(QColor(editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(2),editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(3),editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(4)));
                        //qDebug() << "Color" << selectorToolArgs.controlColor.last();
                        for (int k=0; k<2;k++) {
                            selectorToolArgs.arg.push_back(editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(k)); //location and length of selector tool
                        }
                    } else {
                        selectorToolArgs.arg += editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data; //add all the numbers if there are not specifically 5
                    }
                    probeData.push_back(selectorToolArgs);
                }

            }

            //set default mode for each probe here
            int tmpSelectionMode = 0;
            int tmpAPGainMode = 0;
            int tmpLFPGainMode = 0;
            int tmpRefMode = 0;
            QVector<double> tmpProbeLoc;
            tmpProbeLoc << 0.0 << 0.0 << 0.0; //ap, ml, dv coordinates



            for (int j=0;j<editWorkspace.hardwareConf.sourceControls.at(i).customOptions.length();j++) {

                if (editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).name == "activeSelectionTool") {
                    if ( (editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) >= 0) && (editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) <= 2) ) {
                        tmpSelectionMode = editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);
                    }
                }
                if (editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).name == "APGainMode") {
                        int APGainVal = editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);

                        for (int s=0;s<gainModes.length();s++) {
                            if (gainModes.at(s) == APGainVal) {
                                tmpAPGainMode = s;
                                break;
                            }
                        }
                }
                if (editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).name == "LFPGainMode") {
                        int LFPGainVal = editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);

                        for (int s=0;s<gainModes.length();s++) {
                            if (gainModes.at(s) == LFPGainVal) {
                                tmpLFPGainMode = s;
                                break;
                            }
                        }
                }
                if (editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).name == "RefMode") {
                    if ( (editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) >= 0) && (editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) <= 2) ) {
                        tmpRefMode = editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);
                    }
                }

                if (editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).name == "ProbeLocation") {


                    if (editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.length() == 3) {
                        tmpProbeLoc[0] = editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0); //AP coordinate
                        tmpProbeLoc[1] = editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(1); //ML coordinate
                        tmpProbeLoc[2] = editWorkspace.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(2); //DV coordinate
                    }

                }
            }




            selectionModeForEachProbe.push_back(tmpSelectionMode);
            APGainForEachProbe.push_back(tmpAPGainMode);
            LFPGainForEachProbe.push_back(tmpLFPGainMode);
            refModeForEachProbe.push_back(tmpRefMode);
            allSelectionModeArgs.push_back(probeData);
            probeLocations.push_back(tmpProbeLoc);
        }
    }
}

void ProbeLayoutWidget::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);

    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("neuropixels_window"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void ProbeLayoutWidget::closeEvent(QCloseEvent *event)
{

    emit closing("NeuroPixels1");
    QWidget::closeEvent(event);
}

void ProbeLayoutWidget::wheelEvent(QWheelEvent *event)
{

    //Scroll wheel scrolls the chart up and down
    double changeAmount;
    int currentValue = plotScrollControl->value();
    int newValue;
    double scrollDivisor = 1.0;

    if (zoomMode == 0) {
        scrollDivisor = 25.0;
    } else if (zoomMode == 1) {
        scrollDivisor = 2.0;
    } else if (zoomMode == 2) {
        scrollDivisor = 1.0;
    }

    changeAmount = (float)event->angleDelta().y()/scrollDivisor;
    if ((changeAmount > 0.0) && (changeAmount < 1.0)) {
        changeAmount = 1.0;
    } else if ((changeAmount < 0.0) && (changeAmount > -1.0)) {
        changeAmount = -1.0;
    }

    newValue = currentValue+changeAmount;
    if (newValue > plotScrollControl->maximum()) {
        newValue = plotScrollControl->maximum();
    }
    if (newValue < plotScrollControl->minimum()) {
        newValue = plotScrollControl->minimum();
    }


    plotScrollControl->setValue(newValue);


    QWidget::wheelEvent(event);
}

void ProbeLayoutWidget::zoomButtonPressed(int m)
{

    int yRange = 10500; //range of entire probe (in uM)
    int pageViewRange = 1000;
    int markerSize = 12;

    if (m == 0) {
        //pageViewRange = 1000; //zoom range
        pageViewRange = 80;
        markerSize = 12;
    } else if (m == 1) {
        pageViewRange = 3000; //zoom range
        markerSize = 4;
    } else if (m == 2) {
        pageViewRange = 10000; //zoom range
        markerSize = 3;
    }

    zoomMode = m;


    plotScrollControl->setRange(0,yRange-pageViewRange);
    plotScrollControl->setPageStep(pageViewRange);
    plotScrollControl->setValue(0);
    plotPanel->setViewMin(0);
    plotPanel->setViewRange(pageViewRange);
    //plotPanel->setChannelPlotSize(markerSize);
}

void ProbeLayoutWidget::updateActiveChannelCount(int ch)
{
    activeChannelCountIndicator->setText(QString("%1/%2").arg(ch).arg(numberOfProbes*384));
}



