#ifndef QUICKSETUP_H
#define QUICKSETUP_H

#include <QtWidgets>
#include "hardwaresettings.h"

class HardwareSettingsDisplay : public QWidget
{
    Q_OBJECT
public:
    explicit HardwareSettingsDisplay(HardwareControllerSettings mcu, HeadstageSettings hs, QWidget *parent = nullptr);
    QGroupBox *mcusettingsbox;
    QGroupBox *hssettingsbox;
};

class QuickSetup : public QDialog
{
    Q_OBJECT
public:
    explicit QuickSetup(HardwareControllerSettings mcu, HeadstageSettings hs, int measuredPacketSize, QWidget *parent = nullptr);
    void hideMCUSettings();
    void hideHSSettings();
signals:
    void CreateWorkspace(bool ecuconnected, bool rfconnected, int chansperntrode, int psize, int samplingrate);
    void OpenWorkspaceEditor(bool ecuconnected, bool rfconnected, int chansperntrode, int psize, int samplingrate);
    void CancelPressed();
public slots:

private slots:
    void DoneClicked();

private:
    HardwareSettingsDisplay *hardwaresettingsdisplay;
    QCheckBox *ECUcheckbox;
    QCheckBox *RFcheckbox;
    QComboBox *chansperntrode;
    int psize;
    int samplingRate;
};

#endif // QUICKSETUP_H
