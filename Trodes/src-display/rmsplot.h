#ifndef RMSPLOT_H
#define RMSPLOT_H

#include <QtWidgets>
#include <QtCharts>
#include "sharedtrodesstyles.h"
#include "dialogs.h"

class RMSPlot : public QWidget
{
    Q_OBJECT
public:
    explicit RMSPlot(const TrodesConfiguration &conf,QWidget *parent = nullptr);

signals:
    void windowClosed();
    void oneSecBin();
    void tenSecBin();
public slots:
    void plot(QList<qreal> values);
private:

    //Histogram plot
    ChannelHistogramWidget *histChart;
    TrodesConfiguration workspace;

    //QChart *chart;
    //QChartView *chartView;
    //QBarCategoryAxis *Xaxis;
    //QValueAxis *Yaxis;
    QList<qreal> values;
    //QBarSeries *data;
    TrodesClickableLabel *infoLabel;
    QCheckBox *stopAutoScaling;
    QSpinBox *customMax;
    QCheckBox *freezePlot;
    void closeEvent(QCloseEvent* event);
    void exportDataCSV();
};

#endif // RMSPLOT_H
