============================
TrodesNetwork Python Package
============================

Deploying package to PyPi (for maintainers)
===========================================

This Python library is available on PyPi. Package maintainers can
update the package version using the following commands.

::

   python setup.py sdist
   twine upload dist/*

The ``twine`` command expects a file, which in this case would be a
``.tar.gz`` file generated from the first command.