Frequently Asked Questions
===========================


How do I...
------------

Inform developers of a bug?
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Write down the exact scenario that happened, everything that happened
the moment you started Trodes until the bug or crash, and what you were
clicking when it happened. Open the debugLogs folder and in the file
created under that session, at the top, note the version, compile time,
and Git commit.

Provide the developers with the debug log, your exact actions, operating
system, and if possible, the files you were using.

