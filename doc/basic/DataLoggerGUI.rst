Data Logger GUI
---------------

This tool was developed to provide a convenient way to download data from 
SpikeGadgets' data loggers and merge the data with a simultaneously recorded environmental record. It is used in conjunction with the SpikeGadgets' `Logger Dock <https://spikegadgets.com/products/logger-dock/>`_. The program can either be opened directly or from Trodes using the "Merge with logger data" menu item under "File" when an environmental record is opened in playback mode.


Mounting SD card
~~~~~~~~~~~~~~~~

Once you have finished your recording, you need to download the data to
your computer. To start this process, insert the SD card into the Logger Dock. The Logger Dock should be turned on and connector to your computer via a USB connection. Then, open the DataLoggerGUI application and hit the "Refresh list" button at the top of the window. If the SD card is recognized, the Logger Dock will appear an one of the entries in the list with information about card size and status. If data has been recorded on the SD card, the right-hand panel will also populate with information about the recording. Furthermore, the card status will indicate that the card is "Not enabled for recording". This is a safety feature to prevent accidental erasing of data before it had been downloaded to your computer. After you have downloaded the data, you will need to enable the card for recording before using it with a SpikeGadgets datalogger using the "Enable for recording" button. 

Once the SD card is detected, you can press the "Edit logger config" button to modify some of the logger runtime behaviors. These settings are stored on the SD card and used by the loggers when they power on. You can also modify the dock settings using the "Edit dock settings" button.

Extracting data and merging with environmental record
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Most users will have two recordings that were simultaneously recorded during the experiment: 1) the neural recording on the SD card, and 2) environmental recording (.rec file) taken by the Trodes software, which may be linked to other files such as video. These two files both have a synchronization pattern embedded in them (from the radio synchronization system used by the SpikeGadgets MCU or Docking Station). This synchronization pattern is used to register the two files in time and merge the records into a single .rec file.

To merge the neural data with the environmental data, make sure the checkbox next to "Merge logger data with environmental data" is checked. If you opened the DataLoggerGUI application from Trodes, the path to the environmental recording will already be populated. If you opened the application directly, you will need to browse to the environmental .rec file.

Next, you will need to select the "Final Trodes workspace" file that will be appended to the header of the merged file. This workspace should be the one that you use when recording from the headstage in tethered mode (live streaming to Trodes). Important: the workspace used during environmental recording should not be used here.

Finally, you will need to give a filename for the merge file. A default filename will be given that you can change if you want to. 

Now click the "START" button to begin the process.

Notes:
 
- If you are not merging with an environmental record, uncheck the corresponding checkbox. The "Final workspace" will simply be appended to the extracted file without a merge process.

-If you have already extracted a file from an SD card and the resulting .dat file is already on your computer, there is a browse button underneath the device list labeled "Already have an extracted .dat file?".  Once you have selected the .dat file, the extraction step will be skipped when the START button is pressed, and the data in the .dat file will be used instead.

Process output
~~~~~~~~~~~~~~

Once the START button is pressed, the extraction process will begin. The "Console" window will display the output of the process, with a percentage complete indications and potential errors or warnings displayed. For large files, this process may take several hours.

When the extraction process is complete, the extracted file will be saved onto your computer in the same folder as the designated output file. This is in case you need to redo the subsequent merge process and do not want to wait several hours for the extraction again. 

Next, the merge process will begin. This process is generally fairly fast. A percent complete will be shown in the console window, along with errors and warnings. If the process completes without errors, the merged .rec file will be saved on your computer automatically. Please review the output of the merge process as there are many errors and warnings that may need your attention:

- Excessive drift. During the ongoing merge process, every time a synchronization signal is detected the program will display how far apart the two files have drifted. It is normal for the two files to drift a few dozen samples every ten seconds. However, drift in the thousands of samples points to a larger problem that needs to be addressed.

- Excessive dropped data. Every time data are dropped in either the environmental file or the neural data file, a warning will be printed to the console displaying how may packets were dropped. A few hundred packets here and there is generally not a problem, but excessive drops will also need to be addressed, and likely indicates that the SD card was not fast enough to record the number of channels used on the headstage.

All alignment corrections (due to drift or dropped packets) gives priority to the neural recording. Data are either filled in or taken out of the environmental recording to accomplish the alignment. When data are filled in, the last digital or analog values are repeated.

If you opened the DataLoggerGUI from Trodes, clicking the "Close" button at the bottom of the window will trigger Trodes to automatically open the resulting merged .rec file in playback mode for inspection.

Card Enable
~~~~~~~~~~~

Before recording again, you need to enable the SD card so the logger headstage
will write to it. This is so users have to explicitly acknowledge that
the data on the SD card is no longer needed and is ready to be recorded
to again.

1. Select your device from the device list.
2. Click on Card Enable.
3. A window should pop up and the process should be finished almost immediately.
